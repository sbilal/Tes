$(document).ready(function () {      
    var modal = $('#notary-modal').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": 'api-get-notaries', 
    "columns": [
            { data: "id" },
            { data: "first_name"},       
            { data: "second_name"},       
            { data: "contact_number" },
            { data: "notary_number" },           
            { data: "notary_expiry_date" }, 
            { data : null, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>Select</button>", "searchable" : false }                        
            ],            
        colReorder: true
    });

    $('#notary-modal').on( 'click', 'button', function () {
        var data = modal.row( $(this).parents('tr') ).data();
        $("#notary_id").val(data.id); 
        $('#notary-modal-div').modal('hide');
    } );    
} );