function roles() {
	var slug = $('#slug').val();
	$.ajax({
		type: 'POST', 
		url : "list", 
		data: {"slug":slug},
		success : function (data) {

			$("#permission-details").html(data);
			
			$("#save-btn").show();



		}
	});
}




$(document).ready(function() {
       
    $("#update-permission").click(function(){
    	var slug = $('#slug').val();
        var permissions = [];
        $.each($("input[name='route_name']:checked"), function(){            
            permissions.push($(this).val());
        });

        if(permissions == ''){
        	$("#warning-message-modal .modalMessage").html("Select Permission");
	        $("#warning-message-modal").modal("show");
        } else {
	        $.ajax({
				type: 'POST', 
				url : "update", 
				data: {"permissions":permissions,"slug":slug},
				success : function (data) {
					
					if(data == 'pass'){
	                    $("#info-message-modal .modalMessage").html("Permissions Updated");
	                    $("#info-message-modal").modal("show");
	                }
				}
			});
    	}
    });
});