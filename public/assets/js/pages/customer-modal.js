$(document).ready(function () {      
    var modal = $('#customer-modal').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": 'api-get-customers', 
    "columns": [
            { data: "id" },
            { data: "first_name"},       
            { data: "second_name"},       
            { data: "mobile_1" },
            { data: "gender" },           
            { data : null, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>Select</button>", searchabe: false }                        
            ],            
        colReorder: true
    });

    $('#customer-modal').on( 'click', 'button', function () {
        var data = modal.row( $(this).parents('tr') ).data();
        $("#customer_id").val(data.id); 
        $('#customer-modal-div').modal('hide');
    } );    
} );