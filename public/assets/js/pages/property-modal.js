$(document).ready(function () {      
    var modal = $('#property-modal').DataTable({
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": 'property-datatable', 
    "columns": [
            { data: "id" },
            { data: "parcel_id" },
            { data: "currentowners", render: function ( data, type, full ) {
                                        return $.map( data, function ( d, i ) {
                                          var name = d.customer.first_name + " " + d.customer.second_name + " " + d.customer.third_name +  " "  + d.customer.fourth_name;
                                          if (d.primary_owner == "Yes") name += " [P] ";
                                          return name;
                                        } ).join( '<br/> ' ); },
                                "searchable" : false},
            { data : null, "defaultContent" : "<button class='btn btn-primary btn-sm' id='select'>Select</button>" ,"searchable" : false}                        
            ],            
        colReorder: true
    });

    $('#property-modal').on( 'click', 'button', function () {
        var data = modal.row( $(this).parents('tr') ).data();
        $("#property_id").val(data.id); 
        $('#property-modal-div').modal('hide');
    } );    
} );