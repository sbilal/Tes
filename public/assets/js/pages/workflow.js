    $(document).ready(function() {   
        $(".wrapper").addClass("hide_menu");
   
        $('body').on('click', '#claim-task', function() {
            var data = tasksToClaimTable.row( $(this).parents('tr') ).data();
            var taskId = data.data.workflow["task-id"];
            var transactionId = data.data.transaction.id;
            var url = "../../workflow/claim-task/"  + transactionId + "/" + taskId;
            $.post(url,
              function (data) {
                  flashMessage(data, "alert-success");            
                  tasksToClaimTable.ajax.reload();
                  myTasksTable.ajax.reload();
                    // reload Tasks to claim,/// Reload my Queue Ajax tables 
                  return false;
               }).fail(function(){
                  console.log("error");
               });      
               event.preventDefault();
        });       

        $('body').on('click', 'a.release-task', function() {
            var data = myTasksTable.row( $(this).parents('tr') ).data();
            var taskId = data.data.workflow["task-id"];
            var transactionId = data.data.transaction.id;
            var url = "../../workflow/release-task/"  + transactionId + "/" + taskId;
            $.post(url,
              function (data) {
                  flashMessage(data, "alert-success");            
                  tasksToClaimTable.ajax.reload();
                  myTasksTable.ajax.reload();
                  // reload Tasks to claim,/// Reload my Queue Ajax tables 
                  return false;
              }).fail(function(){
                  console.log("error");
              });      
              event.preventDefault();
        });
 
        var tasksToClaimTable = $('#tasks-to-claim-table').DataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,            
                "ajax": {
                    url: '../../workflow/dt-get-tasks-to-claim', 
                    type: "post"
                }, 
                "columnDefs": [
                            {
                                "targets": [ 0 ],
                                "visible": false,
                                "searchable": false
                            }
                        ],            
                "columns": [
                        { data: "data.workflow.task-id" },                
                        { data: "data.transaction", render: function (data, type, full) {
                                                                                        return "<a href='../../workflow/application/" + data.id + "'>" + data.transaction_reference_number  + "</a>";
                                                                                        }
                        },
                        { data: "data.transaction.service.name" },                
                        { data: "data.transaction.property.parcel_id" }, 
                        { data: "data.transaction", render: function ( data, type, full ) {
                                                                    var buyer = data.buyer.first_name + " " + data.buyer.second_name + " " + data.buyer.third_name + " " + data.buyer.fourth_name;
                                                                    return buyer ;
                                                                },
                                    "searchable" : false},                    
                        /*{ data: "transaction", render: function ( data, type, full ) {
                                                                    var seller =  data.seller.first_name + " " + data.seller.second_name + " " + data.seller.third_name + " " + data.seller.fourth_name;
                                                                    return  + seller;
                                                                },
                                    "searchable" : false},   */                          
                        { data: "data.transaction.settlement_date", "searchable" : false},      
                        { data: "data.transaction.erp_payment_id" },      
                        { data: "data.workflow",  render: function (data, type, full) {
                                return "<span>" + data["task-status"] + "</span>";
                        }},
                        { data : "data.transaction", render: function(data, type, full, meta) {
                                if (meta.row <= 0)
                                    return "<button class='btn btn-success btn-sm' id='claim-task'>claim</button>"
                                else 
                                    return "<button class='btn btn-disabled btn-sm' disabled id='claim-task'>claim</button>"},
                                "searchable" : false
                        }
                    ],            
                    colReorder: true
            });                

        var myTasksTable = $('#my-tasks-table').DataTable({
                "responsive": true,
                "processing": true,
                "serverSide": true,            
                "ajax": {
                    url: '../../workflow/dt-get-my-tasks', 
                    type: "post"
                },           
                "columns": [
                        { data: "data.workflow.task-id" },                
                        { data: "data.transaction.transaction_reference_number" },
                        { data: "data.transaction.service.name" },                
                        { data: "data.transaction.property.parcel_id" }, 
                        { data: "data.transaction", render: function ( data, type, full ) {
                                                                    var buyer = data.buyer.first_name + " " + data.buyer.second_name + " " + data.buyer.third_name + " " + data.buyer.fourth_name;
                                                                    return buyer ;
                                                                },
                                    "searchable" : false},                    
                        /*{ data: "transaction", render: function ( data, type, full ) {
                                                                    var seller =  data.seller.first_name + " " + data.seller.second_name + " " + data.seller.third_name + " " + data.seller.fourth_name;
                                                                    return  + seller;
                                                                },
                                    "searchable" : false},   */                          
                        { data: "data.transaction.settlement_date", "searchable" : false},      
                        { data: "data.transaction.erp_payment_id" },      
                        { data: "data.workflow",  render: function (data, type, full) {
                                return "<span>" + data["task-status"] + "</span>";
                        }},
                        { data : "data", render: function(data, type, full, meta) {
                                    return "<a href='../../workflow/application/" + data.transaction.id + "' class='btn btn-success btn-sm col-xs-4 process-task'>></a> \n\
                                            <a class='btn btn-warning btn-sm col-xs-4 release-task'> \n\
                                                x \n\
                                            </a>"},
                                "searchable" : false
                        }
                    ],            
                    colReorder: true
        });       
    });
