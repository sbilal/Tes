<?php

Route::get('unauthorized', 'Admin\AuthController@unauthorized')->name('unauthorized');

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::get('confirm-delete', array('as' => 'confirm-delete', 'uses' => 'GisPortalBaseController@getModalDelete'));
}); 

Route::group(['prefix' => 'workflow', 'middleware' => 'admin', 'as'=>'workflow.'], function () {
    Route::post('initiate/{transactionId}', ['as'=> 'initiate', 'uses' => 'WorkflowController@initiate']);    
    Route::post('claim-task/{transactionId}/{taskId}', 'WorkflowController@claimTask')->name('claim-task');
    Route::post('release-task/{transactionId}/{taskId}', 'WorkflowController@releaseTask')->name('release-task');
    Route::get('application/{id}', 'WorkflowController@getApplication')->name('view-application');
    Route::post('application/{id}/approve', 'WorkflowController@approveApplication')->name('approve-application');
    Route::post('application/{id}/reject', 'WorkflowController@rejectApplication')->name('reject-application');
    
    Route::get('dt-application-history/{id}/', 'WorkflowController@getApplicationHistoryDatatable')->name('dt-application-history');
    
    //datatable apis
    Route::post('dt-get-tasks-to-claim', 'WorkflowController@getUnclaimedTasksDatatable')->name('dt-get-tasks-to-claim');
    Route::post('dt-get-my-tasks', 'WorkflowController@getMyTasksDatatable')->name('dt-get-my-tasks');

    //may be userful for later implmentation 
    Route::get('admin/workflow', 'WorkflowController@index')->name('workflow');
    Route::get('admin/workflow/unclaimedTask/{role}', 'WorkflowController@getUnclaimedTasks')->name('workflow.unclaimedtasks');
    
});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::post('transaction/{id}/comments', ['as'=> 'transactions.comments.store', 'uses' => 'Transaction\TransactionController@addTransactionComments']);
    Route::post('transaction/{id}/document', ['as'=> 'transactions.document.store', 'uses' => 'Transaction\TransactionController@addTransactionDocument']);

    Route::post('transaction/{id}/document/delete', ['as'=> 'transaction.document.delete', 'uses' => 'Transaction\TransactionController@deleteTransactionDocument']);
    Route::post('transaction/{id}/commens/delete', ['as'=> 'transaction.comments.delete', 'uses' => 'Transaction\TransactionController@deleteTransactionComments']);

    Route::post('create-transaction', ['as'=> 'create-transaction', 'uses' => 'Transaction\TransactionController@createTransaction']); 
    Route::get('invoice/gis-invoice/{id}', ['as'=> 'invoice.gis-invoice', 'uses' => 'Transaction\TransactionController@getInvoice']);
    Route::post('transaction/submit-payment', ['as'=> 'transaction.submit-payment', 'uses' => 'Transaction\TransactionController@makePayment']);    
    
    Route::post('api-get-recent-transactions', 'Transaction\TransactionController@getRecentTransactionsApi')->name('api-get-recent-transactions');
    
    Route::get('wall_transactions', ['as'=> 'transactions.wall_transaction', 'uses' => 'Transaction\TransactionWallController@index']);
    Route::get('transactions', ['as'=> 'transactions.index', 'uses' => 'Transaction\TransactionController@index']);
    Route::post('transactions', ['as'=> 'transactions.store', 'uses' => 'Transaction\TransactionController@store']);
    Route::get('transactions/create', ['as'=> 'transactions.create', 'uses' => 'Transaction\TransactionController@create']);
    Route::put('transactions/{transactions}', ['as'=> 'transactions.update', 'uses' => 'Transaction\TransactionController@update']);
    Route::patch('transactions/{transactions}', ['as'=> 'transactions.update', 'uses' => 'Transaction\TransactionController@update']);
    Route::get('transactions/{id}/delete', array('as' => 'transactions.delete', 'uses' => 'Transaction\TransactionController@getDelete'));
    Route::get('transactions/{id}/confirm-delete', array('as' => 'transactions.confirm-delete', 'uses' => 'Transaction\TransactionController@getModalDelete'));
    Route::get('transactions/{transactions}', ['as'=> 'transactions.show', 'uses' => 'Transaction\TransactionController@show']);
    Route::get('transactions/{transactions}/edit', ['as'=> 'transactions.edit', 'uses' => 'Transaction\TransactionController@edit']);
});


/* Role Based Routes */
Route::group(['prefix' => '/app', 'middleware' => 'admin'], function () {
    Route::get('user/dashboard', 'Auth\RoleBaseController@dashboard')->name('user.dashboard');
    // no need of any mapping below to clean up after some evaluatoin 
    Route::get('admin/dashboard', 'Auth\TellerRoleController@dashboard')->name('admin.dashboard');
    Route::get('teller/dashboard', 'Auth\TellerRoleController@dashboard')->name('teller.dashboard');
    Route::get('notary/dashboard', 'Auth\NotaryRoleController@dashboard')->name('notary.dashboard');
    Route::get('court/dashboard', 'Auth\CourtRoleController@dashboard')->name('court.dashboard');
    Route::get('district-officer/dashboard', 'Auth\DistrictOfficerRoleController@dashboard')->name('district-officer.dashboard');
    Route::get('district-geometer/dashboard', 'Auth\DistrictGeometerRoleController@dashboard')->name('district-geometer.dashboard');
    Route::get('headoffice-officer/dashboard', 'Auth\HeadOfficeOfficerRoleController@dashboard')->name('headoffice-officer.dashboard');
    Route::get('headoffice-geometer/dashboard', 'Auth\HeadOfficeGeometerRoleController@dashboard')->name('headoffice-geometer.dashboard');
    Route::get('delivery/dashboard', 'Auth\DeliveryRoleController@dashboard')->name('delivery.dashboard');
    Route::get('executive/dashboard', 'Auth\ExecutiveRoleController@dashboard')->name('executive.dashboard');
    Route::get('customer-service/dashboard', 'Auth\CustomerServiceRoleController@dashboard')->name('customer-service.dashboard');
    Route::get('customerservice-officerverification/dashboard', 'Auth\CustomerServiceRoleController@dashboard')->name('customerservice-officerverification.dashboard');
});

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::get('property-modal', 'propertyController@modal')->name('property-modal');
    Route::get('property-datatable', 'propertyController@getPropertyJson')->name('property-datatable');    
    Route::get('property-main-partial/{id}', 'propertyController@getMainPartial')->name('property-main-partial');
    Route::get('property-cetificate/{id}', 'propertyController@generatePropertyCetificatePdf')->name('property-cetificate');
});

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {    
    Route::get('customer-modal-test', 'CustomerController@modalTest')->name('customer-modal-test');
    Route::get('api-get-customers', 'CustomerController@getCustomersApi')->name('api-get-customers');    
    Route::get('api-get-customer/{id}', 'CustomerController@getCustomerApi')->name('api-get-customer');    
    Route::get('customer-main-partial/{id}', 'CustomerController@getMainPartial')->name('customer-main-partial');
});

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::get('api-get-notaries', 'NotaryController@getNotariesApi')->name('api-get-notaries');    
    Route::get('api-get-notary/{id}', 'NotaryController@getNotaryApi')->name('api-get-notary');    
    Route::get('notary-main-partial/{id}', 'NotaryController@getMainPartial')->name('notary-main-partial');
});

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::get('services', ['as'=> 'services.index', 'uses' => 'ServiceController@index']);
    Route::post('services', ['as'=> 'services.store', 'uses' => 'ServiceController@store']);
    Route::get('services/create', ['as'=> 'services.create', 'uses' => 'ServiceController@create']);
    Route::put('services/{services}', ['as'=> 'services.update', 'uses' => 'ServiceController@update']);
    Route::patch('services/{services}', ['as'=> 'services.update', 'uses' => 'ServiceController@update']);
    Route::get('services/{id}/delete', array('as' => 'services.delete', 'uses' => 'ServiceController@getDelete'));
    Route::get('services/{id}/confirm-delete', array('as' => 'services.confirm-delete', 'uses' => 'ServiceController@getModalDelete'));
    Route::get('services/{services}', ['as'=> 'services.show', 'uses' => 'ServiceController@show']);
    Route::get('services/{services}/edit', ['as'=> 'services.edit', 'uses' => 'ServiceController@edit']);
});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('documentTypes', ['as'=> 'documentTypes.index', 'uses' => 'DocumentTypeController@index']);
Route::post('documentTypes', ['as'=> 'documentTypes.store', 'uses' => 'DocumentTypeController@store']);
Route::get('documentTypes/create', ['as'=> 'documentTypes.create', 'uses' => 'DocumentTypeController@create']);
Route::put('documentTypes/{documentTypes}', ['as'=> 'documentTypes.update', 'uses' => 'DocumentTypeController@update']);
Route::patch('documentTypes/{documentTypes}', ['as'=> 'documentTypes.update', 'uses' => 'DocumentTypeController@update']);
Route::get('documentTypes/{id}/delete', array('as' => 'documentTypes.delete', 'uses' => 'DocumentTypeController@getDelete'));
Route::get('documentTypes/{id}/confirm-delete', array('as' => 'documentTypes.confirm-delete', 'uses' => 'DocumentTypeController@getModalDelete'));
Route::get('documentTypes/{documentTypes}', ['as'=> 'documentTypes.show', 'uses' => 'DocumentTypeController@show']);
Route::get('documentTypes/{documentTypes}/edit', ['as'=> 'documentTypes.edit', 'uses' => 'DocumentTypeController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('feeTypes', ['as'=> 'feeTypes.index', 'uses' => 'FeeTypeController@index']);
Route::post('feeTypes', ['as'=> 'feeTypes.store', 'uses' => 'FeeTypeController@store']);
Route::get('feeTypes/create', ['as'=> 'feeTypes.create', 'uses' => 'FeeTypeController@create']);
Route::put('feeTypes/{feeTypes}', ['as'=> 'feeTypes.update', 'uses' => 'FeeTypeController@update']);
Route::patch('feeTypes/{feeTypes}', ['as'=> 'feeTypes.update', 'uses' => 'FeeTypeController@update']);
Route::get('feeTypes/{id}/delete', array('as' => 'feeTypes.delete', 'uses' => 'FeeTypeController@getDelete'));
Route::get('feeTypes/{id}/confirm-delete', array('as' => 'feeTypes.confirm-delete', 'uses' => 'FeeTypeController@getModalDelete'));
Route::get('feeTypes/{feeTypes}', ['as'=> 'feeTypes.show', 'uses' => 'FeeTypeController@show']);
Route::get('feeTypes/{feeTypes}/edit', ['as'=> 'feeTypes.edit', 'uses' => 'FeeTypeController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('statuses', ['as'=> 'statuses.index', 'uses' => 'StatusController@index']);
Route::post('statuses', ['as'=> 'statuses.store', 'uses' => 'StatusController@store']);
Route::get('statuses/create', ['as'=> 'statuses.create', 'uses' => 'StatusController@create']);
Route::put('statuses/{statuses}', ['as'=> 'statuses.update', 'uses' => 'StatusController@update']);
Route::patch('statuses/{statuses}', ['as'=> 'statuses.update', 'uses' => 'StatusController@update']);
Route::get('statuses/{id}/delete', array('as' => 'statuses.delete', 'uses' => 'StatusController@getDelete'));
Route::get('statuses/{id}/confirm-delete', array('as' => 'statuses.confirm-delete', 'uses' => 'StatusController@getModalDelete'));
Route::get('statuses/{statuses}', ['as'=> 'statuses.show', 'uses' => 'StatusController@show']);
Route::get('statuses/{statuses}/edit', ['as'=> 'statuses.edit', 'uses' => 'StatusController@edit']);

});


Route::group(array('prefix' => 'admin/district/', 'middleware' => 'admin','as'=>'admin.district.'), function () {

Route::get('districts', ['as'=> 'index', 'uses' => 'DistrictController@index']);
Route::post('districts', ['as'=> 'store', 'uses' => 'DistrictController@store']);
Route::get('districts/create', ['as'=> 'create', 'uses' => 'DistrictController@create']);
Route::put('districts/{districts}', ['as'=> 'update', 'uses' => 'DistrictController@update']);
Route::patch('districts/{districts}', ['as'=> 'update', 'uses' => 'DistrictController@update']);
Route::get('districts/{id}/delete', array('as' => 'delete', 'uses' => 'DistrictController@getDelete'));
Route::get('districts/{id}/confirm-delete', array('as' => 'confirm-delete', 'uses' => 'DistrictController@getModalDelete'));
Route::get('districts/{districts}', ['as'=> 'show', 'uses' => 'DistrictController@show']);
Route::get('districts/{districts}/edit', ['as'=> 'edit', 'uses' => 'DistrictController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('subDivisions', ['as'=> 'subDivisions.index', 'uses' => 'SubDivisionController@index']);
Route::post('subDivisions', ['as'=> 'subDivisions.store', 'uses' => 'SubDivisionController@store']);
Route::get('subDivisions/create', ['as'=> 'subDivisions.create', 'uses' => 'SubDivisionController@create']);
Route::put('subDivisions/{subDivisions}', ['as'=> 'subDivisions.update', 'uses' => 'SubDivisionController@update']);
Route::patch('subDivisions/{subDivisions}', ['as'=> 'subDivisions.update', 'uses' => 'SubDivisionController@update']);
Route::get('subDivisions/{id}/delete', array('as' => 'subDivisions.delete', 'uses' => 'SubDivisionController@getDelete'));
Route::get('subDivisions/{id}/confirm-delete', array('as' => 'subDivisions.confirm-delete', 'uses' => 'SubDivisionController@getModalDelete'));
Route::get('subDivisions/{subDivisions}', ['as'=> 'subDivisions.show', 'uses' => 'SubDivisionController@show']);
Route::get('subDivisions/{subDivisions}/edit', ['as'=> 'subDivisions.edit', 'uses' => 'SubDivisionController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('subDivisions', ['as'=> 'subDivisions.index', 'uses' => 'SubDivisionController@index']);
Route::post('subDivisions', ['as'=> 'subDivisions.store', 'uses' => 'SubDivisionController@store']);
Route::get('subDivisions/create', ['as'=> 'subDivisions.create', 'uses' => 'SubDivisionController@create']);
Route::put('subDivisions/{subDivisions}', ['as'=> 'subDivisions.update', 'uses' => 'SubDivisionController@update']);
Route::patch('subDivisions/{subDivisions}', ['as'=> 'subDivisions.update', 'uses' => 'SubDivisionController@update']);
Route::get('subDivisions/{id}/delete', array('as' => 'subDivisions.delete', 'uses' => 'SubDivisionController@getDelete'));
Route::get('subDivisions/{id}/confirm-delete', array('as' => 'subDivisions.confirm-delete', 'uses' => 'SubDivisionController@getModalDelete'));
Route::get('subDivisions/{subDivisions}', ['as'=> 'subDivisions.show', 'uses' => 'SubDivisionController@show']);
Route::get('subDivisions/{subDivisions}/edit', ['as'=> 'subDivisions.edit', 'uses' => 'SubDivisionController@edit']);

});




Route::group(array('prefix' => 'admin/documents/', 'middleware' => 'admin','as'=>'admin.documents.'), function () {

Route::get('documents', ['as'=> 'documents.index', 'uses' => 'Documents\DocumentsController@index']);
Route::post('documents', ['as'=> 'documents.store', 'uses' => 'Documents\DocumentsController@store']);
Route::get('documents/create', ['as'=> 'documents.create', 'uses' => 'Documents\DocumentsController@create']);
Route::put('documents/{documents}', ['as'=> 'documents.update', 'uses' => 'Documents\DocumentsController@update']);
Route::patch('documents/{documents}', ['as'=> 'documents.update', 'uses' => 'Documents\DocumentsController@update']);
Route::get('documents/{id}/delete', array('as' => 'documents.delete', 'uses' => 'Documents\DocumentsController@getDelete'));
Route::get('documents/{id}/confirm-delete', array('as' => 'documents.confirm-delete', 'uses' => 'Documents\DocumentsController@getModalDelete'));
Route::get('documents/{documents}', ['as'=> 'documents.show', 'uses' => 'Documents\DocumentsController@show']);
Route::get('documents/{documents}/edit', ['as'=> 'documents.edit', 'uses' => 'Documents\DocumentsController@edit']);

});

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('zonings', ['as'=> 'zonings.index', 'uses' => 'ZoningController@index']);
Route::post('zonings', ['as'=> 'zonings.store', 'uses' => 'ZoningController@store']);
Route::get('zonings/create', ['as'=> 'zonings.create', 'uses' => 'ZoningController@create']);
Route::put('zonings/{zonings}', ['as'=> 'zonings.update', 'uses' => 'ZoningController@update']);
Route::patch('zonings/{zonings}', ['as'=> 'zonings.update', 'uses' => 'ZoningController@update']);
Route::get('zonings/{id}/delete', array('as' => 'zonings.delete', 'uses' => 'ZoningController@getDelete'));
Route::get('zonings/{id}/confirm-delete', array('as' => 'zonings.confirm-delete', 'uses' => 'ZoningController@getModalDelete'));
Route::get('zonings/{zonings}', ['as'=> 'zonings.show', 'uses' => 'ZoningController@show']);
Route::get('zonings/{zonings}/edit', ['as'=> 'zonings.edit', 'uses' => 'ZoningController@edit']);

});


Route::group(array('prefix' => 'admin/propertyStatus/', 'middleware' => 'admin','as'=>'admin.propertyStatus.'), function () {

Route::get('propertyStatuses', ['as'=> 'propertyStatuses.index', 'uses' => 'Propertystatus\PropertyStatusController@index']);
Route::post('propertyStatuses', ['as'=> 'propertyStatuses.store', 'uses' => 'Propertystatus\PropertyStatusController@store']);
Route::get('propertyStatuses/create', ['as'=> 'propertyStatuses.create', 'uses' => 'Propertystatus\PropertyStatusController@create']);
Route::put('propertyStatuses/{propertyStatuses}', ['as'=> 'propertyStatuses.update', 'uses' => 'Propertystatus\PropertyStatusController@update']);
Route::patch('propertyStatuses/{propertyStatuses}', ['as'=> 'propertyStatuses.update', 'uses' => 'Propertystatus\PropertyStatusController@update']);
Route::get('propertyStatuses/{id}/delete', array('as' => 'propertyStatuses.delete', 'uses' => 'Propertystatus\PropertyStatusController@getDelete'));
Route::get('propertyStatuses/{id}/confirm-delete', array('as' => 'propertyStatuses.confirm-delete', 'uses' => 'Propertystatus\PropertyStatusController@getModalDelete'));
Route::get('propertyStatuses/{propertyStatuses}', ['as'=> 'propertyStatuses.show', 'uses' => 'Propertystatus\PropertyStatusController@show']);
Route::get('propertyStatuses/{propertyStatuses}/edit', ['as'=> 'propertyStatuses.edit', 'uses' => 'Propertystatus\PropertyStatusController@edit']);

});


Route::group(array('prefix' => 'admin/bank/', 'middleware' => 'admin','as'=>'admin.bank.'), function () {

Route::get('banks', ['as'=> 'banks.index', 'uses' => 'Bank\BankController@index']);
Route::post('banks', ['as'=> 'banks.store', 'uses' => 'Bank\BankController@store']);
Route::get('banks/create', ['as'=> 'banks.create', 'uses' => 'Bank\BankController@create']);
Route::put('banks/{banks}', ['as'=> 'banks.update', 'uses' => 'Bank\BankController@update']);
Route::patch('banks/{banks}', ['as'=> 'banks.update', 'uses' => 'Bank\BankController@update']);
Route::get('banks/{id}/delete', array('as' => 'banks.delete', 'uses' => 'Bank\BankController@getDelete'));
Route::get('banks/{id}/confirm-delete', array('as' => 'banks.confirm-delete', 'uses' => 'Bank\BankController@getModalDelete'));
Route::get('banks/{banks}', ['as'=> 'banks.show', 'uses' => 'Bank\BankController@show']);
Route::get('banks/{banks}/edit', ['as'=> 'banks.edit', 'uses' => 'Bank\BankController@edit']);

});


Route::group(array('prefix' => 'admin/invocie/', 'middleware' => 'admin','as'=>'admin.invocie.'), function () {

Route::get('invocies', ['as'=> 'invocies.index', 'uses' => 'Invocie\InvocieController@index']);
Route::post('invocies', ['as'=> 'invocies.store', 'uses' => 'Invocie\InvocieController@store']);
Route::get('invocies/create', ['as'=> 'invocies.create', 'uses' => 'Invocie\InvocieController@create']);
Route::put('invocies/{invocies}', ['as'=> 'invocies.update', 'uses' => 'Invocie\InvocieController@update']);
Route::patch('invocies/{invocies}', ['as'=> 'invocies.update', 'uses' => 'Invocie\InvocieController@update']);
Route::get('invocies/{id}/delete', array('as' => 'invocies.delete', 'uses' => 'Invocie\InvocieController@getDelete'));
Route::get('invocies/{id}/confirm-delete', array('as' => 'invocies.confirm-delete', 'uses' => 'Invocie\InvocieController@getModalDelete'));
Route::get('invocies/{invocies}', ['as'=> 'invocies.show', 'uses' => 'Invocie\InvocieController@show']);
Route::get('invocies/{invocies}/edit', ['as'=> 'invocies.edit', 'uses' => 'Invocie\InvocieController@edit']);

});


Route::group(array('prefix' => 'admin/payment/', 'middleware' => 'admin','as'=>'admin.payment.'), function () {

Route::get('payments', ['as'=> 'payments.index', 'uses' => 'Payment\PaymentController@index']);
Route::post('payments', ['as'=> 'payments.store', 'uses' => 'Payment\PaymentController@store']);
Route::get('payments/create', ['as'=> 'payments.create', 'uses' => 'Payment\PaymentController@create']);
Route::put('payments/{payments}', ['as'=> 'payments.update', 'uses' => 'Payment\PaymentController@update']);
Route::patch('payments/{payments}', ['as'=> 'payments.update', 'uses' => 'Payment\PaymentController@update']);
Route::get('payments/{id}/delete', array('as' => 'payments.delete', 'uses' => 'Payment\PaymentController@getDelete'));
Route::get('payments/{id}/confirm-delete', array('as' => 'payments.confirm-delete', 'uses' => 'Payment\PaymentController@getModalDelete'));
Route::get('payments/{payments}', ['as'=> 'payments.show', 'uses' => 'Payment\PaymentController@show']);
Route::get('payments/{payments}/edit', ['as'=> 'payments.edit', 'uses' => 'Payment\PaymentController@edit']);

});










Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('customerIdentities', ['as'=> 'customerIdentities.index', 'uses' => 'CustomerIdentityController@index']);
Route::post('customerIdentities', ['as'=> 'customerIdentities.store', 'uses' => 'CustomerIdentityController@store']);
Route::get('customerIdentities/create', ['as'=> 'customerIdentities.create', 'uses' => 'CustomerIdentityController@create']);
Route::put('customerIdentities/{customerIdentities}', ['as'=> 'customerIdentities.update', 'uses' => 'CustomerIdentityController@update']);
Route::patch('customerIdentities/{customerIdentities}', ['as'=> 'customerIdentities.update', 'uses' => 'CustomerIdentityController@update']);
Route::get('customerIdentities/{id}/delete', array('as' => 'customerIdentities.delete', 'uses' => 'CustomerIdentityController@getDelete'));
Route::get('customerIdentities/{id}/confirm-delete', array('as' => 'customerIdentities.confirm-delete', 'uses' => 'CustomerIdentityController@getModalDelete'));
Route::get('customerIdentities/{customerIdentities}', ['as'=> 'customerIdentities.show', 'uses' => 'CustomerIdentityController@show']);
Route::get('customerIdentities/{customerIdentities}/edit', ['as'=> 'customerIdentities.edit', 'uses' => 'CustomerIdentityController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('documents', ['as'=> 'documents.index', 'uses' => 'DocumentsController@index']);
Route::post('documents', ['as'=> 'documents.store', 'uses' => 'DocumentsController@store']);
Route::get('documents/create', ['as'=> 'documents.create', 'uses' => 'DocumentsController@create']);
Route::put('documents/{documents}', ['as'=> 'documents.update', 'uses' => 'DocumentsController@update']);
Route::patch('documents/{documents}', ['as'=> 'documents.update', 'uses' => 'DocumentsController@update']);
Route::get('documents/{id}/delete', array('as' => 'documents.delete', 'uses' => 'DocumentsController@getDelete'));
Route::get('documents/{id}/confirm-delete', array('as' => 'documents.confirm-delete', 'uses' => 'DocumentsController@getModalDelete'));
Route::get('documents/{documents}', ['as'=> 'documents.show', 'uses' => 'DocumentsController@show']);
Route::get('documents/{documents}/edit', ['as'=> 'documents.edit', 'uses' => 'DocumentsController@edit']);

});





Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('propertyOwnerships', ['as'=> 'propertyOwnerships.index', 'uses' => 'propertyOwnershipController@index']);
Route::post('propertyOwnerships', ['as'=> 'propertyOwnerships.store', 'uses' => 'propertyOwnershipController@store']);
Route::get('propertyOwnerships/create', ['as'=> 'propertyOwnerships.create', 'uses' => 'propertyOwnershipController@create']);
Route::put('propertyOwnerships/{propertyOwnerships}', ['as'=> 'propertyOwnerships.update', 'uses' => 'propertyOwnershipController@update']);
Route::patch('propertyOwnerships/{propertyOwnerships}', ['as'=> 'propertyOwnerships.update', 'uses' => 'propertyOwnershipController@update']);
Route::get('propertyOwnerships/{id}/delete', array('as' => 'propertyOwnerships.delete', 'uses' => 'propertyOwnershipController@getDelete'));
Route::get('propertyOwnerships/{id}/confirm-delete', array('as' => 'propertyOwnerships.confirm-delete', 'uses' => 'propertyOwnershipController@getModalDelete'));
Route::get('propertyOwnerships/{propertyOwnerships}', ['as'=> 'propertyOwnerships.show', 'uses' => 'propertyOwnershipController@show']);
Route::get('propertyOwnerships/{propertyOwnerships}/edit', ['as'=> 'propertyOwnerships.edit', 'uses' => 'propertyOwnershipController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('propertylienhistories', ['as'=> 'propertylienhistories.index', 'uses' => 'propertylienhistoryController@index']);
Route::post('propertylienhistories', ['as'=> 'propertylienhistories.store', 'uses' => 'propertylienhistoryController@store']);
Route::get('propertylienhistories/create', ['as'=> 'propertylienhistories.create', 'uses' => 'propertylienhistoryController@create']);
Route::put('propertylienhistories/{propertylienhistories}', ['as'=> 'propertylienhistories.update', 'uses' => 'propertylienhistoryController@update']);
Route::patch('propertylienhistories/{propertylienhistories}', ['as'=> 'propertylienhistories.update', 'uses' => 'propertylienhistoryController@update']);
Route::get('propertylienhistories/{id}/delete', array('as' => 'propertylienhistories.delete', 'uses' => 'propertylienhistoryController@getDelete'));
Route::get('propertylienhistories/{id}/confirm-delete', array('as' => 'propertylienhistories.confirm-delete', 'uses' => 'propertylienhistoryController@getModalDelete'));
Route::get('propertylienhistories/{propertylienhistories}', ['as'=> 'propertylienhistories.show', 'uses' => 'propertylienhistoryController@show']);
Route::get('propertylienhistories/{propertylienhistories}/edit', ['as'=> 'propertylienhistories.edit', 'uses' => 'propertylienhistoryController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('documentShelves', ['as'=> 'documentShelves.index', 'uses' => 'documentShelfController@index']);
Route::post('documentShelves', ['as'=> 'documentShelves.store', 'uses' => 'documentShelfController@store']);
Route::get('documentShelves/create', ['as'=> 'documentShelves.create', 'uses' => 'documentShelfController@create']);
Route::put('documentShelves/{documentShelves}', ['as'=> 'documentShelves.update', 'uses' => 'documentShelfController@update']);
Route::patch('documentShelves/{documentShelves}', ['as'=> 'documentShelves.update', 'uses' => 'documentShelfController@update']);
Route::get('documentShelves/{id}/delete', array('as' => 'documentShelves.delete', 'uses' => 'documentShelfController@getDelete'));
Route::get('documentShelves/{id}/confirm-delete', array('as' => 'documentShelves.confirm-delete', 'uses' => 'documentShelfController@getModalDelete'));
Route::get('documentShelves/{documentShelves}', ['as'=> 'documentShelves.show', 'uses' => 'documentShelfController@show']);
Route::get('documentShelves/{documentShelves}/edit', ['as'=> 'documentShelves.edit', 'uses' => 'documentShelfController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('propertyClaimantDocuments', ['as'=> 'propertyClaimantDocuments.index', 'uses' => 'PropertyClaimantDocumentsController@index']);
Route::post('propertyClaimantDocuments', ['as'=> 'propertyClaimantDocuments.store', 'uses' => 'PropertyClaimantDocumentsController@store']);
Route::get('propertyClaimantDocuments/create', ['as'=> 'propertyClaimantDocuments.create', 'uses' => 'PropertyClaimantDocumentsController@create']);
Route::put('propertyClaimantDocuments/{propertyClaimantDocuments}', ['as'=> 'propertyClaimantDocuments.update', 'uses' => 'PropertyClaimantDocumentsController@update']);
Route::patch('propertyClaimantDocuments/{propertyClaimantDocuments}', ['as'=> 'propertyClaimantDocuments.update', 'uses' => 'PropertyClaimantDocumentsController@update']);
Route::get('propertyClaimantDocuments/{id}/delete', array('as' => 'propertyClaimantDocuments.delete', 'uses' => 'PropertyClaimantDocumentsController@getDelete'));
Route::get('propertyClaimantDocuments/{id}/confirm-delete', array('as' => 'propertyClaimantDocuments.confirm-delete', 'uses' => 'PropertyClaimantDocumentsController@getModalDelete'));
Route::get('propertyClaimantDocuments/{propertyClaimantDocuments}', ['as'=> 'propertyClaimantDocuments.show', 'uses' => 'PropertyClaimantDocumentsController@show']);
Route::get('propertyClaimantDocuments/{propertyClaimantDocuments}/edit', ['as'=> 'propertyClaimantDocuments.edit', 'uses' => 'PropertyClaimantDocumentsController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('zonings', ['as'=> 'zonings.index', 'uses' => 'ZoningController@index']);
Route::post('zonings', ['as'=> 'zonings.store', 'uses' => 'ZoningController@store']);
Route::get('zonings/create', ['as'=> 'zonings.create', 'uses' => 'ZoningController@create']);
Route::put('zonings/{zonings}', ['as'=> 'zonings.update', 'uses' => 'ZoningController@update']);
Route::patch('zonings/{zonings}', ['as'=> 'zonings.update', 'uses' => 'ZoningController@update']);
Route::get('zonings/{id}/delete', array('as' => 'zonings.delete', 'uses' => 'ZoningController@getDelete'));
Route::get('zonings/{id}/confirm-delete', array('as' => 'zonings.confirm-delete', 'uses' => 'ZoningController@getModalDelete'));
Route::get('zonings/{zonings}', ['as'=> 'zonings.show', 'uses' => 'ZoningController@show']);
Route::get('zonings/{zonings}/edit', ['as'=> 'zonings.edit', 'uses' => 'ZoningController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('propertyStatuses', ['as'=> 'propertyStatuses.index', 'uses' => 'PropertyStatusController@index']);
Route::post('propertyStatuses', ['as'=> 'propertyStatuses.store', 'uses' => 'PropertyStatusController@store']);
Route::get('propertyStatuses/create', ['as'=> 'propertyStatuses.create', 'uses' => 'PropertyStatusController@create']);
Route::put('propertyStatuses/{propertyStatuses}', ['as'=> 'propertyStatuses.update', 'uses' => 'PropertyStatusController@update']);
Route::patch('propertyStatuses/{propertyStatuses}', ['as'=> 'propertyStatuses.update', 'uses' => 'PropertyStatusController@update']);
Route::get('propertyStatuses/{id}/delete', array('as' => 'propertyStatuses.delete', 'uses' => 'PropertyStatusController@getDelete'));
Route::get('propertyStatuses/{id}/confirm-delete', array('as' => 'propertyStatuses.confirm-delete', 'uses' => 'PropertyStatusController@getModalDelete'));
Route::get('propertyStatuses/{propertyStatuses}', ['as'=> 'propertyStatuses.show', 'uses' => 'PropertyStatusController@show']);
Route::get('propertyStatuses/{propertyStatuses}/edit', ['as'=> 'propertyStatuses.edit', 'uses' => 'PropertyStatusController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('banks', ['as'=> 'banks.index', 'uses' => 'BankController@index']);
Route::post('banks', ['as'=> 'banks.store', 'uses' => 'BankController@store']);
Route::get('banks/create', ['as'=> 'banks.create', 'uses' => 'BankController@create']);
Route::put('banks/{banks}', ['as'=> 'banks.update', 'uses' => 'BankController@update']);
Route::patch('banks/{banks}', ['as'=> 'banks.update', 'uses' => 'BankController@update']);
Route::get('banks/{id}/delete', array('as' => 'banks.delete', 'uses' => 'BankController@getDelete'));
Route::get('banks/{id}/confirm-delete', array('as' => 'banks.confirm-delete', 'uses' => 'BankController@getModalDelete'));
Route::get('banks/{banks}', ['as'=> 'banks.show', 'uses' => 'BankController@show']);
Route::get('banks/{banks}/edit', ['as'=> 'banks.edit', 'uses' => 'BankController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('documentShelves', ['as'=> 'documentShelves.index', 'uses' => 'DocumentShelfController@index']);
Route::post('documentShelves', ['as'=> 'documentShelves.store', 'uses' => 'DocumentShelfController@store']);
Route::get('documentShelves/create', ['as'=> 'documentShelves.create', 'uses' => 'DocumentShelfController@create']);
Route::put('documentShelves/{documentShelves}', ['as'=> 'documentShelves.update', 'uses' => 'DocumentShelfController@update']);
Route::patch('documentShelves/{documentShelves}', ['as'=> 'documentShelves.update', 'uses' => 'DocumentShelfController@update']);
Route::get('documentShelves/{id}/delete', array('as' => 'documentShelves.delete', 'uses' => 'DocumentShelfController@getDelete'));
Route::get('documentShelves/{id}/confirm-delete', array('as' => 'documentShelves.confirm-delete', 'uses' => 'DocumentShelfController@getModalDelete'));
Route::get('documentShelves/{documentShelves}', ['as'=> 'documentShelves.show', 'uses' => 'DocumentShelfController@show']);
Route::get('documentShelves/{documentShelves}/edit', ['as'=> 'documentShelves.edit', 'uses' => 'DocumentShelfController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('customerIdentities', ['as'=> 'customerIdentities.index', 'uses' => 'CustomerIdentityController@index']);
Route::post('customerIdentities', ['as'=> 'customerIdentities.store', 'uses' => 'CustomerIdentityController@store']);
Route::get('customerIdentities/create', ['as'=> 'customerIdentities.create', 'uses' => 'CustomerIdentityController@create']);
Route::put('customerIdentities/{customerIdentities}', ['as'=> 'customerIdentities.update', 'uses' => 'CustomerIdentityController@update']);
Route::patch('customerIdentities/{customerIdentities}', ['as'=> 'customerIdentities.update', 'uses' => 'CustomerIdentityController@update']);
Route::get('customerIdentities/{id}/delete', array('as' => 'customerIdentities.delete', 'uses' => 'CustomerIdentityController@getDelete'));
Route::get('customerIdentities/{id}/confirm-delete', array('as' => 'customerIdentities.confirm-delete', 'uses' => 'CustomerIdentityController@getModalDelete'));
Route::get('customerIdentities/{customerIdentities}', ['as'=> 'customerIdentities.show', 'uses' => 'CustomerIdentityController@show']);
Route::get('customerIdentities/{customerIdentities}/edit', ['as'=> 'customerIdentities.edit', 'uses' => 'CustomerIdentityController@edit']);

});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin', 'as'=>'admin.'), function () {
    Route::resource('properties', 'propertyController');
    Route::patch('properties/{properties}', ['as'=> 'properties.update', 'uses' => 'propertyController@update']);
    Route::get('properties/{id}/confirm-delete', array('as' => 'properties.confirm-delete', 'uses' => 'propertyController@getModalDelete'));
});


-

Route::group(array('prefix' => 'app/','middleware' => 'admin', 'as'=>'admin.'), function () {
    Route::post('customers/customer-erp-sync/{id}', ['as'=> 'customers.customer-erp-sync', 'uses' => 'CustomerController@syncWithErp']);
    Route::get('customers', ['as'=> 'customers.index', 'uses' => 'CustomerController@index']);
    Route::post('customers', ['as'=> 'customers.store', 'uses' => 'CustomerController@store']);
    Route::get('customers/create', ['as'=> 'customers.create', 'uses' => 'CustomerController@create']);
    Route::put('customers/{customers}', ['as'=> 'customers.update', 'uses' => 'CustomerController@update']);
    Route::patch('customers/{customers}', ['as'=> 'customers.update', 'uses' => 'CustomerController@update']);
    Route::get('customers/{id}/delete', array('as' => 'customers.delete', 'uses' => 'CustomerController@getDelete'));
    Route::get('customers/{id}/confirm-delete', array('as' => 'customers.confirm-delete', 'uses' => 'CustomerController@getModalDelete'));
    Route::get('customers/{customers}', ['as'=> 'customers.show', 'uses' => 'CustomerController@show']);
    Route::get('customers/{customers}/edit', ['as'=> 'customers.edit', 'uses' => 'CustomerController@edit']);
    Route::get('customer-popup/{id}', ['as'=> 'customers.popup', 'uses' => 'CustomerController@popUp']);


});

Route::group(array('prefix' => 'app/','middleware' => 'admin', 'as'=>'app.'), function () {
    Route::post("application-wizard", ['as'=> 'application-wizard', 'uses' => 'Transaction\TransactionController@processApplicationWizard']);
    Route::post("finish-application-wizard", ['as'=> 'finish-application-wizard', 'uses' => 'Transaction\TransactionController@finishApplicationWizard']);
});

Route::group(array('prefix' => 'admin/','middleware' => 'admin', 'as'=>'admin.'), function () {
    Route::get('change-ownership', ['as'=> 'change-ownership.index', 'uses' => 'ChangeOwnershipController@index']);
    Route::post('load-property', ['as'=> 'load-property.load_property', 'uses' => 'ChangeOwnershipController@load_property']);
    Route::post('load_buyer_model', ['as'=> 'load_buyer_model.load_buyer_detail', 'uses' => 'ChangeOwnershipController@load_buyer_detail']);
    Route::post('load-customer', ['as'=> 'load-customer.load_customer', 'uses' => 'ChangeOwnershipController@load_customer']);
    Route::post('post-ownership', ['as'=> 'post-ownership.post_ownership', 'uses' => 'ChangeOwnershipController@post_ownership']);
    Route::post('co-summary-view', ['as'=> 'co-summary-view', 'uses' => 'ChangeOwnershipController@getSummaryView']);
    Route::post('payment', ['as'=> 'payment', 'uses' => 'ChangeOwnershipController@payment']);
    Route::post('post-payment', ['as'=> 'post-payment', 'uses' => 'ChangeOwnershipController@postPayment']);
});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {

Route::get('ledgerScannings', ['as'=> 'ledgerScannings.index', 'uses' => 'LedgerScanningController@index']);
Route::post('ledgerScannings', ['as'=> 'ledgerScannings.store', 'uses' => 'LedgerScanningController@store']);
Route::get('ledgerScannings/create', ['as'=> 'ledgerScannings.create', 'uses' => 'LedgerScanningController@create']);
Route::put('ledgerScannings/{ledgerScannings}', ['as'=> 'ledgerScannings.update', 'uses' => 'LedgerScanningController@update']);
Route::patch('ledgerScannings/{ledgerScannings}', ['as'=> 'ledgerScannings.update', 'uses' => 'LedgerScanningController@update']);
Route::get('ledgerScannings/{id}/delete', array('as' => 'ledgerScannings.delete', 'uses' => 'LedgerScanningController@getDelete'));
Route::get('ledgerScannings/{id}/confirm-delete', array('as' => 'ledgerScannings.confirm-delete', 'uses' => 'LedgerScanningController@getModalDelete'));
Route::get('ledgerScannings/{ledgerScannings}', ['as'=> 'ledgerScannings.show', 'uses' => 'LedgerScanningController@show']);
Route::get('ledgerScannings/{ledgerScannings}/edit', ['as'=> 'ledgerScannings.edit', 'uses' => 'LedgerScanningController@edit']);

});

Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::get('role.checklist', ['as'=> 'role.checklists', 'uses' => 'ChecklistController@index']);

    Route::get('checklists', ['as'=> 'checklists.index', 'uses' => 'ChecklistController@index']);
    Route::post('checklists', ['as'=> 'checklists.store', 'uses' => 'ChecklistController@store']);
    Route::get('checklists/create', ['as'=> 'checklists.create', 'uses' => 'ChecklistController@create']);
    Route::put('checklists/{checklists}', ['as'=> 'checklists.update', 'uses' => 'ChecklistController@update']);
    Route::patch('checklists/{checklists}', ['as'=> 'checklists.update', 'uses' => 'ChecklistController@update']);
    Route::get('checklists/{id}/delete', array('as' => 'checklists.delete', 'uses' => 'ChecklistController@getDelete'));
    Route::get('checklists/{checklists}', ['as'=> 'checklists.show', 'uses' => 'ChecklistController@show']);
    Route::get('checklists/{checklists}/edit', ['as'=> 'checklists.edit', 'uses' => 'ChecklistController@edit']);
    Route::get('checklists/{id}/confirm-delete', array('as' => 'checklists.confirm-delete', 'uses' => 'ChecklistController@getModalDelete'));
});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::get('propertyOwnerships', ['as'=> 'propertyOwnerships.index', 'uses' => 'PropertyOwnershipController@index']);
    Route::post('propertyOwnerships', ['as'=> 'propertyOwnerships.store', 'uses' => 'PropertyOwnershipController@store']);
    Route::get('propertyOwnerships/create', ['as'=> 'propertyOwnerships.create', 'uses' => 'PropertyOwnershipController@create']);
    Route::put('propertyOwnerships/{propertyOwnerships}', ['as'=> 'propertyOwnerships.update', 'uses' => 'PropertyOwnershipController@update']);
    Route::patch('propertyOwnerships/{propertyOwnerships}', ['as'=> 'propertyOwnerships.update', 'uses' => 'PropertyOwnershipController@update']);
    Route::get('propertyOwnerships/{id}/delete', array('as' => 'propertyOwnerships.delete', 'uses' => 'PropertyOwnershipController@getDelete'));
    Route::get('propertyOwnerships/{propertyOwnerships}', ['as'=> 'propertyOwnerships.show', 'uses' => 'PropertyOwnershipController@show']);
    Route::get('propertyOwnerships/{propertyOwnerships}/edit', ['as'=> 'propertyOwnerships.edit', 'uses' => 'PropertyOwnershipController@edit']);
    Route::get('propertyOwnerships/{id}/confirm-delete', array('as' => 'propertyOwnerships.confirm-delete', 'uses' => 'PropertyOwnershipController@getModalDelete'));
});


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    Route::resource('notaries', 'NotaryController');
    Route::patch('notaries/{notaries}', ['as'=> 'notaries.update', 'uses' => 'NotaryController@update']);
    Route::get('notaries/{id}/confirm-delete', array('as' => 'notaries.confirm-delete', 'uses' => 'NotaryController@getModalDelete'));
});



Route::post('audit-details', array('as' => 'audit-details', 'uses' => 'AuditController@userAuditDetails'));

Route::post('customer-audit-details', array('as' => 'audit-details', 'uses' => 'AuditController@customerAuditDetails'));


Route::group(array('prefix' => 'admin/', 'middleware' => 'admin','as'=>'admin.'), function () {
    
    Route::get('permission/create', array('as' => 'permission.create', 'uses' => 'PermissionController@roleList'));
    Route::post('permission/list', array('as' => 'permission.list', 'uses' => 'PermissionController@permissionList'));
    Route::post('permission/update', array('as' => 'permission.update', 'uses' => 'PermissionController@permissionUpdate'));
});