<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->delete(); // Using truncate function so all info will be cleared when re-seeding.
         //DB::table('services')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`services` (`id`, `name`, `code`) VALUES
            (1,'Change Ownership', 'CO-FEE'),
            (2,'New Registration', 'NR-FEE'),
            (3,'Conversion', 'CR-FEE');";
        DB::unprepared($statement);
    }
}
