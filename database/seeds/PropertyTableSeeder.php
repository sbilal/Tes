<?php

use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('property')->delete();
        
        \DB::table('property')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parcel_id' => 'HO-112-12445',
                'status' => 'APPROVED',
                'migration_status' => 'MIGRATED',
                'north' => 'Road',
                'south' => 'Street',
                'east' => 'office',
                'west' => 'house',
                'barcode' => '99999',
                'deed_number' => '999999',
                'legacy_file_number' => '1',
                'street_number' => '99',
                'street_name' => 'Smith St',
                'address' => '99 Smith St',
                'district' => 'Loirb',
                'sub_division' => 'Uidi',
                'area' => 'Rober',
                'zone' => '23',
                'purpose_of_use' => '23',
                'created_at' => '2018-07-03 07:08:28',
                'updated_at' => '2018-07-03 23:06:55',
            ),
            1 => 
            array (
                'id' => 2,
                'parcel_id' => 'HO-112-12444',
                'status' => 'APPROVED',
                'migration_status' => 'MIGRATED',
                'north' => 'Road',
                'south' => 'Street',
                'east' => 'Hospital',
                'west' => 'Office',
                'barcode' => '99999',
                'deed_number' => '123456',
                'legacy_file_number' => '2',
                'street_number' => '1',
                'street_name' => '1st Street',
                'address' => 'Ave of New York',
                'district' => 'Vistral',
                'sub_division' => 'Socket',
                'area' => 'North Suburbs',
                'zone' => 'Residential',
                'purpose_of_use' => 'Residential',
                'created_at' => '2018-07-03 07:41:41',
                'updated_at' => '2018-07-03 23:07:05',
            ),
        ));
        
        
    }
}