<?php

use Illuminate\Database\Seeder;

class PropertyOwnershipHistoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('property_ownership_history')->delete();
        
        \DB::table('property_ownership_history')->insert(array (
            0 => 
            array (
                'id' => 1,
                'property_id' => 1,
                'status' => 'Sold',
                'purchase_date' => '2001-01-01',
                'purchase_price' => '100.00',
                'sale_date' => '2002-01-01',
                'sale_price' => '200.00',
                'service_id' => 1,
                'owner_id' => 4,
                'primary_owner' => 'Yes',
                'created_at' => '2001-01-01 00:00:00',
                'updated_at' => '2001-01-01 00:00:00',
            ),
        ));
        
        
    }
}