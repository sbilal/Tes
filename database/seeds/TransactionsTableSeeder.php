<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'service_id' => '1',
                'application_date' => '2018-07-01',
                'buyer_id' => '1',
                'seller_id' => '2',
                'property_id' => '1',
                'settlement_date' => '2018-07-20',
                'settlement_amount' => '4000.00',
                'status' => 'IN PROGRESS',
                'workflow_status' => 'IN PROGRESS',
                'workflow_instance_id' => NULL,                
                'assigned_to' => '1',
                'payment_status' => NULL,
                'transaction_reference_number' => 'TRN-1010',
                'payment_amount' => NULL,
                'payment_method' => NULL,
                'payment_customer' => NULL,
                'payment_date' => NULL,
                'payment_reference_number' => NULL,
                'payment_customer_mobile' => NULL,                
                'created_at' => '2018-07-04 03:24:17',
                'updated_at' => '2018-07-04 03:24:17',
            ),
        ));
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 2,
                'service_id' => '1',
                'application_date' => '2018-07-01',
                'buyer_id' => '1',
                'seller_id' => '2',
                'property_id' => '1',
                'settlement_date' => '2018-07-20',
                'settlement_amount' => '4000.00',
                'status' => 'IN PROGRESS',
                'workflow_status' => 'IN PROGRESS',
                'workflow_instance_id' => NULL,                    
                'assigned_to' => '8',
                'payment_amount' => NULL,
                'payment_status' => NULL,
                'payment_method' => NULL,
                'payment_customer' => NULL,
                'payment_reference_number' => NULL,
                'payment_customer_mobile' => NULL,   
                'payment_date' => NULL,
                'transaction_reference_number' => 'TRN-2010',
                'created_at' => '2018-07-04 03:24:17',
                'updated_at' => '2018-07-04 03:24:17',
            ),
        ));
    }
}