<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slug' => 'admin',
                'name' => 'Admin',
                'permissions' => '{"admin":1}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            1 => 
            array (
                'id' => 2,
                'slug' => 'user',
                'name' => 'User',
                'permissions' => NULL,
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            2 => 
            array (
                'id' => 3,
                'slug' => 'teller',
                'name' => 'Teller',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 13:15:37',
            ),
            3 => 
            array (
                'id' => 4,
                'slug' => 'district-officer',
                'name' => 'District-Officer',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            4 => 
            array (
                'id' => 5,
                'slug' => 'district-geometer',
                'name' => 'District-Geometer',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            5 => 
            array (
                'id' => 6,
                'slug' => 'headoffice-officer',
                'name' => 'HeadOffice-officer',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            6 => 
            array (
                'id' => 7,
                'slug' => 'headoffice-geometer',
                'name' => 'HeadOffice-Geometer',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            7 => 
            array (
                'id' => 8,
                'slug' => 'notary',
                'name' => 'Notary',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            8 => 
            array (
                'id' => 9,
                'slug' => 'court',
                'name' => 'Court',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
            9 => 
            array (
                'id' => 10,
                'slug' => 'executive',
                'name' => 'Executive',
                'permissions' => '{"workflow.workflow.unclaimedtasks":true,"workflow.dt-get-my-tasks":true,"workflow.dt-get-tasks-to-claim":true,"workflow.dt-application-history":true,"workflow.reject-application":true,"workflow.approve-application":true,"workflow.view-application":true,"workflow.release-task":true,"workflow.claim-task":true,"workflow.initiate":true}',
                'created_at' => '2018-08-19 00:30:42',
                'updated_at' => '2018-08-19 00:30:42',
            ),
        ));
        
        
    }
}