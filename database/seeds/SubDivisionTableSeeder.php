<?php

use Illuminate\Database\Seeder;

class SubDivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('sub_division')->delete();
        
        \DB::table('sub_division')->insert(array (
            0 => 
            array (
                'id' => 1,
                'DistrictId' => 1,
                'Name'       => 'Almis',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
                
            ),
            1 => 
            array (
                'id' => 2,
                'DistrictId' => 1,
                'Name'       => 'Cayngal',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            2 => 
            array (
                'id' => 3,
                'DistrictId' => 1,
                'Name'       => 'Durya',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            3 => 
            array (
               'id' => 4,
                'DistrictId' => 1,
                'Name'       => 'Goljano',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),

            4 => 
            array (
               'id' => 5,
                'DistrictId' => 2,
                'Name'       => '18-May',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            5 => 
            array (
               'id' => 6,
                'DistrictId' => 2,
                'Name'       => 'AbdiLidan',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),

            6 => 
            array (
                'id' => 7,
                'DistrictId' => 2,
                'Name'       => 'F.Nuur',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
                
            ),
            7 => 
            array (
                'id' => 8,
                'DistrictId' => 2,
                'Name'       => 'M.Ali',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            8 => 
            array (
                'id' => 9,
                'DistrictId' => 2,
                'Name'       => 'Sh.MDuaalleh',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            9 => 
            array (
               'id' => 10,
                'DistrictId' => 3,
                'Name'       => 'M.Xarbi',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            10 => 
            array (
               'id' => 11,
                'DistrictId' => 3,
                'Name'       => 'Sh.Madar',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),

            11 => 
            array (
               'id' => 12,
                'DistrictId' => 3,
                'Name'       => 'Sh.Nuur',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),

            12 => 
            array (
               'id' => 13,
                'DistrictId' => 3,
                'Name'       => 'Sh.Yusuf',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            13 => 
            array (
               'id' => 14,
                'DistrictId' => 3,
                'Name'       => 'W.Salan',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            14 => 
            array (
               'id' => 15,
                'DistrictId' => 4,
                'Name'       => 'GuulAlla',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            15 => 
            array (
               'id' => 16,
                'DistrictId' => 4,
                'Name'       => 'Lixle',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),

            16 => 
            array (
               'id' => 17,
                'DistrictId' => 4,
                'Name'       => 'X.Faarax',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),

            17 => 
            array (
               'id' => 18,
                'DistrictId' => 4,
                'Name'       => 'XeroAwr',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            18 => 
            array (
               'id' => 19,
                'DistrictId' => 5,
                'Name'       => 'B.Duuray',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            19 =>

             array (
               'id' => 20,
                'DistrictId' => 5,
                'Name'       => 'J.Wayn',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            20 =>
            array (
               'id' => 21,
                'DistrictId' => 5,
                'Name'       => 'M.Mooge',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            21 =>
            array (
               'id' => 22,
                'DistrictId' => 5,
                'Name'       => 'Q.Dheer',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            22 =>
            array (
               'id' => 23,
                'DistrictId' => 5,
                'Name'       => 'Sh.Shukri',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
        ));
    }
}
