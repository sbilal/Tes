<?php

use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends DatabaseSeeder {

	public function run()
	{
                //DB::statement('TRUNCATE users CASCADE');
                //DB::statement('TRUNCATE roles CASCADE');
                //DB::statement('TRUNCATE role_users CASCADE');
                //DB::statement('TRUNCATE activations CASCADE');
                
		DB::table('users')->delete(); // Using truncate function so all info will be cleared when re-seeding.
		DB::table('roles')->delete();
		DB::table('role_users')->delete();
		DB::table('activations')->delete();

		$admin = Sentinel::registerAndActivate(array(
			'email'       => 'admin@admin.com',
			'password'    => "admin",
			'first_name'  => 'John',
			'last_name'   => 'Doe',
		));
                
		$teller = Sentinel::registerAndActivate(array(
			'email'       => 'teller@coh.com',
			'password'    => "teller",
			'first_name'  => 'Teller',
			'last_name'   => 'Coh',
                        'permissions' => ["workflow.view-application" => true, "admin.transaction.document.delete" => true, "admin.transaction.comments.delete" => true, "admin.confirm-delete" => true, "admin.transactions.document.store" => true, "admin.transactions.comments.store" =>true, "admin.transactions.comments.store" => true, "admin.customers.popup" => true, "workflow.process-application" =>true, "workflow.release-task"=>true, "workflow.dt-get-my-tasks"=> true, "workflow.claim-task"=>true, "workflow.dt-get-tasks-to-claim" => true, "admin.api-get-recent-transactions"=> true, "workflow.initiate"=>true,"admin.transactions.index"=>true, "admin.transactions.show"=> true, "app.finish-application-wizard"=>true,"app.application-wizard"=>true, "teller.dashboard"=> true, "admin.customers.index"=>true, "admin.properties.index"=>true , "admin.customers.create"=> true, "admin.customers.show" => true, "admin.properties.show"=>true, "admin.property-modal"=>true, "admin.property-datatable"=>true, "admin.property-main-partial"=>true, "admin.api-get-customers"=>true, "admin.api-get-notaries"=>true, "admin.change-ownership.index"=>true, "admin.api-get-customer"=> true, "admin.api-get-notary"=>true, "admin.co-summary-view"=>true, "admin.create-transaction"=>true, "admin.invoice.gis-invoice"=>true, "admin.transaction.submit-payment"=>true],
		));                

		$dofficer = Sentinel::registerAndActivate(array(
			'email'       => 'dofficer@coh.com',
			'password'    => "dofficer",
			'first_name'  => 'District',
			'last_name'   => 'Officer',
                        'permissions' => ["district-officer.dashboard"=>true],                    
		));    
                
		$hoofficer = Sentinel::registerAndActivate(array(
			'email'       => 'hoofficer@coh.com',
			'password'    => "hoofficer",
			'first_name'  => 'HeadOffice',
			'last_name'   => 'Officer',
                        'permissions' => ["headoffice-officer.dashboard"=>true],
		));                 
                
		$dgeometer = Sentinel::registerAndActivate(array(
			'email'       => 'dgeometer@coh.com',
			'password'    => "dgeometer",
			'first_name'  => 'District',
			'last_name'   => 'Geometer',
                        'permissions' => ["district-geometer.dashboard"=>true],
    		));                 
                
		$hogeometer = Sentinel::registerAndActivate(array(
			'email'       => 'hogeometer@coh.com',
			'password'    => "hogeometer",
			'first_name'  => 'HeadOffice',
			'last_name'   => 'Geometer',
                        'permissions' => ["headoffice-geometer.dashboard"=>true],
		));                 
                
		$court = Sentinel::registerAndActivate(array(
			'email'       => 'court@coh.com',
			'password'    => "court",
			'first_name'  => 'Court',
			'last_name'   => 'External',
                        'permissions' => ["court.dashboard"=>true],
		));                 
                
		$notary1 = Sentinel::registerAndActivate(array(
			'email'       => 'notary1@coh.com',
			'password'    => "notary",
			'first_name'  => 'Notary1',
			'last_name'   => 'External',
                        'permissions' => ["notary.dashboard"=>true],
		));             
                
		$notary2 = Sentinel::registerAndActivate(array(
			'email'       => 'notary2@coh.com',
			'password'    => "notary",
			'first_name'  => 'Notary2',
			'last_name'   => 'External',
                        'permissions' => ["notary.dashboard"=>true],
		));                 
                
		$executive = Sentinel::registerAndActivate(array(
			'email'       => 'executive@coh.com',
			'password'    => "executive",
			'first_name'  => 'Executive',
			'last_name'   => 'Coh',
                        'permissions' => ["executive.dashboard"=>true],
		));                                 
                
		$delivery = Sentinel::registerAndActivate(array(
			'email'       => 'delivery@coh.com',
			'password'    => "delivery",
			'first_name'  => 'Delivery',
			'last_name'   => 'Coh',
                        'permissions' => ["delivery.dashboard"=>true],
		));                  

                $adminRole = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Admin',
			'slug' => 'admin',
			'permissions' => array('admin' => 1),
		]);

                $userRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'User',
			'slug'  => 'user',
		]);
                
                $tellerRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Teller',
			'slug'  => 'teller',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]);                

                $doRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'District-Officer',
			'slug'  => 'district-officer',
                        'permissions' => ["user.dashboard" => true, "workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],
		]);                

                $dgRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'District-Geometer',
			'slug'  => 'district-geometer',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],
		]);                
                
                $hooRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'HeadOffice-officer',
			'slug'  => 'headoffice-officer',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]);                

                $hogRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'HeadOffice-Geometer',
			'slug'  => 'headoffice-geometer',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]); 
                             

                $notaryRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Notary',
			'slug'  => 'notary',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]);                 
                
                $courtRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Court',
			'slug'  => 'court',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]);                 
                
                $execRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Executive',
			'slug'  => 'executive',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]);     
                
                $deliveryRole = Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Delivery',
			'slug'  => 'delivery',
                        'permissions' => ["user.dashboard" => true,"workflow.workflow.unclaimedtasks"=> true,"workflow.dt-get-my-tasks"=> true,"workflow.dt-get-tasks-to-claim"=> true,"workflow.dt-application-history"=> true,"workflow.reject-application"=> true,"workflow.approve-application"=> true,"workflow.view-application"=> true,"workflow.release-task"=> true,"workflow.claim-task"=> true,"workflow.initiate"=> true],                    
		]);                 
                
                $admin->roles()->attach($adminRole);
                $teller->roles()->attach($tellerRole);
                $dofficer->roles()->attach($doRole);
                $dgeometer->roles()->attach($dgRole);
                $hoofficer->roles()->attach($hooRole);
                $hogeometer->roles()->attach($hogRole);
                $court->roles()->attach($courtRole);
                $notary1->roles()->attach($notaryRole);
                $notary2->roles()->attach($notaryRole);
                $executive->roles()->attach($execRole);
                $delivery->roles()->attach($deliveryRole);

		$this->command->info('Admin User created with username admin@admin.com and password admin');
                $this->command->info('Teller User created with username teller@coh.com and password teller');
                $this->command->info('District Officer User created with username dofficer@coh.com and password dofficer');
                $this->command->info('District Geometer User created with username dgeometer@coh.com and password dgeomter');
                $this->command->info('Headoffice officer created with username hooofficer.com and password hooficer');
                $this->command->info('HeadOffice Geometer User created with username hogeomter@coh.com and password hogeometer');
                $this->command->info('Notary1 created with username notary1@coh.com and password notary');
                $this->command->info('Notary2 created with username notary2@coh.com and password notary');
                $this->command->info('Court User created with username court@coh.com and password court');
                $this->command->info('Executive User created with username executive@coh.com and password executive');
                $this->command->info('Delivery User created with username delivery@coh.com and password delivery');
	}

}