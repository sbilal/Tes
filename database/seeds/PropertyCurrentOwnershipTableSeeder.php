<?php

use Illuminate\Database\Seeder;

class PropertyCurrentOwnershipTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('property_current_ownership')->delete();
        
        \DB::table('property_current_ownership')->insert(array (
            0 => 
            array (
                'id' => 1,
                'property_id' => 1,
                'status' => 'Owner',
                'purchase_date' => '2010-01-01',
                'purchase_price' => '100.00',
                'service_id' => 1,
                'owner_id' => 1,
                'primary_owner' => 'Yes',
                'created_at' => '2018-01-01 00:00:00',
                'updated_at' => '2018-01-01 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'property_id' => 2,
                'status' => 'Owner',
                'purchase_date' => '2011-10-10',
                'purchase_price' => '2000.00',
                'service_id' => 2,
                'owner_id' => 5,
                'primary_owner' => 'Yes',
                'created_at' => '2018-01-01 00:00:00',
                'updated_at' => '2018-01-01 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'property_id' => 1,
                'status' => 'Owner',
                'purchase_date' => '2010-01-01',
                'purchase_price' => '100.00',
                'service_id' => 1,
                'owner_id' => 2,
                'primary_owner' => 'No',
                'created_at' => '2018-01-01 00:00:00',
                'updated_at' => '2018-01-01 00:00:00',
            ),
        ));
        
        
    }
}