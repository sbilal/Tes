<?php

use Illuminate\Database\Seeder;

class ChecklistsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('checklists')->delete();
        
        \DB::table('checklists')->insert(array (
            0 => 
            array (
                'id' => 4,
                'name' => 'Customer Driver License',
                'description' => 'Customer Driver License',
                'role_id' => 3,
                'created_at' => '2018-08-06 23:58:36',
                'updated_at' => '2018-08-06 23:58:36',
            ),
            1 => 
            array (
                'id' => 5,
                'name' => 'Lien Verified',
                'description' => 'Lien Verified',
                'role_id' => 4,
                'created_at' => '2018-08-07 00:46:58',
                'updated_at' => '2018-08-07 00:46:58',
            ),
            2 => 
            array (
                'id' => 6,
                'name' => 'Parcel Verified',
                'description' => 'Parcel Verified',
                'role_id' => 5,
                'created_at' => '2018-08-07 00:50:18',
                'updated_at' => '2018-08-07 00:50:18',
            ),
            3 => 
            array (
                'id' => 7,
                'name' => 'Lien Verified',
                'description' => 'Lien Verified',
                'role_id' => 6,
                'created_at' => '2018-08-07 00:50:36',
                'updated_at' => '2018-08-07 00:50:36',
            ),
            4 => 
            array (
                'id' => 8,
                'name' => 'Parcel Verified',
                'description' => 'Parcel Verified',
                'role_id' => 7,
                'created_at' => '2018-08-07 00:50:46',
                'updated_at' => '2018-08-07 00:50:46',
            ),
            5 => 
            array (
                'id' => 9,
                'name' => 'Data Collected',
                'description' => 'Data Collected',
                'role_id' => 8,
                'created_at' => '2018-08-07 00:50:57',
                'updated_at' => '2018-08-07 00:50:57',
            ),
            6 => 
            array (
                'id' => 10,
                'name' => 'Witness Verified',
                'description' => 'Witness Verified',
                'role_id' => 8,
                'created_at' => '2018-08-07 00:51:08',
                'updated_at' => '2018-08-07 00:51:08',
            ),
            7 => 
            array (
                'id' => 11,
                'name' => 'Tax Document Verified',
                'description' => 'Tax Document Verified',
                'role_id' => 9,
                'created_at' => '2018-08-07 00:51:21',
                'updated_at' => '2018-08-07 00:51:21',
            ),
        ));
        
        
    }
}