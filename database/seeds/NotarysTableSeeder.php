<?php

use Illuminate\Database\Seeder;

class NotarysTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('notarys')->delete();
        
        \DB::table('notarys')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'James',
                'second_name' => 'Bond',
                'third_name' => '007',
                'fourth_name' => 'Notary',
                'address' => '90',
                'contact_number' => '10001000',
                'email' => 'notary1@coh.com',
                'district' => 'Vistral',
                'sub_division' => 'Uidi',
                'notary_number' => 3,
                'notary_expiry_date' => '2020-10-10',
                'created_at' => '2018-07-06 06:16:38',
                'updated_at' => '2018-07-06 06:16:38',
                'login_id' =>8,
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'Tom',
                'second_name' => 'Cruise',
                'third_name' => '300',
                'fourth_name' => 'Notary',
                'address' => '90',
                'contact_number' => '1001000',
                'email' => 'notary2@coh.com',
                'district' => 'Vistral',
                'sub_division' => '1',
                'notary_number' => 11,
                'notary_expiry_date' => '2020-12-31',
                'created_at' => '2018-07-06 06:17:43',
                'updated_at' => '2018-07-06 06:17:43',
                'login_id' =>9,
            ),
        ));
        
        
    }
}