<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(DatatablesSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(PropertyTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(NotarysTableSeeder::class);
        $this->call(PropertyCurrentOwnershipTableSeeder::class);
        $this->call(PropertyOwnershipHistoryTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(DistrictTableSeeder::class);
        $this->call(SubDivisionTableSeeder::class);
        $this->call(DocumentTypeTableSeeder::class);
        //$this->call(UsersTableSeeder::class);
        $this->call(ChecklistsTableSeeder::class);
    }
}
