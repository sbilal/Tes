<?php

use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('district')->delete();
        
        \DB::table('district')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '26 June',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
                
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Ahmed Dhegeh',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            2 => 
            array (
               'id' => 3,
                'name' => 'Gaan Libaah',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            3 => 
            array (
               'id' => 4,
                'name' => 'Kood Buur',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
            4 => 
            array (
               'id' => 5,
                'name' => 'Mohamoud Haybe',
                'created_at' => '2018-07-01 05:50:43',
                'updated_at' => '2018-07-03 14:04:57',
            ),
        ));
    }
}
