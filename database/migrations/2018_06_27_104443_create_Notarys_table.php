<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotarysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Notarys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('third_name')->nullable();
            $table->string('fourth_name')->nullable();
            $table->text('address')->nullable();
            $table->text('contact_number')->nullable();
            $table->text('email')->nullable();
            $table->string('district')->nullable();
            $table->string('sub_division')->nullable();
            $table->integer('notary_number')->nullable();
            $table->date('notary_expiry_date')->nullable();
            $table->integer('login_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Notarys');
    }
}
