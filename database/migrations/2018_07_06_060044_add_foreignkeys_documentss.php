<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysDocumentss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentss', function (Blueprint $table) {
            $table->integer('property_id')->unsigned()->change();
            $table->integer('document_type_id')->unsigned()->change();
            $table->integer('customer_id')->unsigned()->change();
            $table->foreign('property_id')->references('id')->on('property');
            $table->foreign('document_type_id')->references('id')->on('document_type');
            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentss', function (Blueprint $table) {
            $table->dropColumn('property_id');
            $table->dropColumn('document_type_id');
            $table->dropColumn('customer_id');
            $table->dropForeign('property_ownership_property_id_foreign');
            $table->dropForeign('property_ownership_document_type_id_foreign');
            $table->dropForeign('property_ownership_customer_id_foreign');
        });
    }
}
