<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parcel_id')->nullable();
            $table->string('status')->nullable();
            $table->string('migration_status')->nullable();
            $table->string('north')->nullable();
            $table->string('south')->nullable();
            $table->string('east')->nullable();
            $table->string('west')->nullable();
            $table->string('barcode')->nullable();
            $table->string('deed_number')->nullable();
            $table->string('legacy_file_number')->nullable();
            $table->string('street_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('address')->nullable();
            $table->string('district')->nullable();
            $table->string('sub_division')->nullable();
            $table->string('area')->nullable();
            $table->string('zone')->nullable();
            $table->string('purpose_of_use')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propertys');
    }
}
