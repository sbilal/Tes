<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysPropertyOwnershipApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_ownership_application', function (Blueprint $table) {
            $table->integer('property_id')->unsigned()->change();
            $table->integer('service_id')->unsigned()->change();
            $table->integer('owner_id')->unsigned()->change();
            $table->foreign('property_id')->references('id')->on('property');
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('owner_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_ownership_application', function (Blueprint $table) {
            $table->dropColumn('property_id');
            $table->dropColumn('service_id');
            $table->dropColumn('owner_id');
            $table->dropForeign('property_ownership_property_id_foreign');
            $table->dropForeign('property_ownership_service_id_foreign');
            $table->dropForeign('property_ownership_owner_id_foreign');
        });
    }
}
