<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('third_name')->nullable();
            $table->string('fourth_name')->nullable();
            $table->string('mobile_1')->nullable();
            $table->string('mobile_2')->nullable();
            $table->string('customer_type')->nullable();
            $table->string('street_number')->nullable();
            $table->string('street_name')->nullable();
            $table->string('address')->nullable();
            $table->string('district')->nullable();
            $table->string('sub_division')->nullable();
            $table->string('area')->nullable();
            $table->string('zone')->nullable();
            $table->string('gender')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('email')->nullable();            
            $table->string('noor_account_number')->nullable();
            $table->string('mother_first_name')->nullable();
            $table->string('mother_second_name')->nullable();
            $table->string('mother_third_name')->nullable();
            $table->string('mother_fourth_name')->nullable();
            $table->string('father_first_name')->nullable();
            $table->string('father_second_name')->nullable();
            $table->string('father_third_name')->nullable();
            $table->string('father_fourth_name')->nullable();
            $table->string('erp_customer_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Customers');
    }
}
