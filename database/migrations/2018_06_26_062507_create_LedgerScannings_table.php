<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLedgerScanningsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledgerscannings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->string('registration_no')->nullable();
            $table->string('magaca')->nullable();
            $table->string('degree')->nullable();
            $table->string('waxx')->nullable();
            $table->string('xaag')->nullable();
            $table->string('quab')->nullable();
            $table->string('xof')->nullable();
            $table->string('cabbie')->nullable();
            $table->date('gr_date')->nullable();
            $table->string('gr_number')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('orign_no')->nullable();
            $table->string('file_no')->nullable();
            $table->string('teysaro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ledgerscannings');
    }
}
