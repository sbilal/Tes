<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service_id')->nullable();
            $table->date('application_date')->nullable();
            $table->string('buyer_id')->nullable();
            $table->string('seller_id')->nullable();
            $table->string('property_id')->nullable();
            $table->date('settlement_date')->nullable();
            $table->decimal('settlement_amount')->nullable();
            $table->string('status')->nullable();
            $table->string('workflow_status')->nullable();
            $table->string('notary_id')->nullable();
            $table->string('assigned_to')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('payment_date')->nullable();
            $table->string('payment_amount')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_customer')->nullable();
            $table->string('payment_reference_number')->nullable();
            $table->string('payment_customer_mobile')->nullable();
            $table->string('transaction_reference_number');			
            $table->string('erp_invoice_id')->nullable();            
            $table->string('erp_payment_id')->nullable();            
            $table->integer('workflow_instance_id')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Transactions');
    }
}
