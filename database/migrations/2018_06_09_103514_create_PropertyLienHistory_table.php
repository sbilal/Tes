<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyLienHistoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_lien_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('PropertyOwnershipId')->nullable();
            $table->integer('BankId')->nullable();
            $table->float('LoanAmount')->nullable();
            $table->date('LoanDate')->nullable();
            $table->date('ReleaseDate')->nullable();
            $table->string('Status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_lien_history');
    }
}
