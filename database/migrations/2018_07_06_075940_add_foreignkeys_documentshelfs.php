<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysDocumentshelfs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentshelfs', function (Blueprint $table) {
            $table->integer('document_id')->unsigned()->change();
            
            $table->foreign('document_id')->references('id')->on('documentss');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentshelfs', function (Blueprint $table) {
            $table->dropColumn('document_id');
            
            $table->dropForeign('documentshelfs_document_id_foreign');
        });
    }
}
