<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyOwnershipsHistoryTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_ownership_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->string('status')->nullable();
            $table->date('purchase_date')->nullable();
            $table->decimal('purchase_price')->nullable();
            $table->date('sale_date')->nullable();
            $table->decimal('sale_price')->nullable();
            $table->integer('service_id')->nullable();
            $table->integer('owner_id')->nullable();
            $table->string('primary_owner')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_ownership_history');
    }
}
