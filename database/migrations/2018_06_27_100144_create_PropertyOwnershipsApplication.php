<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyOwnershipsApplication extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_ownership_application', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable();
            $table->date('settlement_date')->nullable();
            $table->decimal('settlement_amount')->nullable();
            $table->integer('service_id')->nullable();
            $table->integer('owner_id')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->string('primary_owner')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_ownership_application');
    }
}
