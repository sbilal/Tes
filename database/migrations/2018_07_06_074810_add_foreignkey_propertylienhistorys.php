<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeyPropertylienhistorys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propertylienhistorys', function (Blueprint $table) {
            $table->integer('property_ownership_id')->unsigned()->change();
            
            $table->foreign('property_ownership_id')->references('id')->on('property');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('propertylienhistorys', function (Blueprint $table) {
            $table->dropColumn('property_ownership_id');
            
            $table->dropForeign('propertylienhistorys_property_ownership_id_foreign');
            
        });
    }
}
