<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertylienhistorysTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertylienhistorys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_ownership_id')->nullable();
            $table->string('bank_id')->nullable();
            $table->decimal('loan_amount')->nullable();
            $table->date('loan_date')->nullable();
            $table->date('release_date')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propertylienhistorys');
    }
}
