<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysPropertyclaimantdocumentss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propertyclaimantdocumentss', function (Blueprint $table) {
            $table->integer('document_id')->unsigned()->change();
            $table->integer('property_id')->unsigned()->change();
            $table->integer('customer_id')->unsigned()->change();
            $table->foreign('document_id')->references('id')->on('documentss');
            $table->foreign('property_id')->references('id')->on('property');
            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('propertyclaimantdocumentss', function (Blueprint $table) {
            $table->dropColumn('document_id');
            $table->dropColumn('property_id');
            $table->dropColumn('customer_id');
            $table->dropForeign('propertyclaimantdocumentss_document_id_foreign');
            $table->dropForeign('propertyclaimantdocumentss_property_id_foreign');
            $table->dropForeign('propertyclaimantdocumentss_customer_id_foreign');
        });
    }
}
