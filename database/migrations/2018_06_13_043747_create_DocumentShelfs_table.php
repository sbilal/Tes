<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentShelfsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DocumentShelfs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document_id')->nullable();
            $table->string('rack_number')->nullable();
            $table->string('shelf_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('DocumentShelfs');
    }
}
