<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeySubDivision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sub_division', function (Blueprint $table) {
            $table->integer('DistrictId')->unsigned()->change();
            
            $table->foreign('DistrictId')->references('id')->on('district');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sub_division', function (Blueprint $table) {
            $table->dropColumn('DistrictId');
            
            $table->dropForeign('sub_division_DistrictId_foreign');
            
        });
    }
}
