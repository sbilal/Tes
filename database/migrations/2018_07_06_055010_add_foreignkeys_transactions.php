<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('service_id')->unsigned()->change();
            $table->integer('buyer_id')->unsigned()->change();
            $table->integer('seller_id')->unsigned()->change();
            $table->integer('property_id')->unsigned()->change();
            $table->integer('assigned_to')->unsigned()->change();
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('buyer_id')->references('id')->on('customers');
            $table->foreign('seller_id')->references('id')->on('customers');
            $table->foreign('property_id')->references('id')->on('property');
            $table->foreign('assigned_to')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->dropColumn('buyer_id');
            $table->dropColumn('seller_id');
            $table->dropColumn('property_id');
            $table->dropColumn('assigned_to');
            $table->dropForeign('transactions_service_id_foreign');
            $table->dropForeign('transactions_buyer_id_foreign');
            $table->dropForeign('transactions_seller_id_foreign');
            $table->dropForeign('transactions_property_id_foreign');
            $table->dropForeign('transactions_assigned_to_foreign');
        });
    }
}
