<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyClaimantDocumentssTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PropertyClaimantDocumentss', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document_id')->nullable();
            $table->string('property_id')->nullable();
            $table->string('customer_id')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('PropertyClaimantDocumentss');
    }
}
