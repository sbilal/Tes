<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeysServicefeeitemss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servicefeeitemss', function (Blueprint $table) {
            $table->integer('service_id')->unsigned()->change();
            $table->integer('fee_type_id')->unsigned()->change();
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('fee_type_id')->references('id')->on('fee_type');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servicefeeitemss', function (Blueprint $table) {
            $table->dropColumn('service_id');
            $table->dropColumn('fee_type_id');
            $table->dropForeign('servicefeeitemss_service_id_foreign');
            $table->dropForeign('servicefeeitemss_fee_type_id_foreign');
            
        });
    }
}
