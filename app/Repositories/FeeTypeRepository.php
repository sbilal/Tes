<?php

namespace App\Repositories;

use App\Models\FeeType;
use InfyOm\Generator\Common\BaseRepository;

class FeeTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FeeType::class;
    }
}
