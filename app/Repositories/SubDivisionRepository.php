<?php

namespace App\Repositories;

use App\Models\SubDivision;
use InfyOm\Generator\Common\BaseRepository;

class SubDivisionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubDivision::class;
    }
}
