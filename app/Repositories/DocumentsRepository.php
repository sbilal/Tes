<?php

namespace App\Repositories;

use App\Models\Documents;
use InfyOm\Generator\Common\BaseRepository;

class DocumentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Documents::class;
    }
}
