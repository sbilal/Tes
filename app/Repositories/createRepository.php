<?php

namespace App\Repositories;

use App\Models\create;
use InfyOm\Generator\Common\BaseRepository;

class createRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return create::class;
    }
}
