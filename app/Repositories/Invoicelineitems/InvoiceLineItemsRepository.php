<?php

namespace App\Repositories\Invoicelineitems;

use App\Models\Invoicelineitems\InvoiceLineItems;
use InfyOm\Generator\Common\BaseRepository;

class InvoiceLineItemsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InvoiceLineItems::class;
    }
}
