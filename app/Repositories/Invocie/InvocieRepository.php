<?php

namespace App\Repositories\Invocie;

use App\Models\Invocie\Invocie;
use InfyOm\Generator\Common\BaseRepository;

class InvocieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Invocie::class;
    }
}
