<?php

namespace App\Repositories;

use App\Models\CustomerIdentity;
use InfyOm\Generator\Common\BaseRepository;

class CustomerIdentityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomerIdentity::class;
    }
}
