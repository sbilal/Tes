<?php

namespace App\Repositories;

use App\Models\PropertyClaimantDocuments;
use InfyOm\Generator\Common\BaseRepository;

class PropertyClaimantDocumentsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyClaimantDocuments::class;
    }
}
