<?php

namespace App\Repositories;

use App\Models\Notary;
use InfyOm\Generator\Common\BaseRepository;

class NotaryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Notary::class;
    }
}
