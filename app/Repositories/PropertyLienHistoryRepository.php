<?php

namespace App\Repositories;

use App\Models\propertylienhistory;
use InfyOm\Generator\Common\BaseRepository;

class propertylienhistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return propertylienhistory::class;
    }
}
