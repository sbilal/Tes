<?php

namespace App\Repositories;

use App\Models\LedgerScanning;
use InfyOm\Generator\Common\BaseRepository;

class LedgerScanningRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LedgerScanning::class;
    }
}
