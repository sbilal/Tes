<?php

namespace App\Repositories\Property;

use App\Models\Property\Property;
use InfyOm\Generator\Common\BaseRepository;

class PropertyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Property::class;
    }
}
