<?php

namespace App\Repositories;

use App\Models\Checklist;
use InfyOm\Generator\Common\BaseRepository;

class ChecklistRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Checklist::class;
    }
}
