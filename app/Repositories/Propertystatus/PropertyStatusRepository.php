<?php

namespace App\Repositories\Propertystatus;

use App\Models\Propertystatus\PropertyStatus;
use InfyOm\Generator\Common\BaseRepository;

class PropertyStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyStatus::class;
    }
}
