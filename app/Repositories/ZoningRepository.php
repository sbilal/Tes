<?php

namespace App\Repositories;

use App\Models\Zoning;
use InfyOm\Generator\Common\BaseRepository;

class ZoningRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Zoning::class;
    }
}
