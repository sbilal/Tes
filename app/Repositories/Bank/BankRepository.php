<?php

namespace App\Repositories\Bank;

use App\Models\Bank\Bank;
use InfyOm\Generator\Common\BaseRepository;

class BankRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bank::class;
    }
}
