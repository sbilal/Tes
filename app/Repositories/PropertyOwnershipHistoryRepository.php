<?php

namespace App\Repositories;

use App\Models\PropertyOwnershipHistory;
use InfyOm\Generator\Common\BaseRepository;

class PropertyOwnershipHistoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyOwnershipHistory::class;
    }
}
