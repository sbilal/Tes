<?php

namespace App\Repositories;

use App\Models\PropertyOwnership;
use InfyOm\Generator\Common\BaseRepository;

class PropertyOwnershipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyOwnership::class;
    }
}
