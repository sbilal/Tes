<?php

namespace App\Repositories;

use App\Models\PropertyStatus;
use InfyOm\Generator\Common\BaseRepository;

class PropertyStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PropertyStatus::class;
    }
}
