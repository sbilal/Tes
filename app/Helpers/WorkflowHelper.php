<?php

namespace App\Helpers;

use App\Models\Customer;
use App\Models\Transaction;
use App\Helpers\WorkflowConnector;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Sentinel;


class WorkflowHelper {
    
    protected $workflowConnector;
    
    public function __construct() {
        $this->workflowConnector = new WorkflowConnector();         
    }

    public function claimTask($taskId){
        $user = Sentinel::getUser();              
        Log::info("User " . $user->login . " Claiming Task  " . $taskId);
        $result = $this->workflowConnector->claimTask($taskId);
        Log::info("User " . $user->login . " Task  Claim successful taskId: " . $taskId);
        return $result;
    }
    
    public function releaseTask($taskId){
        $user = Sentinel::getUser();              
        Log::info("User " . $user->login . " Releasing Task  " . $taskId);
        $result = $this->workflowConnector->releaseTask($taskId);
        Log::info("User " . $user->login . " Task  Release successful taskId: " . $taskId);
        return $result;
    }
    
    public function approveTask($transaction) {
        $user = Sentinel::getUser();              
        Log::info("User " . $user->login . " Approved Task  " . $transaction->task_id);
        $workflowData = $this->getWorkflowData($transaction->toArray(), "Yes");
        return $this->completeTask($transaction->task_id, $workflowData);
    }
    
    public function rejectTask($transaction) {
        $user = Sentinel::getUser();              
        Log::info("User " . $user->login . " Reject Task  " . $transaction->task_id);
        $workflowData = $this->getWorkflowData($transaction->toArray(), "no");
        return $this->completeTask($transaction->task_id, $workflowData);
    }    
    
    public function completeTask($taskId, $workflowData){
        $user = Sentinel::getUser();              
        Log::info("User " . $user->login . " Completing Task  " . $taskId);
        $result = $this->workflowConnector->completeTask($taskId, $workflowData);
        Log::info("User " . $user->login . " Task Completion successful taskId: " . $taskId);
        return $result;
    }
    
    public function startChangeOwnershipWorkflow($transaction){
        $workflowData = $this->getWorkflowData($transaction->toArray(), "Yes");
        Log::info("Sending Workflow data " . json_encode($workflowData));
        $result = $this->workflowConnector->startWorkflow($workflowData);
        Log::info("Workflow Result WorkflowInstance Id: " . $result . " Transaction id:" . $transaction["id"] );
        return $result;
    }
        
    public function getUnclaimedTasks($role = null) {
        if (!Sentinel::inRole("admin") && $role == null) {
            $user = Sentinel::getUser();      
            $role = $user->getRoles()->pluck('name')->first();
            $role = str_replace("-", "", $role);
        }            
        $result = $this->workflowConnector->getUnclaimedTasks($role);
        return $result;
    }
    
    public function getCurrentWorkflowInstance($processInstanceId) {
        $result = $this->workflowConnector->getCurrentProcessInstances($processInstanceId);
        return $result;
    }    
    
    public function getWorkflowHistory($processInstanceId) {
        $processInstanceDetails = $this->workflowConnector->getProcessInstances($processInstanceId);
        
        $processInstanceDetailsArray = json_decode(json_encode($processInstanceDetails->{'node-instance'}));
        
        $result = array();
        //var_dump($processInstanceDetailsArray);
        foreach ($processInstanceDetailsArray as $processInstanceDetailsItem) {
            $record = get_object_vars($processInstanceDetailsItem);

            $eventsHistory = ["node-instance-id" => $record["node-instance-id"], "node-name" => $record["node-name"], "node-completed" => $record["node-completed"]];
                         
            
            if ($record["work-item-id"] != null) {
                //echo "adding " . $record["node-type"] . "-" . $record["work-item-id"] . "\r\n";
                $eventsHistory = array_merge($eventsHistory, $this->getTaskEvents($record["work-item-id"]));
                //if (in_array($result, $record["node-instance-id"]))
                $result[$record["work-item-id"]] = $eventsHistory;
            }
            /*
            if (str_contains($record["node-type"], "StartNode")) {
                echo "adding start node" . $record["node-type"] . "-" . $record["work-item-id"] . "\r\n";
                $result = array_prepend($result, $eventsHistory);
            }*/
        }
        
        return $result;
    }
    
    public function getTaskEvents($taskId) {
        $result = $this->workflowConnector->getTaskEvents($taskId);

        $taskEventInstances = json_decode(json_encode($result->{"task-event-instance"}));
        
        $taskEvent = [];
        $taskUser = "";
        // transposing rows to columns
        foreach ($taskEventInstances as $taskEventInstance) {   
            $taskUser = $taskEventInstance->{"task-event-user"};
            if (str_contains($taskEventInstance->{"task-event-type"}, "ADDED")) { 
                $taskEvent["added_at"] = $this->getEventDate($taskEventInstance, "ADDED"); 
            }
            if (str_contains($taskEventInstance->{"task-event-type"}, "CLAIMED")) { 
                $taskEvent["claimed_at"] = $this->getEventDate($taskEventInstance, "CLAIMED");
            }
            if (str_contains($taskEventInstance->{"task-event-type"}, "STARTED")) { 
                $taskEvent["started_at"] = $this->getEventDate($taskEventInstance, "STARTED");
            }
            if (str_contains($taskEventInstance->{"task-event-type"}, "UPDATED")) { 
                $taskEvent["updated_at"] = $this->getEventDate($taskEventInstance, "UPDATED");
            }
            if (str_contains($taskEventInstance->{"task-event-type"}, "COMPLETED")) { 
                $taskEvent["completed_at"] = $this->getEventDate($taskEventInstance, "COMPLETED");
            }
        }        
        $taskEvent["task-event-user"] = $taskUser;
        return $taskEvent;
    }
    
    public function getTask($taskId) {         
        $result = $this->workflowConnector->getTask($taskId);
        return $result;
    }
    
    public function getMyTasks($userId = null) {
        if ($userId == null) {
            $userId = Sentinel::getUser()->email;      
        }            
        $result = $this->workflowConnector->getMyTasks($userId);
        return $result;
    }
    
    protected function getWorkflowData($transaction, $approved) {
        $workflowdata = [
            "com.dhh.landservices.LandServiceWorkflowModel" => [
                "transaction" => [
                    "com.dhh.landservices.Transaction" => $transaction
                ], 
                "is_approved" => $approved
            ]
        ];
        return $workflowdata; 
    }
    
    protected function getEventDate($taskEventInstance, $event) {
        if (str_contains($taskEventInstance->{"task-event-type"}, $event)) { 
            $javaDate = $taskEventInstance->{"task-event-date"}->{"java.util.Date"};
            $phpDate = intval($javaDate / 1000);
            return \Carbon\Carbon::parse(gmdate("d M Y H:i:s", $phpDate)); 
        }        
        return null;
    }
}
