<?php

namespace App\Helpers;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use Cache;
use Carbon\Carbon;
use Exception;

class ErpConnector {
    
    protected static $CSRF_TOKEN = null;
    protected static $SERVICE_FEE_CACHE_KEY = "erp.service";
    protected static $CSRF_TOKEN_CACHE_KEY = "erp.csrftoken";
    protected static $LOGIN_CACHE_KEY = "erp.login";
    protected static $CACHE_EXPIRY_TIME = 10; // to be moved to settings later 

    public function __construct() {
        Log::info('Initializing ErpConnector');
        $this->login();
    }
    
    public function login() {
        Log::info('Erp Login ');
        if (Cache::has(self::$CSRF_TOKEN_CACHE_KEY)) {
            Log::info('Erp Login Params from Cache');
            self::$CSRF_TOKEN = Cache::get(self::$CSRF_TOKEN_CACHE_KEY);
            $content = Cache::get(self::$LOGIN_CACHE_KEY);
            return $content;                    
        }

        $startTime = date_create();
        
        $response = Curl::to($this->getApiUrl("ERP_LOGIN_API", ""))
            ->withData( array('usr'=>env("ERP_LOGIN_USER"),'pwd'=>env("ERP_LOGIN_PASSWORD")) )
            ->returnResponseObject()
            ->withResponseHeaders()
            ->setCookieJar("erpcookie.txt")
            ->post();        

        Log::info('Erp Login Execution Time : ' . date_diff($startTime, date_create())->format("%I:%S"));
        $content = $response->content;
        $headers = $response->headers;
        
        self::$CSRF_TOKEN = self::initCsrfToken();
        Cache::put(self::$CSRF_TOKEN_CACHE_KEY, self::$CSRF_TOKEN, Carbon::now()->addMinutes(self::$CACHE_EXPIRY_TIME));        
        Cache::put(self::$LOGIN_CACHE_KEY, $content, Carbon::now()->addMinutes(self::$CACHE_EXPIRY_TIME));        
        //var_dump($content);
        //var_dump($headers["Set-Cookie"]); 
        return $content;
    }

    public function getDefaultTax() {  
        
        // return the default tax item        
    }
        
    public function getServiceFee($serviceId) {             
        
        if (Cache::has(self::$SERVICE_FEE_CACHE_KEY)) {
            $feeitems = Cache::get(self::$SERVICE_FEE_CACHE_KEY);
            return $feeitems;                    
        }
        
        $productBundleUrl = $this->getApiUrl("ERP_BUNDLEFEE_API", $serviceId);
        $productBundle = $this->doGet($productBundleUrl); 

        $itemcodes = [];
        foreach ($productBundle->items as $bundleitem) {
            arraY_push($itemcodes, $bundleitem->item_code);
        }

        $filters = urlencode('"' . implode ('"," ', $itemcodes) . '"');
        $params = 'fields=[%22standard_rate%22,%22name%22,%22item_code%22,%22description%22]&filters=[[%22name%22,%22in%22,[' . $filters . ']]]';
        $feeitemsurl = $this->getApiUrl("ERP_ITEMFEE_API", $params);
        $feeitems = $this->doGet($feeitemsurl); 
        Cache::put(self::$SERVICE_FEE_CACHE_KEY, $feeitems,  Carbon::now()->addMinutes(self::$CACHE_EXPIRY_TIME));            
        return $feeitems;
    }
    
    public function createPayment($model) {
        $result = self::doPost(self::getApiUrl("ERP_PAYMENT_API"), json_encode($model));
        return $result;
    }    
    
    public function createInvoice($model) {
        $result = self::doPost(self::getApiUrl("ERP_INVOICE_API"), json_encode($model));
        return $result;
    }
    
    public function createCustomer($erpcustomer) {
        $result = self::doPost(self::getApiUrl("ERP_CUSTOMER_API"), json_encode($erpcustomer));
        return $result;
    }
         
    public function updateCustomer($erpid, $erpcustomer) {
        $url = self::getApiUrl("ERP_CUSTOMER_API") . rawurlencode($erpid);
        $result = self::doPut($url, json_encode($erpcustomer));
        return $result;
    }
    
    public function doGet($url) {         
        Log::info('Invoking GET : ' . $url);        
        $startTime = date_create();
        $response = Curl::to($url)
            ->setCookieFile("erpcookie.txt")        
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->get();        
        
        Log::info('Erp Get Execution Time for : ' . date_diff($startTime, date_create())->format("%I:%S"));
        if ($response->status == 200){
            $content = json_decode($response->content);
            $headers = $response->headers;         
            return $content->data; 
        } else {
            throw new Exception("Response Code :" . $response->status . ", Message: " . json_decode($response));
        }
    }
    
    public function doPost($url, $data = null) {
        Log::info('Invoking POST : ' . $url);        
        $startTime = date_create();
        $response = Curl::to($url)
            ->setCookieFile("erpcookie.txt")   
            ->withData( array( 'data' => $data ) )
            ->withHeader('X-Frappe-CSRF-Token: ' . self::$CSRF_TOKEN)
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->post();        
        
        Log::info('Erp Post Execution Time for $url : ' . date_diff($startTime, date_create())->format("%I:%S"));
        $content = json_decode($response->content);
        $headers = $response->headers;         
        return $content->data;              
    }
    
    public function doPut($url, $data = null) {
        Log::info('Invoking PUT : ' . $url);        
        $startTime = date_create();
        $response = Curl::to($url)
            ->setCookieFile("erpcookie.txt")   
            ->withData( array( 'data' => $data ) )
            ->withHeader('X-Frappe-CSRF-Token: ' . self::$CSRF_TOKEN)
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->put();        
        
        Log::info('Erp Put Execution Time for $url : ' . date_diff($startTime, date_create())->format("%I:%S"));
        $content = json_decode($response->content);
        $headers = $response->headers;         
        return $content->data;              
    }
    
    public function getApiUrl($api, $params = null) {
        $api = env("ERP_HOST") . env($api) . $params;
        return $api;
    }
    
    protected function initCsrfToken() {       
        $response = Curl::to(self::getApiUrl("ERP_HOME_URL"))
            ->setCookieFile("erpcookie.txt")        
            ->returnResponseObject()
            ->withResponseHeaders()                
            ->get();          
        
        Log::info('Extracting CSRF Token');
        $csrfstart = strpos($response->content, "frappe.csrf_token");
        $csrfassignment = strpos($response->content, "=\"", $csrfstart);
        Log::info('CSRF Assignment ' . $csrfassignment);
        $csrfend = strpos($response->content, "\";", $csrfstart);
        $token = substr($response->content, $csrfassignment+2, $csrfend - $csrfassignment - 2);
        Log::info('CSRF Token ' . $token);
        return $token;
    }


}
