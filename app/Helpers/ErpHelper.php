<?php

namespace App\Helpers;

use App\Models\Customer;
use App\Models\Transaction;
use App\Helpers\ErpConnector;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ErpHelper {
    
    protected $erpConnector;
    
    public function __construct() {
        $this->erpConnector = new ErpConnector();         
    }
    
    public function getServiceFees($code) {
        return $this->erpConnector->getServiceFee($code);
    }
    
    public function createOrUpdateCustomerInErp($customerId) {
        $customer = Customer::find($customerId); 
        $erpcustomer = $this->getCustomerModel($customer);
        if ($customer) {
            if ($customer->erp_customer_id) {
                 $result = $this->erpConnector->updateCustomer($customer->erp_customer_id, $erpcustomer);
            } else {
                 $result = $this->erpConnector->createCustomer($erpcustomer);
                 $customer->erp_customer_id = $result->name;
                 $customer->update();
            }
        }   
        return $customer; 
    }
    
    public function createErpInvoiceAndPayment($transaction) {
        $serviceFees = $this->erpConnector->getServiceFee("CO-FEE");
        
        $customer = $this->createOrUpdateCustomerInErp($transaction->buyer_id);
        
        Log::info("Creating Invoice for transaction " .  $transaction->id);
        $invoiceModel =  $this->erpConnector->createInvoice($this->getInvoiceModel($transaction, $customer, $serviceFees));
        $transaction->erp_invoice_id = $invoiceModel->name;
        $transaction->update();
        Log::info("Update Erp Invoice Id for transaction " .  $transaction->id . " Invoice Number " . $transaction->erp_invoice_id);

         
        $paymentModel =  $this->erpConnector->createPayment($this->getPaymentModel($transaction, $customer));
        $transaction->erp_payment_id = $paymentModel->name;
        $transaction->status = "PAID";
        $transaction->update();                
        Log::info("Update Erp Invoice Id for transaction " .  $transaction->id . " Payment Receipt No " . $transaction->erp_payment_id . " Status " . $transaction->status);
        
        return $transaction;
    }    
    
    protected function getCustomerModel($customer) {
         $model = [ 
             "customer_name" => $customer->first_name,
             "second_name" => $customer->second_name,
             "third_name" => $customer->third_name,
             "fourth_name" => $customer->fourth_name
         ];       
         return $model;                    
    }   

    protected function getInvoiceModel($transaction,$customer, $serviceFees) {
         $model = [ 
             "customer" => $customer->erp_customer_id, 
             "docstatus" => 1,
             "items" => $this->getInvoiceLineItemsModel($transaction, $serviceFees)
         ];       
         return $model;                    
    }   
    
    protected function getInvoiceLineItemsModel($transaction, $serviceFees) {
        $invoiceItems = [];
        foreach ($serviceFees as $serviceFee) {
            $invoiceItem = [ 
                "item_code"  =>  $serviceFee->name 
            ];            
            if (strpos($serviceFee->item_code, 'LEVY') !== false) {
                $invoiceItem["rate"]  = $transaction->settlement_amount * 0.025;
            }            
            array_push($invoiceItems, $invoiceItem);
        }        
        return $invoiceItems;
    }

    protected function getPaymentModel($transaction, $customer) {
         $model = [ 
                "payment_type" => "Receive",
                "party_type"=> "Customer", 
                "party" => $customer->erp_customer_id,
                "paid_amount" => $transaction->payment_amount,
                "received_amount"=> $transaction->payment_amount,
                "paid_to"=> $transaction->payment_method, 
                "references" => [[
                        "reference_doctype" => "Sales Invoice",
                        "reference_name"=> $transaction->erp_invoice_id,
                        "allocated_amount"=> $transaction->payment_amount
                ]], 
                "reference_no"=> $transaction->payment_reference_number,
                "reference_date"=> Carbon::parse($transaction->payment_date)->format('Y M d'), 
                "payment_customer" => $transaction->payment_customer, 
                "payment_customer_mobile" => $transaction->payment_customer_mobile,
                "docstatus" => 1
              ];      
         return $model;                    
    }       
}
