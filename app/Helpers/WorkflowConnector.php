<?php

namespace App\Helpers;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use Cache;
use Carbon\Carbon;
use Sentinel;
use App\Models\ApiLog;


class WorkflowConnector {
    
    protected static $PASSWORD_TEMP = "abc@123";
    
    protected static $CHANGE_WORKFLOW_URL_CONSTRUCT = ["deploymentId" => "LandServices_2.2", "definitionId" => "ChangeOwnershipWorkflow"];

    public function __construct() {
        Log::info('Initializing WorkflowConnector');
    }
    
    public function startWorkflow($data) {
        $url = $this->getApiUrl("WORKFLOW_START_INSTANCE_API", null, self::$CHANGE_WORKFLOW_URL_CONSTRUCT);
        $result = $this->doPost($url, $data);
        return $result;
    }
    
    public function getUnclaimedTasks($role=null) {     
        $queryParam = "status=Ready&page=0&pageSize=10&sort=processInstanceId&sortOrder=true"; 
        if ($role != null)
            $queryParam = $queryParam . "&groups=" . strtolower($role);
        $result = $this->doGet($this->getApiUrl("WORKFLOW_TASKS_TO_CLAIM_API", $queryParam));
        return $result;
    }  
    
    public function getTask($taskId) {
       $tarray = self::$CHANGE_WORKFLOW_URL_CONSTRUCT;
       $tarray["taskId"] = $taskId;
       Log::info("Get task Info : " . $taskId);
       $url = $this->getApiUrl("WORKFLOW_GET_TASK", null, $tarray); 
       $result = $this->doGet($url, null);
       return $result;
    }    
    
    public function getMyTasks($userId) {
        $queryParam = "user=" . rawurlencode($userId);        
        $result = $this->doGet($this->getApiUrl("WORKFLOW_MY_TASKS_API", $queryParam));
        return $result;
    }
    
    public function completeTask($taskId, $data) {        
       $tarray = self::$CHANGE_WORKFLOW_URL_CONSTRUCT;
       $tarray["taskId"] = $taskId;
       Log::info("Completing task : " . $taskId . ", User : " . Sentinel::getUser()->email);
       $url = $this->getApiUrl("WORKFLOW_COMPLETE_TASK_API", null, $tarray); 
       $result = $this->doPut($url, $data);      
       return $result;
    }    
    
    public function releaseTask($taskId) {        
       $tarray = self::$CHANGE_WORKFLOW_URL_CONSTRUCT;
       $tarray["taskId"] = $taskId;
       Log::info("Releasing task : " . $taskId . ", User : " . Sentinel::getUser()->email);
       $url = $this->getApiUrl("WORKFLOW_RELEASE_TASKS_API", null, $tarray); 
       $result = $this->doPut($url, null);      
       return $result;
    }


    public function claimTask($taskId) {        
        //claim task from portal is two actions - claim and start
       $tarray = self::$CHANGE_WORKFLOW_URL_CONSTRUCT;
       $tarray["taskId"] = $taskId;
       Log::info("Reservering task : " . $taskId . ", User : " . Sentinel::getUser()->email);
       $url = $this->getApiUrl("WORKFLOW_CLAIM_TASK_API", null, $tarray); 
       $result = $this->doPut($url, null);
       
       Log::info("Starting task : " . $taskId . ", User : " . Sentinel::getUser()->email);
       $url = $this->getApiUrl("WORKFLOW_START_TASK_API", null, $tarray); 
       $result = $this->doPut($url, null); 
       
       return $result;
    }
    
    public function getProcessInstances($processInstanceId) {
       $tarray = self::$CHANGE_WORKFLOW_URL_CONSTRUCT;
       $tarray["pInstanceId"] = $processInstanceId;
       Log::info("Get Process Instance Details : " . $processInstanceId);
       $result = $this->doGet($this->getApiUrl("WORKFLOW_GET_PROCESS_INSTANCES", "page=0&pageSize=20", $tarray));
       return $result;        
    }
    
    public function getCurrentProcessInstances($processInstanceId) {
       $tarray = self::$CHANGE_WORKFLOW_URL_CONSTRUCT;
       $tarray["pInstanceId"] = $processInstanceId;
       Log::info("Get Current Process Instance Details : " . $processInstanceId);
       $result = $this->doGet($this->getApiUrl("WORKFLOW_GET_CURRENT_PROCESS_INSTANCES", "page=0&pageSize=20", $tarray));
       return $result;        
    }    
    
    public function getTaskEvents($taskId) {
        $result = $this->doGet($this->getApiUrl("WORKFLOW_GET_TASK_EVENTS", null, ["taskId" => $taskId]));
        return $result;        
    }
   
    protected function getCurl($url, $asadmin = false) {
        $user = Sentinel::getUser();          
        $auth = base64_encode($user->email . ":" . self::$PASSWORD_TEMP);
        if ($asadmin) {
            $auth = base64_encode("teller@coh.com:" . self::$PASSWORD_TEMP);
        }

        $curl = Curl::to($url)
            ->setCookieFile("jbpmcookie.txt")  
            ->withHeader('Authorization: Basic ' . $auth)
            ->withHeader('Content-Type: application/json')
            ->returnResponseObject()
            ->withResponseHeaders()
            ->withTimeout(300);      
        return $curl;
    }

    public function doGet($url) {      
        Log::info('Invoking GET : ' . $url);        
        $startTime = date_create();
        
        $apilog = ApiLog::addApiLogRequest("GET", $url);                
        $response = $this->getCurl($url)->get();
        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        
        Log::info('Workflow Get Execution Time : ' . date_diff($startTime, date_create())->format("%I:%S"));
        
        $content = json_decode($response->content);
        $headers = $response->headers;         
        return $content; 
    }
    
    public function doPost($url, $data = null) {
        $user = Sentinel::getUser();   
        Log::info('Invoking POST : ' . $url);        
        $startTime = date_create();
        
        $apilog = ApiLog::addApiLogRequest("POST", $url, json_encode([ 'data' => $data ]));                        
        $response = $this->getCurl($url)
                    ->withData( json_encode([ 'data' => $data ]) )
                    ->post();
        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        

        Log::info('Erp Post Execution Time for $url : ' . date_diff($startTime, date_create())->format("%I:%S"));
        $content = json_decode($response->content);
        $headers = $response->headers;         
        return $content;              
    }
    
    public function doPut($url, $data = null) {
        Log::info('Invoking PUT : ' . $url);        
        $startTime = date_create();
        
        $apilog = ApiLog::addApiLogRequest("PUT", $url, json_encode([ 'data' => $data ]));                                
        $response = $this->getCurl($url)
                    ->withData( json_encode([ 'data' => $data ]) )
                    ->put();
        $apilog->addApiLogResponse(json_encode(json_decode($response->content, true)));
        
        Log::info('Erp Put Execution Time for $url : ' . date_diff($startTime, date_create())->format("%I:%S"));
        $content = json_decode($response->content);
        $headers = $response->headers;         
        return $content;              
    }
    
    public function getApiUrl($api, $params = null, $urlarray = null) {
        $api = env("WORKFLOW_HOST") . env($api);
        if ($urlarray) {
            foreach ($urlarray as $key => $value) {                
                $api = str_replace('{' . $key . '}', $value, $api);
            }
        }
        $api = $api . "?" . $params;
        return $api;
    }   
}
