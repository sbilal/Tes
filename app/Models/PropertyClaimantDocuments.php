<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class PropertyClaimantDocuments extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;

    public $table = 'PropertyClaimantDocumentss';
    


    public $fillable = [
        'document_id',
        'property_id',
        'customer_id',
        'document'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'document_id' => 'string',
        'property_id' => 'string',
        'customer_id' => 'string',
        'document' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'document_id' => 'required',
        'property_id' => 'required'
    ];
}
