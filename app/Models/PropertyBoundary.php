<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class PropertyBoundary extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    public $table = 'property_boundary';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'property_id', 
        'latitide',
        'longitude'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'property_id' => 'int',
        'latitide' => 'string',
        'longitide' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'property_id' => 'required',
        'latitide' => 'required',
        'longitide' => 'required'        
    ];
}
