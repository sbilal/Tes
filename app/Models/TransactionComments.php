<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Sentinel;
use OwenIt\Auditing\Contracts\Auditable;
use App;

class TransactionComments extends Model
{

    public $table = 'transaction_comments';
    


    public $fillable = [
        'comment_date',
        'comments',
        'transaction_id', 
        'updated_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'comments' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'comment_date' => 'required',
        'comments' => 'required',
        'transaction_id' => 'required',
        'updated_by' => 'required'
    ];
    

    public function transaction() {
        return $this->belongsTo('App\Models\Transaction');
    }
    
    public function updatedBy() {
        return $this->belongsTo(App\User::class,'updated_by');
    }    
}
