<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;



class Payment extends Model
{

    public $table = 'payment';
    


    public $fillable = [
        'InvoiceId',
        'PaidAmount',
        'PaymentDate',
        'PaymentMethod',
        'ReferenceNumber'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'InvoiceId' => 'integer',
        'PaymentMethod' => 'string',
        'ReferenceNumber' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'InvoiceId' => 'required',
        'PaidAmount' => 'required',
        'PaymentDate' => 'required',
        'PaymentMethod' => 'required',
        'ReferenceNumber' => 'required'
    ];
}
