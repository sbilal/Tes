<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class Notary extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'notarys';
    


    public $fillable = [
        'first_name',
        'second_name',
        'third_name',
        'fourth_name',
        'address',
        'district',
        'sub_division',
        'email',
        'contact_number',
        'notary_number',
        'notary_expiry_date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'first_name' => 'string',
        'second_name' => 'string',
        'third_name' => 'string',
        'fourth_name' => 'string',
        'address' => 'string',
        'city' => 'string',
        'state' => 'string',
        'notary_number' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'second_name' => 'required',
        'address' => 'required'
    ];
    
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->second_name} {$this->third_name} {$this->fourth_name}";
    }    
}
