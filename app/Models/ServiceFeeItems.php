<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class ServiceFeeItems extends Model
{

    public $table = 'ServiceFeeItemss';
    


    public $fillable = [
        'service_id',
        'fee_type_id',
        'amount',
        'due_on_status',
        'due_on_duration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'service_id' => 'string',
        'fee_type_id' => 'string',
        'due_on_status' => 'string',
        'due_on_duration' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'service_id' => 'required',
        'fee_type_id' => 'required',
        'amount' => 'required'
    ];


   
}
