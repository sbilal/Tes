<?php

namespace App\Models\Documents;

use Illuminate\Database\Eloquent\Model;



class Documents extends Model
{

    public $table = 'Documentss';
    


    public $fillable = [
        'CustomerId',
        'PropertyId',
        'DocumentName',
        'DocumentTypeId',
        'DocumentLink'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'CustomerId' => 'integer',
        'PropertyId' => 'integer',
        'DocumentName' => 'string',
        'DocumentTypeId' => 'string',
        'DocumentLink' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
