<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;




class CustomerIdentity extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'CustomerIdentitys';
    


    public $fillable = [
        'customer_id',
        'id_type',
        'id_number',
        'issuing_authority',
        'place_of_issue',
        'date_of_issue',
        'expiry_date',
        'issuing_country',
        'points'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer_id' => 'string',
        'id_type' => 'string',
        'id_number' => 'string',
        'issuing_authority' => 'string',
        'place_of_issue' => 'string',
        'issuing_country' => 'string',
        'points' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

     //ASSOCIATIONS
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
}
