<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class propertylienhistory extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $table = 'propertylienhistorys';
    
    


    public $fillable = [
        'property_ownership_id',
        'bank_id',
        'loan_amount',
        'loan_date',
        'release_date',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'property_ownership_id' => 'string',
        'bank_id' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'property_ownership_id' => 'required',
        'bank_id' => 'required',
        'loan_amount' => 'required',
        'loan_date' => 'required'
    ];
}
