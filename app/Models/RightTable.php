<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class RightTable extends Model
{

    public $table = 'Righttable';
    


    public $fillable = [
        'Property_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Property_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
