<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Sentinel;
use OwenIt\Auditing\Contracts\Auditable;
use App;

class TransactionDocuments extends Model
{

    public $table = 'transaction_documents';
    


    public $fillable = [
        'transaction_id', 
        'updated_by',
        'document_type_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'transaction_id' => 'required',
        'updated_by' => 'required',
        'document_type_id' => 'required',
    ];
    

    public function documentType() {
        return $this->belongsTo('App\Models\DocumentType');
    }
    
    public function transaction() {
        return $this->belongsTo('App\Models\Transaction');
    }
    
    public function updatedBy() {
        return $this->belongsTo(App\User::class,'updated_by');
    }    
}
