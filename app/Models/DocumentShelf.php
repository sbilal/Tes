<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class DocumentShelf extends Model  implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $table = 'DocumentShelfs';
    


    public $fillable = [
        'document_id',
        'rack_number',
        'shelf_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'document_id' => 'string',
        'rack_number' => 'string',
        'shelf_number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'document_id' => 'required'
    ];
}
