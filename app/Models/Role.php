<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class Role extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'roles';    

    public $fillable = [
        'slug',
        'name',
        'permissions',       
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'slug' => 'string',
        'name' => 'string',
        'permissions' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'slug' => 'required',
        'name' => 'required'
    ];    
}
