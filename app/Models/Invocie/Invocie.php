<?php

namespace App\Models\Invocie;

use Illuminate\Database\Eloquent\Model;



class Invocie extends Model
{

    public $table = 'invoice';
    


    public $fillable = [
        'TransactionId',
        'CustomerId',
        'InvoiceDate',
        'Status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'TransactionId' => 'string',
        'CustomerId' => 'string',
        'Status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
