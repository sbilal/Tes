<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;


class Customer extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'customers';
    


    public $fillable = [
        'first_name',
        'second_name',
        'third_name',
        'fourth_name',
        'mobile_1',
        'mobile_2',
        'customer_type',
        'street_number',
        'street_name',
        'address',
        'district',
        'sub_division',
        'area',
        'zone',
        'gender',
        'date_of_birth',
        'noor_account_number',
        'mother_first_name',
        'mother_second_name',
        'mother_third_name',
        'mother_fourth_name',
        'father_first_name',
        'father_second_name',
        'father_third_name',
        'father_fourth_name',
        'source_reference_number',
        'place_of_birth',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'first_name' => 'string',
        'second_name' => 'string',
        'third_name' => 'string',
        'fourth_name' => 'string',
        'mobile_1' => 'string',
        'mobile_2' => 'string',
        'customer_type' => 'string',
        'street_number' => 'string',
        'street_name' => 'string',
        'address' => 'string',
        'district' => 'string',
        'sub_division' => 'string',
        'area' => 'string',
        'zone' => 'string',
        'gender' => 'string',
        'noor_account_number' => 'string',
        'mother_first_name' => 'string',
        'mother_second_name' => 'string',
        'mother_third_name' => 'string',
        'mother_fourth_name' => 'string',
        'father_first_name' => 'string',
        'father_second_name' => 'string',
        'father_third_name' => 'string',
        'father_fourth_name' => 'string',
        'source_reference_number' => 'string',
        'place_of_birth' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'second_name' => 'required',
        'third_name' => 'required',
        'mobile_1' => 'required',
        'gender' => 'required',
        'date_of_birth' => 'required',
        'mother_first_name' => 'required',
        'mother_second_name' => 'required',
        'father_first_name' => 'required',
        'father_second_name' => 'required'
    ];
    
    public function getShortNameAttribute()
    {
        return "{$this->first_name} {$this->second_name}";
    }    

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->second_name} {$this->third_name} {$this->fourth_name}";
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Documents', 'customer_id');
    }

    public function identities()
    {
        return $this->hasMany('App\Models\CustomerIdentity', 'customer_id');
    }
    
    public function properties() {
        return $this->hasMany('App\Models\PropertyOwnership', 'owner_id');
    }

    public function propertyOwnerships()
    {
        return $this->hasMany('App\Models\PropertyOwnership', 'owner_id');
    }

    public function buyerTransactions()
    {
        return $this->hasMany('App\Models\Transaction', 'buyer_id');
    }

    public function sellerTransactions()
    {
        return $this->hasMany('App\Models\Transaction', 'seller_id');
    }
    public function identitiesDetail()
    {
        return $this->hasMany('App\Models\CustomerIdentity','customer_id','id');
    }

    static function inserCustomertIdenty($data)
    {
        return DB::table('customeridentitys')->insert($data);
    }

   
}
