<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class PropertyOwnershipHistory extends Model
{

    public $table = 'property_ownership_history';
    


    public $fillable = [
        'property_id',
        'status',
        'purchase_date',
        'purchase_price',
        'sale_date',
        'sale_price',
        'service_id',
        'owner_id',
        'primary_owner'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'property_id' => 'integer',
        'status' => 'string',
        'service_id' => 'integer',
        'owner_id' => 'integer',
        'primary_owner' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'property_id' => 'required',
        'status' => 'required',
        'purchase_date' => 'required',
        'purchase_price' => 'required',
        'sale_date' => 'required',
        'sale_price' => 'required'
    ];
    
    public function customer() {
        return $this->belongsTo("App\Models\Customer");
    }
    
    public function property() {
        return $this->belongsTo("App\Models\Property");
    }
    
    public function service() {
        return $this->hasOne("App\Models\service", "id", "service_id");
    }
}
