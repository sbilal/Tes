<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class Zoning extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'zonings';
    


    public $fillable = [
        'zone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'zone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'zone' => 'required'
    ];
}
