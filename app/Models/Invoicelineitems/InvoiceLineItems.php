<?php

namespace App\Models\Invoicelineitems;

use Illuminate\Database\Eloquent\Model;



class InvoiceLineItems extends Model
{

    public $table = 'invoice_line_item';
    


    public $fillable = [
        'InvoiceId',
        'FeeType',
        'Amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'InvoiceId' => 'string',
        'FeeType' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Amount' => 'required'
    ];
}
