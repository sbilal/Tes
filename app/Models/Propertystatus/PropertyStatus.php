<?php

namespace App\Models\Propertystatus;

use Illuminate\Database\Eloquent\Model;



class PropertyStatus extends Model
{

    public $table = 'property_status';
    


    public $fillable = [
        'Status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
