<?php

namespace App\Models;
use Sentinel;

use Illuminate\Database\Eloquent\Model;

class ApiLog extends Model
{
    public $table = 'api_log';
      
    public $fillable = [
        'url', 
        'request', 
        'request_time', 
        'response_time',
        'response', 
        'user'
    ];      
    
    
    public static function addApiLogRequest($method, $url, $request = null)  {
        $apilog = new ApiLog(); 
        $apilog->method = $method;
        $apilog->url = $url;
        $apilog->request_time = date("Y-m-d H:i:s");
        $apilog->user = Sentinel::getUser()->email; 
        $apilog->request = $request;
        $apilog->save(); 
        return $apilog;
    }
    
    public function addApiLogResponse($response)  {
        $this->response = $response;
        $this->response_time = date("Y-m-d H:i:s");
        $this->execution_time = $this->millisecsBetween($this->request_time, $this->response_time);
        return $this->save(); 
    }        
    
    function millisecsBetween($dateOne, $dateTwo, $abs = true) {
        $func = $abs ? 'abs' : 'intval';
        return $func(strtotime($dateOne) - strtotime($dateTwo)) * 1000;
    }    
}
