<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use OwenIt\Auditing\Contracts\Auditable;

class Documents extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'Documentss';
    


    public $fillable = [
        'customer_id',
        'property_id',
        'document_name',
        'document_type_id',
        'document_link'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'customer_id' => 'string',
        'property_id' => 'string',
        'document_name' => 'string',
        'document_type_id' => 'string',
        'document_link' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required',
        'property_id' => 'required',
        'document_name' => 'required'
    ];
}
