<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class Property extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'property';

    protected $guarded = array();

    public $fillable = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'parcel_id' => 'required',
        'area' => 'required',
        'north' => 'required',
        'south' => 'required',
        'east' => 'required',
        'west' => 'required',
        'barcode' => 'required',
        'deed_number' => 'required',     
        'district' => 'required',
        'sub_division' => 'required'
    ];
    
    public function applications() {
        $result = $this->hasMany("App\Models\PropertyOwnershipApplication");
        return $result;
    }    

    public function primaryowner() {
        return $this->hasMany("App\Models\PropertyOwnership")->where("primary_owner", "=", "YES");
    }
    
    public function currentowners() {
        return $this->hasMany("App\Models\PropertyOwnership");
    }
    
    public function ownerhistory() {
        return $this->hasMany("App\Models\PropertyOwnershipHistory");
    }  
}
