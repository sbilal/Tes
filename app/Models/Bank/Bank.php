<?php

namespace App\Models\Bank;

use Illuminate\Database\Eloquent\Model;



class Bank extends Model
{

    public $table = 'bank';
    


    public $fillable = [
        'Name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'Name' => 'required'
    ];
}
