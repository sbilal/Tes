<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;



class LedgerScanning extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    public $table = 'LedgerScannings';
    


    public $fillable = [
        'date',
        'registration_no',
        'magaca',
        'degree',
        'waxx',
        'xaag',
        'quab',
        'xof',
        'cabbie',
        'gr_date',
        'gr_number',
        'amount',
        'orign_no',
        'file_no',
        'teysaro'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'magaca' => 'string',
        'degree' => 'string',
        'waxx' => 'string',
        'xaag' => 'string',
        'quab' => 'string',
        'xof' => 'string',
        'cabbie' => 'string',
        'teysaro' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'registration_no' => 'required',
        'magaca' => 'required',
        'degree' => 'required'
    ];
}
