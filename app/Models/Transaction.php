<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sentinel;
use OwenIt\Auditing\Contracts\Auditable;


 
class Transaction extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    public $table = 'Transactions';
    
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public $fillable = [
        'service_id',
        'application_date',
        'buyer_id',
        'seller_id',
        'property_id',
        'settlement_date',
        'settlement_amount',
        'status',
        'assigned_to',
        'workflow_status', 
        'notary_id',
        'payment_status', 'payment_date', 'payment_amount', 'payment_method', 'payment_customer', 'payment_reference_number', 'transaction_reference_number', 'payment_customer_mobile', 'erp_invoice_id', 'erp_payment_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'service_id' => 'string',
        'buyer_id' => 'string',
        'seller_id' => 'string',
        'property_id' => 'string',
        'status' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'buyer_id' => 'required',
        'seller_id' => 'required',
        'property_id' => 'required'
    ];

     //ASSOCIATIONS
    public function property() {
        return $this->belongsTo('App\Models\Property','property_id');
    }

    public function buyer() {
        return $this->belongsTo('App\Models\Customer','buyer_id');
    }

    public function notary() {
        return $this->belongsTo('App\Models\Notary','notary_id');
    }

    public function seller() {
        return $this->belongsTo('App\Models\Customer','seller_id');
    }

    public function service(){
        return $this->belongsTo('App\Models\service','service_id');
    }

    public function assignedTo(){
        return $this->belongsTo(User::class,'assigned_to');
    }
    
    public function comments(){
        return $this->hasMany(TransactionComments::class);
    }
    
}
