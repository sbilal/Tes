<?php

namespace App\Models\Servicefeeitems;

use Illuminate\Database\Eloquent\Model;



class ServiceFeeItems extends Model
{

    public $table = 'service_fee_item';
    


    public $fillable = [
        'ServiceId',
        'FeeTypeId',
        'Amount',
        'DueOnStatus',
        'DueOnDuration'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ServiceId' => 'string',
        'FeeTypeId' => 'string',
        'DueOnStatus' => 'string',
        'DueOnDuration' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ServiceId' => 'required',
        'FeeTypeId' => 'required',
        'Amount' => 'required',
        'DueOnStatus' => 'required',
        'DueOnDuration' => 'required'
    ];
}
