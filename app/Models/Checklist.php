<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sentinel;
use OwenIt\Auditing\Contracts\Auditable;

class Checklist extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;

    public $table = 'Checklists';
    


    public $fillable = [
        'name',
        'description',
        'role_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'role_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
    
    public function role() {
        return $this->belongsTo('App\Models\Role','role_id');
    }
}
