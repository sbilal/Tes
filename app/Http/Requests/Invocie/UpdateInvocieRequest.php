<?php

namespace App\Http\Requests\Invocie;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Invocie\Invocie;

class UpdateInvocieRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Invocie::$rules;
    }
}
