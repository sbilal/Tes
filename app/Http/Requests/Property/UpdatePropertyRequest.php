<?php

namespace App\Http\Requests\Property;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Property\Property;

class UpdatePropertyRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Property::$rules;
    }
}
