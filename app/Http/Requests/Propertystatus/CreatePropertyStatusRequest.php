<?php

namespace App\Http\Requests\Propertystatus;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Propertystatus\PropertyStatus;

class CreatePropertyStatusRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PropertyStatus::$rules;
    }
}
