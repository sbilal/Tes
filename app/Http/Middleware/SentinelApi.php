<?php

namespace App\Http\Middleware;

use App\Task;
use Closure;
use Sentinel;
use Illuminate\Support\Facades\Log;

class SentinelApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Sentinel::check())
            return response()->json(['error' => 'invalid_credentials'], 401);
        
        $params = $request->only('email', 'password');
        var_dump($request->header('Authorization'));

        if(Sentinel::authenticate($params)){
            return Sentinel::user()->createToken('my_user', []);
        }

        //return response()->json(['error' => 'Invalid username or Password']);           

        //return $next($request);
    }
}
