<?php

namespace App\Http\Middleware;

use App\Task;
use Closure;
use Sentinel;
use Illuminate\Support\Facades\Log;

class SentinelAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Sentinel::check())
            return redirect('admin/signin')->with('info', 'You must be logged in!');
        
        if(Sentinel::inRole('admin')) {
            return $next($request);
        }
        
        $user = Sentinel::getUser();      
        $role = strtolower($user->getRoles()->pluck('name')->first());
        $logmessage = "User [" . $user->email . "] with role " . $role . " accessing [" . \Request::route()->getName() . "]  Access Granted [" . (Sentinel::hasAccess(\Request::route()->getName())?"Yes" : "No") . "]";
        Log::info($logmessage);
        Log::info($user->getRoles()->pluck('name')->first());

        /*elseif(!Sentinel::inRole('admin')) {
            Log::info(" admin condtion true ");
            $tasks_count = Task::where('user_id', Sentinel::getUser()->id)->count();
            $request->attributes->add(['tasks_count' => $tasks_count]);
            return $next($request);
        }*/
                       
        if(Sentinel::hasAccess(\Request::route()->getName())){     
            return $next($request);
        } else {
            return redirect('unauthorized');
        }

        /*elseif(Sentinel::inRole('user'))
            return redirect('my-account');
        elseif(Sentinel::inRole('teller'))
            return redirect('teller/dashboard');
        elseif(Sentinel::inRole('district-officer'))
            return redirect('do-dashboard');
        elseif(Sentinel::inRole('district-geometer'))
            return redirect('dg-dashboard');        
        elseif(Sentinel::inRole('headoffice-officer'))
            return redirect('portal/headoffice/office/dashboard');
        elseif(Sentinel::inRole('headoffice-officer'))
            return redirect('portal/headoffice/office/dashboard');
        elseif(Sentinel::inRole('court'))
            return redirect('portal/court/dashboard');
        elseif(Sentinel::inRole('notary'))
            return redirect('portal/notary/dashboard');
        elseif(Sentinel::inRole('executive'))
            return redirect('portal/executive/dashboard');
        */
        
        $tasks_count = Task::where('user_id', Sentinel::getUser()->id)->count();
        $request->attributes->add(['tasks_count' => $tasks_count]);

        return $next($request);
    }
}
