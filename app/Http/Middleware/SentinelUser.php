<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class SentinelUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {               
        $headers = $request->header();
        if (isset($headers["php-auth-user"])) {
            $credentials['email'] = $headers["php-auth-user"][0];
        }

        if (isset($headers["php-auth-pw"])) {
            $credentials['password'] = $headers["php-auth-pw"][0];
        } 

        if(isset($credentials) && Sentinel::authenticate($credentials)){
            return $next($request);
        }
        return response()->json(['error' => 'unauthorized'], 401);
    }
}
