<?php

namespace App\Http\Controllers\Auth;

use \App\Models\Transaction;
use Sentinel;
use Illuminate\Support\Facades\Log;
use App\Helpers\WorkflowHelper;
use Yajra\DataTables\Facades\DataTables;

class RoleBaseController extends \App\Http\Controllers\JoshController
{
    protected $workflowHelper;
    
    public function __construct() {
        $this->workflowHelper = new WorkflowHelper();
    }    
    
    
    public function dashboard() {
        $user = Sentinel::getUser();   
        $role = strtolower($user->getRoles()->pluck('name')->first());
        $view = 'portal.' . $role . '.dashboard';
        
        
        $tasksAssignedToMe = "";
        $taskId = "";
        $unclaimedTasks = ""; 
        
        
        Log::info("Redirecting user to " . $view);
        return view("portal.partials.dashboard-common", compact('user', 'transactions', "unclaimedTasks", "taskId"));
    }
    
    
    public function getRecentTransactions() {
        $transactions = Transaction::with(["buyer", "seller", "property", "service"])->whereNotNull("buyer_id")->take(20)->orderBy('id', 'desc')->get(); 
        
    }

    
}