<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateFeeTypeRequest;
use App\Http\Requests\UpdateFeeTypeRequest;
use App\Repositories\FeeTypeRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\FeeType;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FeeTypeController extends InfyOmBaseController
{
    /** @var  FeeTypeRepository */
    private $feeTypeRepository;

    public function __construct(FeeTypeRepository $feeTypeRepo)
    {
        $this->feeTypeRepository = $feeTypeRepo;
    }

    /**
     * Display a listing of the FeeType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->feeTypeRepository->pushCriteria(new RequestCriteria($request));
        $feeTypes = $this->feeTypeRepository->all();
        return view('admin.feeTypes.index')
            ->with('feeTypes', $feeTypes);
    }

    /**
     * Show the form for creating a new FeeType.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.feeTypes.create');
    }

    /**
     * Store a newly created FeeType in storage.
     *
     * @param CreateFeeTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateFeeTypeRequest $request)
    {
        $input = $request->all();

        $feeType = $this->feeTypeRepository->create($input);

        Flash::success('FeeType saved successfully.');

        return redirect(route('admin.feeTypes.index'));
    }

    /**
     * Display the specified FeeType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $feeType = $this->feeTypeRepository->findWithoutFail($id);

        if (empty($feeType)) {
            Flash::error('FeeType not found');

            return redirect(route('feeTypes.index'));
        }

        return view('admin.feeTypes.show')->with('feeType', $feeType);
    }

    /**
     * Show the form for editing the specified FeeType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $feeType = $this->feeTypeRepository->findWithoutFail($id);

        if (empty($feeType)) {
            Flash::error('FeeType not found');

            return redirect(route('feeTypes.index'));
        }

        return view('admin.feeTypes.edit')->with('feeType', $feeType);
    }

    /**
     * Update the specified FeeType in storage.
     *
     * @param  int              $id
     * @param UpdateFeeTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeeTypeRequest $request)
    {
        $feeType = $this->feeTypeRepository->findWithoutFail($id);

        

        if (empty($feeType)) {
            Flash::error('FeeType not found');

            return redirect(route('feeTypes.index'));
        }

        $feeType = $this->feeTypeRepository->update($request->all(), $id);

        Flash::success('FeeType updated successfully.');

        return redirect(route('admin.feeTypes.index'));
    }

    /**
     * Remove the specified FeeType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.feeTypes.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = FeeType::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.feeTypes.index'))->with('success', Lang::get('message.success.delete'));

       }

}
