<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePropertyOwnershipRequest;
use App\Http\Requests\UpdatePropertyOwnershipRequest;
use App\Repositories\PropertyOwnershipRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\PropertyOwnership;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PropertyOwnershipController extends InfyOmBaseController
{
    /** @var  PropertyOwnershipRepository */
    private $propertyOwnershipRepository;

    public function __construct(PropertyOwnershipRepository $propertyOwnershipRepo)
    {
        $this->propertyOwnershipRepository = $propertyOwnershipRepo;
    }

    /**
     * Display a listing of the PropertyOwnership.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->propertyOwnershipRepository->pushCriteria(new RequestCriteria($request));
        $propertyOwnerships = $this->propertyOwnershipRepository->all();
        return view('admin.propertyOwnerships.index')
            ->with('propertyOwnerships', $propertyOwnerships);
    }

    /**
     * Show the form for creating a new PropertyOwnership.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.propertyOwnerships.create');
    }

    /**
     * Store a newly created PropertyOwnership in storage.
     *
     * @param CreatePropertyOwnershipRequest $request
     *
     * @return Response
     */
    public function store(CreatePropertyOwnershipRequest $request)
    {
        $input = $request->all();

        $propertyOwnership = $this->propertyOwnershipRepository->create($input);

        Flash::success('PropertyOwnership saved successfully.');

        return redirect(route('admin.propertyOwnerships.index'));
    }

    /**
     * Display the specified PropertyOwnership.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propertyOwnership = $this->propertyOwnershipRepository->findWithoutFail($id);

        if (empty($propertyOwnership)) {
            Flash::error('PropertyOwnership not found');

            return redirect(route('propertyOwnerships.index'));
        }

        return view('admin.propertyOwnerships.show')->with('propertyOwnership', $propertyOwnership);
    }

    /**
     * Show the form for editing the specified PropertyOwnership.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propertyOwnership = $this->propertyOwnershipRepository->findWithoutFail($id);

        if (empty($propertyOwnership)) {
            Flash::error('PropertyOwnership not found');

            return redirect(route('propertyOwnerships.index'));
        }

        return view('admin.propertyOwnerships.edit')->with('propertyOwnership', $propertyOwnership);
    }

    /**
     * Update the specified PropertyOwnership in storage.
     *
     * @param  int              $id
     * @param UpdatePropertyOwnershipRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropertyOwnershipRequest $request)
    {
        $propertyOwnership = $this->propertyOwnershipRepository->findWithoutFail($id);

        

        if (empty($propertyOwnership)) {
            Flash::error('PropertyOwnership not found');

            return redirect(route('propertyOwnerships.index'));
        }

        $propertyOwnership = $this->propertyOwnershipRepository->update($request->all(), $id);

        Flash::success('PropertyOwnership updated successfully.');

        return redirect(route('admin.propertyOwnerships.index'));
    }

    /**
     * Remove the specified PropertyOwnership from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.propertyOwnerships.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = PropertyOwnership::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.propertyOwnerships.index'))->with('success', Lang::get('message.success.delete'));

       }

}
