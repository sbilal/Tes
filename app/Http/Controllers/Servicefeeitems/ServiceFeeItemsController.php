<?php

namespace App\Http\Controllers\Servicefeeitems;

use App\Http\Requests;
use App\Http\Requests\Servicefeeitems\CreateServiceFeeItemsRequest;
use App\Http\Requests\Servicefeeitems\UpdateServiceFeeItemsRequest;
use App\Repositories\Servicefeeitems\ServiceFeeItemsRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Servicefeeitems\ServiceFeeItems;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ServiceFeeItemsController extends InfyOmBaseController
{
    /** @var  ServiceFeeItemsRepository */
    private $serviceFeeItemsRepository;

    public function __construct(ServiceFeeItemsRepository $serviceFeeItemsRepo)
    {
        $this->serviceFeeItemsRepository = $serviceFeeItemsRepo;
    }

    /**
     * Display a listing of the ServiceFeeItems.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->serviceFeeItemsRepository->pushCriteria(new RequestCriteria($request));
        $serviceFeeItems = $this->serviceFeeItemsRepository->all();
        return view('admin.serviceFeeItems.serviceFeeItems.index')
            ->with('serviceFeeItems', $serviceFeeItems);
    }

    /**
     * Show the form for creating a new ServiceFeeItems.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.serviceFeeItems.serviceFeeItems.create');
    }

    /**
     * Store a newly created ServiceFeeItems in storage.
     *
     * @param CreateServiceFeeItemsRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceFeeItemsRequest $request)
    {
        $input = $request->all();

        $serviceFeeItems = $this->serviceFeeItemsRepository->create($input);

        Flash::success('ServiceFeeItems saved successfully.');

        return redirect(route('admin.serviceFeeItems.serviceFeeItems.index'));
    }

    /**
     * Display the specified ServiceFeeItems.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serviceFeeItems = $this->serviceFeeItemsRepository->findWithoutFail($id);

        if (empty($serviceFeeItems)) {
            Flash::error('ServiceFeeItems not found');

            return redirect(route('serviceFeeItems.index'));
        }

        return view('admin.serviceFeeItems.serviceFeeItems.show')->with('serviceFeeItems', $serviceFeeItems);
    }

    /**
     * Show the form for editing the specified ServiceFeeItems.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serviceFeeItems = $this->serviceFeeItemsRepository->findWithoutFail($id);

        if (empty($serviceFeeItems)) {
            Flash::error('ServiceFeeItems not found');

            return redirect(route('serviceFeeItems.index'));
        }

        return view('admin.serviceFeeItems.serviceFeeItems.edit')->with('serviceFeeItems', $serviceFeeItems);
    }

    /**
     * Update the specified ServiceFeeItems in storage.
     *
     * @param  int              $id
     * @param UpdateServiceFeeItemsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceFeeItemsRequest $request)
    {
        $serviceFeeItems = $this->serviceFeeItemsRepository->findWithoutFail($id);

        

        if (empty($serviceFeeItems)) {
            Flash::error('ServiceFeeItems not found');

            return redirect(route('serviceFeeItems.index'));
        }

        $serviceFeeItems = $this->serviceFeeItemsRepository->update($request->all(), $id);

        Flash::success('ServiceFeeItems updated successfully.');

        return redirect(route('admin.serviceFeeItems.serviceFeeItems.index'));
    }

    /**
     * Remove the specified ServiceFeeItems from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.serviceFeeItems.serviceFeeItems.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = ServiceFeeItems::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.serviceFeeItems.serviceFeeItems.index'))->with('success', Lang::get('message.success.delete'));

       }

}
