<?php

namespace App\Http\Controllers\Invocie;

use App\Http\Requests;
use App\Http\Requests\Invocie\CreateInvocieRequest;
use App\Http\Requests\Invocie\UpdateInvocieRequest;
use App\Repositories\Invocie\InvocieRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Invocie\Invocie;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InvocieController extends InfyOmBaseController
{
    /** @var  InvocieRepository */
    private $invocieRepository;

    public function __construct(InvocieRepository $invocieRepo)
    {
        $this->invocieRepository = $invocieRepo;
    }

    /**
     * Display a listing of the Invocie.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->invocieRepository->pushCriteria(new RequestCriteria($request));
        $invocies = $this->invocieRepository->all();
        return view('admin.invocie.invocies.index')
            ->with('invocies', $invocies);
    }

    /**
     * Show the form for creating a new Invocie.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.invocie.invocies.create');
    }

    /**
     * Store a newly created Invocie in storage.
     *
     * @param CreateInvocieRequest $request
     *
     * @return Response
     */
    public function store(CreateInvocieRequest $request)
    {
        $input = $request->all();

        $invocie = $this->invocieRepository->create($input);

        Flash::success('Invocie saved successfully.');

        return redirect(route('admin.invocie.invocies.index'));
    }

    /**
     * Display the specified Invocie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $invocie = $this->invocieRepository->findWithoutFail($id);

        if (empty($invocie)) {
            Flash::error('Invocie not found');

            return redirect(route('invocies.index'));
        }

        return view('admin.invocie.invocies.show')->with('invocie', $invocie);
    }

    /**
     * Show the form for editing the specified Invocie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $invocie = $this->invocieRepository->findWithoutFail($id);

        if (empty($invocie)) {
            Flash::error('Invocie not found');

            return redirect(route('invocies.index'));
        }

        return view('admin.invocie.invocies.edit')->with('invocie', $invocie);
    }

    /**
     * Update the specified Invocie in storage.
     *
     * @param  int              $id
     * @param UpdateInvocieRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvocieRequest $request)
    {
        $invocie = $this->invocieRepository->findWithoutFail($id);

        

        if (empty($invocie)) {
            Flash::error('Invocie not found');

            return redirect(route('invocies.index'));
        }

        $invocie = $this->invocieRepository->update($request->all(), $id);

        Flash::success('Invocie updated successfully.');

        return redirect(route('admin.invocie.invocies.index'));
    }

    /**
     * Remove the specified Invocie from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.invocie.invocies.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Invocie::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.invocie.invocies.index'))->with('success', Lang::get('message.success.delete'));

       }

}
