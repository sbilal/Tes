<?php

namespace App\Http\Controllers\Notary;

use Activation;
use App\Http\Requests\FrontendRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\UserRequest;
use App\Mail\Contact;
use App\Mail\ContactUser;
use App\Mail\ForgotPassword;
use App\Mail\Register;
use App\User;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use File;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Reminder;
use Sentinel;
use URL;
use Validator;
use View;
use \App\Models\Transaction;


class NotaryRoleController extends \App\Http\Controllers\JoshController
{

    public function dashboard() {
        $user = Sentinel::getUser();       
        $transactions = Transaction::where('assigned_to', $user->id)->get();        
        return view('portal.notary.dashboard', compact('user', 'transactions'));
    }

    
}