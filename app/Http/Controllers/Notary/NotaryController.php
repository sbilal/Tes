<?php

namespace App\Http\Controllers\Notary;

use App\Http\Requests;
use App\Http\Requests\CreateNotaryRequest;
use App\Http\Requests\UpdateNotaryRequest;
use App\Repositories\NotaryRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Notary;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Yajra\DataTables\Facades\DataTables;


class NotaryController extends InfyOmBaseController
{
    /** @var  NotaryRepository */
    private $notaryRepository;

    public function __construct(NotaryRepository $notaryRepo)
    {
        $this->notaryRepository = $notaryRepo;
    }

    /**
     * Display a listing of the Notary.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->notaryRepository->pushCriteria(new RequestCriteria($request));
        $notaries = $this->notaryRepository->all();
        return view('admin.notaries.index')
            ->with('notaries', $notaries);
    }

    /**
     * Show the form for creating a new Notary.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.notaries.create');
    }

    /**
     * Store a newly created Notary in storage.
     *
     * @param CreateNotaryRequest $request
     *
     * @return Response
     */
    public function store(CreateNotaryRequest $request)
    {
        $input = $request->all();

        $notary = $this->notaryRepository->create($input);

        Flash::success('Notary saved successfully.');

        return redirect(route('admin.notaries.index'));
    }

    /**
     * Display the specified Notary.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notary = $this->notaryRepository->findWithoutFail($id);

        if (empty($notary)) {
            Flash::error('Notary not found');

            return redirect(route('notaries.index'));
        }

        return view('admin.notaries.show')->with('notary', $notary);
    }

    /**
     * Show the form for editing the specified Notary.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notary = $this->notaryRepository->findWithoutFail($id);

        if (empty($notary)) {
            Flash::error('Notary not found');

            return redirect(route('notaries.index'));
        }

        return view('admin.notaries.edit')->with('notary', $notary);
    }

    /**
     * Update the specified Notary in storage.
     *
     * @param  int              $id
     * @param UpdateNotaryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotaryRequest $request)
    {
        $notary = $this->notaryRepository->findWithoutFail($id);

        

        if (empty($notary)) {
            Flash::error('Notary not found');

            return redirect(route('notaries.index'));
        }

        $notary = $this->notaryRepository->update($request->all(), $id);

        Flash::success('Notary updated successfully.');

        return redirect(route('admin.notaries.index'));
    }

    /**
     * Remove the specified Notary from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.notaries.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Notary::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.notaries.index'))->with('success', Lang::get('message.success.delete'));

       }
       
       public function getNotariesApi() {
           $modal = Notary::query();
           return Datatables::eloquent($modal)->toJson();
       }
       
       public function getNotaryApi($id) {
           $notary = Notary::find($id);
           return $notary;;
       }       

       public function getMainPartial($id) {
            $notary = Notary::find($id);
            return View('admin.notaries.show_fields', compact('notary'));
       }            

}
