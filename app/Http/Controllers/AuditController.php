<?php


namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Response;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Models\Audit;

class AuditController extends Controller
{
    public function userAuditDetails(Request $request)
    {
        $audit_id = $request->input('audit_id');
        $model = $request->input('model');

        $audit_results =    DB::select("
                             SELECT event,auditable_id,auditable_type, old_values, new_values,a.created_at,a.updated_at, first_name, last_name FROM audits a left join users u on a.user_id = u.id
                            where auditable_id = ".$audit_id."  and auditable_type = '".$model."'  ORDER BY a.id DESC");


        

        return view('admin.common-audit-partial',compact('audit_results'));
        
    }


    public function customerAuditDetails(Request $request)
    {
        $audit_ids = $request->input('audit_ids');
        $model = $request->input('model');

        if($audit_ids){

            $audit_results = Audit::join('users', 'users.id', '=', 'audits.user_id')
                                -> whereIn('auditable_id', $audit_ids)
                                -> where('auditable_type', $model)
                                ->orderBy('audits.id', 'desc')
                                ->take(20)
                                ->get();

            return view('admin.common-audit-partial',compact('audit_results'));
        }
        
    }

}
