<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatepropertyRequest;
use App\Http\Requests\UpdatepropertyRequest;
use App\Repositories\propertyRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Property;
use App\Models\PropertyBoundary;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Yajra\DataTables\Facades\DataTables;
use PDF;


class propertyController extends InfyOmBaseController
{
    /** @var  propertyRepository */
    private $propertyRepository;

    public function __construct(propertyRepository $propertyRepo)
    {
        $this->propertyRepository = $propertyRepo;
    }

    /**
     * Display a listing of the property.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->propertyRepository->pushCriteria(new RequestCriteria($request));
        $properties = $this->propertyRepository->all();
        return view('admin.properties.index')
            ->with('properties', $properties);
    }

    /**
     * Show the form for creating a new property.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.properties.create');
    }

    /**
     * Store a newly created property in storage.
     *
     * @param CreatepropertyRequest $request
     *
     * @return Response
     */
    public function store(CreatepropertyRequest $request)
    {
        $input = $request->all();

        $property = $this->propertyRepository->create($input);

        Flash::success('Property saved successfully.');

        return redirect(route('admin.properties.index'));
    }

    /**
     * Display the specified property.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $property = Property::find($id);
        $owner_name_detail = "";
        $ownership_detail = "";
        $property_lien_histories = array();
       if (empty($property)) {
            Flash::error('Property not found');
            return redirect(route('properties.index'));
        }
        return view('admin.properties.show',compact('ownership_detail','owner_name_detail','property_lien_histories'))->with('property', $property);
    }

    /**
     * Show the form for editing the specified property.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $property = $this->propertyRepository->findWithoutFail($id);
        if (empty($property)) {
            Flash::error('Property not found');
            return redirect(route('properties.index'));
        }
        return view('admin.properties.edit')->with('property', $property);
    }

    /**
     * Update the specified property in storage.
     *
     * @param  int              $id
     * @param UpdatepropertyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepropertyRequest $request)
    {
        $property = $this->propertyRepository->findWithoutFail($id);
        if (empty($property)) {
            Flash::error('Property not found');
            return redirect(route('properties.index'));
        }
        $property = $this->propertyRepository->update($request->all(), $id);
        Flash::success('property updated successfully.');
        return redirect(route('admin.properties.index'));
    }

    /**
     * Remove the specified property from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.properties.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = property::destroy($id);
           // Redirect to the group management page
           return redirect(route('admin.properties.index'))->with('success', Lang::get('message.success.delete'));
       }
       
       public function modal() {
            return View('admin.properties.modal-test');
       }
       
       public function getPropertyJson() {
           $properties = property::with( ['currentowners', 'currentowners.customer']);
           return Datatables::eloquent($properties)->toJson();
       }

       public function getMainPartial($id) {
            $property = Property::find($id);
            return View('admin.properties.main-partial', compact('property'));
       } 

    public function generatePropertyCetificatePdf($id) {

        $property = Property::find($id);
        //return View('admin.properties.certificate', compact('property'));
        $pdf = PDF::loadView('admin.properties.certificate', compact('property'));
        $file_name = $property->parcel_id.'.'.$property->id.'.pdf';
        return $pdf->download($file_name);
    }


       
       public function addPropertyBoundary($propertyId, Request $request) {
           $latitude = $request->input("latitude");
           $longitude = $request->input("longitude");
           
           if ($latitude == null || $longitude == null)
               return ["error" => "Latitide or Longitude is missing"];
           
           $propertyBoundary = new PropertyBoundary();
           $propertyBoundary->property_id = $propertyId;
           $propertyBoundary->latitude = $latitude;
           $propertyBoundary->longitude = $longitude;
           $propertyBoundary->save();
           
           return json_encode($propertyBoundary);           
       }
}


