<?php

namespace App\Http\Controllers;

use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;
use App\Helpers\WorkflowHelper;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Transaction;
use App\Models\TransactionComments;
use App\Models\TransactionDocuments;
use App\Models\PropertyOwnershipApplication;
use App\Models\service;
use App\Models\Checklist;
use App\Models\DocumentType;
use Session;
use Sentinel;
use Flash;
use App\Helpers\ErpHelper;


class WorkflowController extends JoshController
{
    protected $workflowHelper;
    protected $erpHelper;
    
    
    public function __construct() {
        $this->workflowHelper = new WorkflowHelper();
        $this->erpHelper = new ErpHelper();        
    }    
    
    public function getApplication ($transactionId, Request $request) {
        $user = Sentinel::getUser();
        $documentTypes = DocumentType::all();

        $transaction = Transaction::with(["buyer", "seller", "property"])->where("id","=",$transactionId)->get()->first();
        $transactionComments = TransactionComments::with("updatedBy")->orderBy('created_at', 'desc')->get();
        $transactionDocuments = TransactionDocuments::with(["documentType", "updatedBy"])->orderBy('created_at', 'desc')->get();
        $propertyOwnershipApplication = PropertyOwnershipApplication::with("customer")->where("transaction_id", "=", $transaction->id)->get();
        $service  = service::find($transaction->service_id);                           ;
        $serviceFees = $this->erpHelper->getServiceFees($service->code);       
  
        $rolechecklist = $this->getRoleChecklist($user);
        $assignedToCurrentUser = $this->isWorkflowInstanceAssignedToCurrentUser($user, $transaction->workflow_instance_id);
    
        return view('admin.workflow.process', ['transaction' => $transaction, "property" => $transaction->property, "propertyOwnershipApplication" => $propertyOwnershipApplication, "serviceFees" => $serviceFees, "documentTypes" => $documentTypes, "transactionComments" => $transactionComments, "transactionDocuments" => $transactionDocuments, "rolechecklist" => $rolechecklist, "assignedToCurrentUser" => $assignedToCurrentUser]);
    }
       

    public function approveApplication($transactionId, Request $request) {
        $transaction = Transaction::find($transactionId); 
        $result = $this->workflowHelper->approveTask($transaction);
        $transaction->task_id = null;
        $transaction->save(); 
        Flash::success("Application Submitted Successfully");
        return redirect(route("workflow.view-application", $transactionId));                
    }
    
    public function rejectApplication($transactionId, Request $request) {    
        $transaction = Transaction::find($transactionId); 
        $result = $this->workflowHelper->rejectTask($transaction);
        $transaction->task_id = null;
        $transaction->save(); 
        Flash::success("Application Submitted Successfully");
        return redirect(route("workflow.view-application", $transactionId));                          
    }
    
    public function processApplication($transactionId, $taskId, Request $request) {
        $rejected = $request->input("reject");
        $approved = $request->input("approve");
        
        if ($rejected) {
            echo "Rejected ";
        }
        if ($approved) {
            echo "Approved ";
        }
        //$workflowTask = $this->workflowHelper->getTask($taskId);
        //var_dump($workflowTask);
        //return redirect(route("view-application", $transacitonId));
    }

    public function getApplicationHistory($transactionId) {
        $transaction = Transaction::find($transactionId);
        $result = $this->workflowHelper->getWorkflowHistory($transaction->workflow_instance_id);
        return $result;
    }
    
    public function getApplicationHistoryDatatable($transactionId) {
        $transaction = Transaction::find($transactionId);
        $result = $this->workflowHelper->getWorkflowHistory($transaction->workflow_instance_id);
        return Datatables::collection($result)->make(true);  
    }
    
    public function getMyTasksApi(Request $request) {
        $tasks = $this->workflowHelper->getMyTasks();
        return $this->mapWorkflowTransaction($tasks);
    }    

    public function getMyTasksDatatable(Request $request) {
        $tasks = $this->workflowHelper->getMyTasks(Sentinel::getUser()->email);
        return $this->getDataTable($tasks);
    }
    
    public function getUnclaimedTasksDatatable(Request $request) { 
        $workflowUnclaimedTasks = $this->workflowHelper->getUnclaimedTasks();
        return $this->getDataTable($workflowUnclaimedTasks);
    }
    
    protected function getDataTable($tasks) {
        $unclaimedTasks = $this->mapWorkflowTransaction($tasks);
        return Datatables::collection($unclaimedTasks)->make(true);  
    }

    protected function mapWorkflowTransaction($tasks) {
        $unclaimedTasks = [];
        if ($tasks ) {           
            $records = json_decode(json_encode($tasks->{'task-summary'}), true);

            if (sizeof($records) > 0) {
                $workflowIds = array_column($records, 'task-proc-inst-id');
                $tranactions = Transaction::with(["buyer", "seller", "property", "service"])->whereIn('workflow_instance_id', $workflowIds)->orderBy('id')->get();         

                $instanceIdMappedRecords = [];
                foreach ($records as $record) {
                    $instanceIdMappedRecords[$record["task-proc-inst-id"]] = $record;
                }

                foreach($tranactions as $tranaction) {
                    $unclaimedTasks[] = ["data" => ["transaction" => $tranaction, "workflow" => $instanceIdMappedRecords[$tranaction["workflow_instance_id"]]]];
                }
            }
        }
        return $unclaimedTasks;
    }


    public function claimTask($transactionId, $taskId) {
        $transaction = Transaction::find($transactionId); 
        $user = Sentinel::getUser();
        $this->workflowHelper->claimTask($taskId);
        $transaction->assigned_to = $user->id;
        $transaction->task_id = $taskId;
        $transaction->save();
        return "<strong>Success:</strong> Application Claim Successful";
    }
    
    public function releaseTask($transactionId, $taskId) {
        $transaction = Transaction::find($transactionId);         
        $this->workflowHelper->releaseTask($taskId);
        $transaction->assigned_to = null;
        $transaction->task_id = null;
        $transaction->save();
        return "<strong>Success:</strong> Application Released Successfully";
    }

    public function initiate($transactionId) {
        $transaction = Transaction::with(["buyer", "notary"])->where("id", "=", $transactionId)->first();
        
        $workflowInstanceId = $this->workflowHelper->startChangeOwnershipWorkflow($transaction);
        if ($workflowInstanceId)  {
            $transaction->workflow_instance_id = $workflowInstanceId;
            $transaction->status = "APPLICATION_SUBMITTED";
            $transaction->update();
            Session::flash('message', 'Application submitted successfully for processing, ref no ' . $workflowInstanceId);
            Session::flash('alert-class', 'alert-success');             
        } else {
            Session::flash('message', 'Failed to submit the application for processing. Please try again or contact administrator');
            Session::flash('alert-class', 'alert-danger');
        }
        return redirect('admin/transactions/' . $transactionId);
    }
    
    public function index() {
        return view("admin.workflow.index");
    }
    
    protected function isWorkflowInstanceAssignedToCurrentUser($user, $workflowId) {
        $currentTaskAssignment = $this->workflowHelper->getCurrentWorkflowInstance($workflowId);
        if (isset($currentTaskAssignment->{"active-user-tasks"})) {  
            $currentAssignedUser = $currentTaskAssignment->{"active-user-tasks"}->{"task-summary"}[0]->{"task-actual-owner"};
            $assignedToCurrentUser = (str_contains($user->email, $currentAssignedUser))?true:false;     
            return $assignedToCurrentUser; 
        }
        return null;
    }
    
    protected function getRoleChecklist($user) {
        $role = $user->getRoles()->pluck('id')->first();    
        $rolechecklist = Checklist::where("role_id", "=", $role)->get();  
        return $rolechecklist;    
    }
}
