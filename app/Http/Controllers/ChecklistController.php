<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateChecklistRequest;
use App\Http\Requests\UpdateChecklistRequest;
use App\Repositories\ChecklistRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Checklist;
use Flash;
use Sentinel;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ChecklistController extends InfyOmBaseController
{
    /** @var  ChecklistRepository */
    private $checklistRepository;

    public function __construct(ChecklistRepository $checklistRepo)
    {
        $this->checklistRepository = $checklistRepo;
    }

    
    
    public function getRoleChecklist() {
        $user = Sentinel::getUser();      
        $role = $user->getRoles()->pluck('id')->first();    
        
        $rolechecklist = Checklist::where("role_id", "=", $role);
        
        return view("admin.checklists.role-checklist", ["$rolechecklist" => $rolechecklist]);    
    }
    
    /**
     * Display a listing of the Checklist.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->checklistRepository->pushCriteria(new RequestCriteria($request));
        $checklists = $this->checklistRepository->all();
        return view('admin.checklists.index')->with('checklists', $checklists);
    }

    /**
     * Show the form for creating a new Checklist.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Sentinel::getRoleRepository()->all();
        return view('admin.checklists.create',  compact('roles'));
    }

    /**
     * Store a newly created Checklist in storage.
     *
     * @param CreateChecklistRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $checklist  = new Checklist();
        $checklist = $checklist->fill($request->only($checklist->getFillable()));             
        $checklist->save();
        //$checklist = $this->checklistRepository->create($input);

        Flash::success('Checklist saved successfully.');

        return redirect(route('admin.checklists.index'));
    }

    /**
     * Display the specified Checklist.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $checklist = $this->checklistRepository->findWithoutFail($id);

        if (empty($checklist)) {
            Flash::error('Checklist not found');

            return redirect(route('checklists.index'));
        }

        return view('admin.checklists.show')->with('checklist', $checklist);
    }

    /**
     * Show the form for editing the specified Checklist.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $checklist = $this->checklistRepository->findWithoutFail($id);
        $groups = Sentinel::getRoleRepository()->all();

        if (empty($checklist)) {
            Flash::error('Checklist not found');

            return redirect(route('checklists.index'));
        }

        return view('admin.checklists.edit', ['checklist' => $checklist, "groups" => $groups]);
    }

    /**
     * Update the specified Checklist in storage.
     *
     * @param  int              $id
     * @param UpdateChecklistRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateChecklistRequest $request)
    {
        $checklist = $this->checklistRepository->findWithoutFail($id);

        

        if (empty($checklist)) {
            Flash::error('Checklist not found');

            return redirect(route('checklists.index'));
        }

        $checklist = $this->checklistRepository->update($request->all(), $id);

        Flash::success('Checklist updated successfully.');

        return redirect(route('admin.checklists.index'));
    }

    /**
     * Remove the specified Checklist from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.checklists.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Checklist::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.checklists.index'))->with('success', Lang::get('message.success.delete'));

       }

}
