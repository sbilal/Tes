<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateLedgerScanningRequest;
use App\Http\Requests\UpdateLedgerScanningRequest;
use App\Repositories\LedgerScanningRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\LedgerScanning;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class LedgerScanningController extends InfyOmBaseController
{
    /** @var  LedgerScanningRepository */
    private $ledgerScanningRepository;

    public function __construct(LedgerScanningRepository $ledgerScanningRepo)
    {
        $this->ledgerScanningRepository = $ledgerScanningRepo;
    }

    /**
     * Display a listing of the LedgerScanning.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->ledgerScanningRepository->pushCriteria(new RequestCriteria($request));
        $ledgerScannings = $this->ledgerScanningRepository->all();
        return view('admin.ledgerScannings.index')
            ->with('ledgerScannings', $ledgerScannings);
    }

    /**
     * Show the form for creating a new LedgerScanning.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.ledgerScannings.create');
    }

    /**
     * Store a newly created LedgerScanning in storage.
     *
     * @param CreateLedgerScanningRequest $request
     *
     * @return Response
     */
    public function store(CreateLedgerScanningRequest $request)
    {
        $input = $request->all();

        $ledgerScanning = $this->ledgerScanningRepository->create($input);

        Flash::success('LedgerScanning saved successfully.');

        return redirect(route('admin.ledgerScannings.index'));
    }

    /**
     * Display the specified LedgerScanning.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ledgerScanning = $this->ledgerScanningRepository->findWithoutFail($id);

        if (empty($ledgerScanning)) {
            Flash::error('LedgerScanning not found');

            return redirect(route('ledgerScannings.index'));
        }

        return view('admin.ledgerScannings.show')->with('ledgerScanning', $ledgerScanning);
    }

    /**
     * Show the form for editing the specified LedgerScanning.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ledgerScanning = $this->ledgerScanningRepository->findWithoutFail($id);

        if (empty($ledgerScanning)) {
            Flash::error('LedgerScanning not found');

            return redirect(route('ledgerScannings.index'));
        }

        return view('admin.ledgerScannings.edit')->with('ledgerScanning', $ledgerScanning);
    }

    /**
     * Update the specified LedgerScanning in storage.
     *
     * @param  int              $id
     * @param UpdateLedgerScanningRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLedgerScanningRequest $request)
    {
        $ledgerScanning = $this->ledgerScanningRepository->findWithoutFail($id);

        

        if (empty($ledgerScanning)) {
            Flash::error('LedgerScanning not found');

            return redirect(route('ledgerScannings.index'));
        }

        $ledgerScanning = $this->ledgerScanningRepository->update($request->all(), $id);

        Flash::success('LedgerScanning updated successfully.');

        return redirect(route('admin.ledgerScannings.index'));
    }

    /**
     * Remove the specified LedgerScanning from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.ledgerScannings.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = LedgerScanning::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.ledgerScannings.index'))->with('success', Lang::get('message.success.delete'));

       }

}
