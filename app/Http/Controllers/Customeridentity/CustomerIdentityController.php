<?php

namespace App\Http\Controllers\Customeridentity;

use App\Http\Requests;
use App\Http\Requests\Customeridentity\CreateCustomerIdentityRequest;
use App\Http\Requests\Customeridentity\UpdateCustomerIdentityRequest;
use App\Repositories\Customeridentity\CustomerIdentityRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Customeridentity\CustomerIdentity;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CustomerIdentityController extends InfyOmBaseController
{
    /** @var  CustomerIdentityRepository */
    private $customerIdentityRepository;

    public function __construct(CustomerIdentityRepository $customerIdentityRepo)
    {
        $this->customerIdentityRepository = $customerIdentityRepo;
    }

    /**
     * Display a listing of the CustomerIdentity.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->customerIdentityRepository->pushCriteria(new RequestCriteria($request));
        $customerIdentities = $this->customerIdentityRepository->all();
        return view('admin.customerIdentity.customerIdentities.index')
            ->with('customerIdentities', $customerIdentities);
    }

    /**
     * Show the form for creating a new CustomerIdentity.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.customerIdentity.customerIdentities.create');
    }

    /**
     * Store a newly created CustomerIdentity in storage.
     *
     * @param CreateCustomerIdentityRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerIdentityRequest $request)
    {
        $input = $request->all();

        $customerIdentity = $this->customerIdentityRepository->create($input);

        Flash::success('CustomerIdentity saved successfully.');

        return redirect(route('admin.customerIdentity.customerIdentities.index'));
    }

    /**
     * Display the specified CustomerIdentity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customerIdentity = $this->customerIdentityRepository->findWithoutFail($id);

        if (empty($customerIdentity)) {
            Flash::error('CustomerIdentity not found');

            return redirect(route('customerIdentities.index'));
        }

        return view('admin.customerIdentity.customerIdentities.show')->with('customerIdentity', $customerIdentity);
    }

    /**
     * Show the form for editing the specified CustomerIdentity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customerIdentity = $this->customerIdentityRepository->findWithoutFail($id);

        if (empty($customerIdentity)) {
            Flash::error('CustomerIdentity not found');

            return redirect(route('customerIdentities.index'));
        }

        return view('admin.customerIdentity.customerIdentities.edit')->with('customerIdentity', $customerIdentity);
    }

    /**
     * Update the specified CustomerIdentity in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerIdentityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerIdentityRequest $request)
    {
        $customerIdentity = $this->customerIdentityRepository->findWithoutFail($id);

        

        if (empty($customerIdentity)) {
            Flash::error('CustomerIdentity not found');

            return redirect(route('customerIdentities.index'));
        }

        $customerIdentity = $this->customerIdentityRepository->update($request->all(), $id);

        Flash::success('CustomerIdentity updated successfully.');

        return redirect(route('admin.customerIdentity.customerIdentities.index'));
    }

    /**
     * Remove the specified CustomerIdentity from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.customerIdentity.customerIdentities.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = CustomerIdentity::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.customerIdentity.customerIdentities.index'))->with('success', Lang::get('message.success.delete'));

       }

}
