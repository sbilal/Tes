<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Property;
use App\Models\PropertyOwnership;
use App\Models\PropertyOwnershipApplication;
use Illuminate\Http\Request;
use App\Models\Notary;
use Illuminate\Support\Facades\Log;
use App\Models\Transaction;
use App\Models\service;
use DB;
use App\Helpers\ErpHelper;

class ChangeOwnershipController extends Controller {

    public function index(Request $request) {
        $request->session()->forget("transaction");
        return view('admin.changeOwnership.process', compact('notary'));
    }
    
     

}
