<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDocumentShelfRequest;
use App\Http\Requests\UpdateDocumentShelfRequest;
use App\Repositories\DocumentShelfRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\DocumentShelf;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DocumentShelfController extends InfyOmBaseController
{
    /** @var  DocumentShelfRepository */
    private $documentShelfRepository;

    public function __construct(DocumentShelfRepository $documentShelfRepo)
    {
        $this->documentShelfRepository = $documentShelfRepo;
    }

    /**
     * Display a listing of the DocumentShelf.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->documentShelfRepository->pushCriteria(new RequestCriteria($request));
        $documentShelves = $this->documentShelfRepository->all();
        return view('admin.documentShelves.index')
            ->with('documentShelves', $documentShelves);
    }

    /**
     * Show the form for creating a new DocumentShelf.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.documentShelves.create');
    }

    /**
     * Store a newly created DocumentShelf in storage.
     *
     * @param CreateDocumentShelfRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentShelfRequest $request)
    {
        $input = $request->all();

        $documentShelf = $this->documentShelfRepository->create($input);

        Flash::success('DocumentShelf saved successfully.');

        return redirect(route('admin.documentShelves.index'));
    }

    /**
     * Display the specified DocumentShelf.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $documentShelf = $this->documentShelfRepository->findWithoutFail($id);

        if (empty($documentShelf)) {
            Flash::error('DocumentShelf not found');

            return redirect(route('documentShelves.index'));
        }

        return view('admin.documentShelves.show')->with('documentShelf', $documentShelf);
    }

    /**
     * Show the form for editing the specified DocumentShelf.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $documentShelf = $this->documentShelfRepository->findWithoutFail($id);

        if (empty($documentShelf)) {
            Flash::error('DocumentShelf not found');

            return redirect(route('documentShelves.index'));
        }

        return view('admin.documentShelves.edit')->with('documentShelf', $documentShelf);
    }

    /**
     * Update the specified DocumentShelf in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentShelfRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentShelfRequest $request)
    {
        $documentShelf = $this->documentShelfRepository->findWithoutFail($id);

        

        if (empty($documentShelf)) {
            Flash::error('DocumentShelf not found');

            return redirect(route('documentShelves.index'));
        }

        $documentShelf = $this->documentShelfRepository->update($request->all(), $id);

        Flash::success('DocumentShelf updated successfully.');

        return redirect(route('admin.documentShelves.index'));
    }

    /**
     * Remove the specified DocumentShelf from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.documentShelves.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = DocumentShelf::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.documentShelves.index'))->with('success', Lang::get('message.success.delete'));

       }

}
