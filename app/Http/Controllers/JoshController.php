<?php namespace App\Http\Controllers;


use App\Blog;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Sentinel;
use Analytics;
use View;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Charts;
use App\Datatable;
use App\User;
use Illuminate\Support\Facades\DB;
use Spatie\Analytics\Period;
use Illuminate\Support\Carbon;
use File;
use App\Models\Transaction;

class JoshController extends Controller {

    /**
     * Message bag.
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $messageBag = null;

    /**
     * Initializer.
     *
     */
    public function __construct()
    {
        $this->messageBag = new MessageBag;

    }


    public function showView($name=null)
    {
        if(View::exists('admin/'.$name))
        {
            if(Sentinel::check())
                return view('admin.'.$name);
            else
                return redirect('admin/signin')->with('error', 'You must be logged in!');
        }
        else
        {
            abort('404');
        }
    }

    public function activityLogData()
    {
        $logs = Activity::get(['causer_id', 'log_name', 'description','created_at']);
        return DataTables::of($logs)
            ->make(true);
    }

    public function showHome() {   
        if($user = Sentinel::check()) { 
            $transactions = Transaction::where('assigned_to', $user->id)->get();

            $my_role_transactions = DB::select('SELECT * FROM transactions where assigned_to in(
                                        select first_name from users where id in (
                                        select user_id from role_users where role_id in (select role_id from role_users where user_id = '.$user->id.')))');

            $transactions = Transaction::where('assigned_to', $user->id)->get();
            
            if ($user->inRole('admin')) {
                return view('admin.index1',compact('transactions','my_role_transactions'));
            }             
        }
        else
            return redirect('admin/signin')->with('error', 'You must be logged in!');
    }

}