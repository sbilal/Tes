<?php

namespace App\Http\Controllers\Propertystatus;

use App\Http\Requests;
use App\Http\Requests\Propertystatus\CreatePropertyStatusRequest;
use App\Http\Requests\Propertystatus\UpdatePropertyStatusRequest;
use App\Repositories\Propertystatus\PropertyStatusRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Propertystatus\PropertyStatus;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PropertyStatusController extends InfyOmBaseController
{
    /** @var  PropertyStatusRepository */
    private $propertyStatusRepository;

    public function __construct(PropertyStatusRepository $propertyStatusRepo)
    {
        $this->propertyStatusRepository = $propertyStatusRepo;
    }

    /**
     * Display a listing of the PropertyStatus.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->propertyStatusRepository->pushCriteria(new RequestCriteria($request));
        $propertyStatuses = $this->propertyStatusRepository->all();
        return view('admin.propertyStatus.propertyStatuses.index')
            ->with('propertyStatuses', $propertyStatuses);
    }

    /**
     * Show the form for creating a new PropertyStatus.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.propertyStatus.propertyStatuses.create');
    }

    /**
     * Store a newly created PropertyStatus in storage.
     *
     * @param CreatePropertyStatusRequest $request
     *
     * @return Response
     */
    public function store(CreatePropertyStatusRequest $request)
    {
        $input = $request->all();

        $propertyStatus = $this->propertyStatusRepository->create($input);

        Flash::success('PropertyStatus saved successfully.');

        return redirect(route('admin.propertyStatus.propertyStatuses.index'));
    }

    /**
     * Display the specified PropertyStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propertyStatus = $this->propertyStatusRepository->findWithoutFail($id);

        if (empty($propertyStatus)) {
            Flash::error('PropertyStatus not found');

            return redirect(route('propertyStatuses.index'));
        }

        return view('admin.propertyStatus.propertyStatuses.show')->with('propertyStatus', $propertyStatus);
    }

    /**
     * Show the form for editing the specified PropertyStatus.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propertyStatus = $this->propertyStatusRepository->findWithoutFail($id);

        if (empty($propertyStatus)) {
            Flash::error('PropertyStatus not found');

            return redirect(route('propertyStatuses.index'));
        }

        return view('admin.propertyStatus.propertyStatuses.edit')->with('propertyStatus', $propertyStatus);
    }

    /**
     * Update the specified PropertyStatus in storage.
     *
     * @param  int              $id
     * @param UpdatePropertyStatusRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropertyStatusRequest $request)
    {
        $propertyStatus = $this->propertyStatusRepository->findWithoutFail($id);

        

        if (empty($propertyStatus)) {
            Flash::error('PropertyStatus not found');

            return redirect(route('propertyStatuses.index'));
        }

        $propertyStatus = $this->propertyStatusRepository->update($request->all(), $id);

        Flash::success('PropertyStatus updated successfully.');

        return redirect(route('admin.propertyStatus.propertyStatuses.index'));
    }

    /**
     * Remove the specified PropertyStatus from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.propertyStatus.propertyStatuses.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = PropertyStatus::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.propertyStatus.propertyStatuses.index'))->with('success', Lang::get('message.success.delete'));

       }

}
