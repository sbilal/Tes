<?php


namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Response;
use Illuminate\Http\Request;
use Sentinel;



class PermissionController extends Controller
{
    public function roleList()
    {
        $roles = Sentinel::getRoleRepository()->all();

        return view('admin.permissions.roles-permission-list',compact('roles'));
           
    }

    public function permissionList(Request $request)
    {
        $slug = $request->input('slug');
        
        $permission_list = Sentinel::getRoleRepository()
                            ->select('permissions')
                            ->where('slug' , $slug)
                            ->first();
                          
        $permission_list = json_decode($permission_list, true);

        $permission_list= $permission_list['permissions'];
                          
        return view('admin.permissions.create',compact('permission_list'));

    }


    public function permissionUpdate(Request $request)
    {
        $permissions = $request->input('permissions');
        $slug = $request->input('slug');
        $routes_array = [];
        foreach ($permissions as $key => $permission) {
           $routes_array=array($permission => true) + $routes_array; 
        }
        
        $group = Sentinel::findRoleBySlug($slug);
        $group->permissions = $routes_array;
        $group->save();
        return 'pass';
    }

}
