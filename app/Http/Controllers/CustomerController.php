<?php


namespace App\Http\Controllers;

use Sentinel;
use App\Http\Requests;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Customer;
use Session;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Property;
use App\Models\propertyOwnership;
use App\Models\District;
use App\Models\SubDivision;
use App\Models\Zoning;
use Yajra\DataTables\Facades\DataTables;
use App\Helpers\ErpConnector;
use App\Helpers\ErpHelper;




class CustomerController extends InfyOmBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->customerRepository->pushCriteria(new RequestCriteria($request));
        $customers = $this->customerRepository->all();
        return view('admin.customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
                echo "notary access " . Sentinel::hasAccess("notaries") . "</br>";
                echo "property access " . Sentinel::hasAccess("proerties");
                echo "route " . (Sentinel::hasAccess(\Request::route()->getName()));
        $districts = District::pluck('name', 'id');
        $sub_divisions = SubDivision::pluck('Name', 'DistrictId');
        $cities = Zoning::pluck('zone', 'id');
        return view('admin.customers.create',compact('districts','sub_divisions','cities'));
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();

        $customer = $this->customerRepository->create($input);

        Flash::success('Customer saved successfully.');

        return redirect(route('admin.customers.index'));
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

     
        $customer = $this->customerRepository->findWithoutFail($id);


        $districts = District::select('name')
                        ->where('id', '=', $customer->district)
                        ->first();
        $sub_divisions = SubDivision::select('Name')
                        ->where('DistrictId', '=', $customer->sub_division)
                        ->first();
        $cities    =      Zoning::select('zone')
                        ->where('id', '=', $customer->zone)
                        ->first();


        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

      //  return view('admin.customers.show')->with('customer', $customer);
        return view('admin.customers.show',[ 'customer'=>$customer,'districts'=>$districts, 'sub_divisions'=>$sub_divisions,'cities'=>$cities] );
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->findWithoutFail($id);


        // echo '<pre>';
        // print_r($customer);
        // echo '</pre>';
        // exit();

        $districts = District::pluck('name', 'id');
        $sub_divisions = SubDivision::pluck('Name', 'DistrictId');
        $cities = Zoning::pluck('zone', 'id');
       
       
       
        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        return view('admin.customers.edit',[ 'customer'=>$customer,'districts'=>$districts, 'sub_divisions'=>$sub_divisions,'cities'=>$cities] );
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->findWithoutFail($id);

        

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect(route('customers.index'));
        }

        $customer = $this->customerRepository->update($request->all(), $id);

        Flash::success('Customer updated successfully.');

        return redirect(route('admin.customers.index'));
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.customers.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Customer::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.customers.index'))->with('success', Lang::get('message.success.delete'));

       }

       /**
     * insert Identification detail of customer.
     *
     * @param : customer_id,id_type,id_number,
     * issuing_authority,place_of_issue,file,
     * issuing_country,date_of_issue,expiry_date,points
     * 
     * @return Response
     */
      public function insertIdentificationDetail(Request $request) {
        $data['customer_id'] = $request->input('customer_id');
        $data['id_type'] = $request->input('id_type');
        $data['id_number'] = $request->input('id_number');
        $data['issuing_authority'] = $request->input('issuing_authority');
        $data['place_of_issue'] = $request->input('place_of_issue');
        $data['issuing_country'] = $request->input('issuing_country');
        $data['date_of_issue'] = $request->input('date_of_issue');
        $data['expiry_date'] = $request->input('expiry_date');
        $data['points'] = $request->input('points');
        if ($data['customer_id'] == "") {
            Session::flash('message', 'Please Enter Customer Id');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['id_type'] == "") {
            Session::flash('message', 'Please enter Id Type');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['issuing_authority'] == "") {
            Session::flash('message', 'Please enter Issuing Authority');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['place_of_issue'] == "") {
            Session::flash('message', 'Please enter place of issue');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['issuing_country'] == "") {
            Session::flash('message', 'Please enter issuing country');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['date_of_issue'] == "") {
            Session::flash('message', 'Please enter date of issue');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['expiry_date'] == "") {
            Session::flash('message', 'Please enter expiry date');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        if ($data['expiry_date'] < $data['date_of_issue']) {
            Session::flash('message', 'Expiry Date should be grater than issue date');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }

        if ($data['points'] == "") {
            Session::flash('message', 'Please enter points');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        $file = $request->file('file');
        if (empty($file)) {
            Session::flash('message', 'Please upload file');
            Session::flash('alert-class', 'alert-danger');
            return redirect('admin/customers/' . $data['customer_id']);
        }
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $picture = date('His') . $filename;
        $destinationPath = base_path() . '/public/uploads/userIdentificationFile';
        $file->move($destinationPath, $picture);
        $data['file'] = $picture;

        $result = Customer::inserCustomertIdenty($data);
        if ($result) {
            Session::flash('message', 'Identification Details Saved Successfully');
            Session::flash('alert-class', 'alert-success');
            return redirect('admin/customers/' . $data['customer_id']);
        } else {
            Session::flash('message', 'Identification Details not Saved something Went wrong');
            Session::flash('alert-class', 'alert-success');
            return redirect('admin/customers/' . $data['customer_id']);
        }
    }

       public function modalTest() {
            return View('admin.customers.modal-test');
       }
       
       public function getCustomersApi() {
           $modal = Customer::query();
           return Datatables::eloquent($modal)->toJson();
       }
       
       public function getCustomerApi($id) {
           $customer = Customer::find($id);
           return $customer;;
       }       

       public function getMainPartial($id) {
            $customer = Customer::find($id);
            return View('admin.customers.show_fields', compact('customer'));
       }       
       
       public function syncWithErp($id) {
           $erpHelper = new ErpHelper();
           $erpHelper->createCustomerInErp($id);
           Flash::success('Customer Sync with ERP successfully.');
           return 'Customer Sync with ERP successfully.';
       }

    public function popUp($id)
    {

       
        $customer = Customer::find($id);
            return View('admin.customers.customer-main-partial', compact('customer'));
    }
}
