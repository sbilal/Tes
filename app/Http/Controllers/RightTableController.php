<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRightTableRequest;
use App\Http\Requests\UpdateRightTableRequest;
use App\Repositories\RightTableRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\RightTable;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RightTableController extends InfyOmBaseController
{
    /** @var  RightTableRepository */
    private $rightTableRepository;

    public function __construct(RightTableRepository $rightTableRepo)
    {
        $this->rightTableRepository = $rightTableRepo;
    }

    /**
     * Display a listing of the RightTable.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->rightTableRepository->pushCriteria(new RequestCriteria($request));
        $rightTables = $this->rightTableRepository->all();
        return view('admin.rightTables.index')
            ->with('rightTables', $rightTables);
    }

    /**
     * Show the form for creating a new RightTable.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.rightTables.create');
    }

    /**
     * Store a newly created RightTable in storage.
     *
     * @param CreateRightTableRequest $request
     *
     * @return Response
     */
    public function store(CreateRightTableRequest $request)
    {
        $input = $request->all();

        $rightTable = $this->rightTableRepository->create($input);

        Flash::success('RightTable saved successfully.');

        return redirect(route('admin.rightTables.index'));
    }

    /**
     * Display the specified RightTable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $rightTable = $this->rightTableRepository->findWithoutFail($id);

        if (empty($rightTable)) {
            Flash::error('RightTable not found');

            return redirect(route('rightTables.index'));
        }

        return view('admin.rightTables.show')->with('rightTable', $rightTable);
    }

    /**
     * Show the form for editing the specified RightTable.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $rightTable = $this->rightTableRepository->findWithoutFail($id);

        if (empty($rightTable)) {
            Flash::error('RightTable not found');

            return redirect(route('rightTables.index'));
        }

        return view('admin.rightTables.edit')->with('rightTable', $rightTable);
    }

    /**
     * Update the specified RightTable in storage.
     *
     * @param  int              $id
     * @param UpdateRightTableRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRightTableRequest $request)
    {
        $rightTable = $this->rightTableRepository->findWithoutFail($id);

        

        if (empty($rightTable)) {
            Flash::error('RightTable not found');

            return redirect(route('rightTables.index'));
        }

        $rightTable = $this->rightTableRepository->update($request->all(), $id);

        Flash::success('RightTable updated successfully.');

        return redirect(route('admin.rightTables.index'));
    }

    /**
     * Remove the specified RightTable from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.rightTables.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = RightTable::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.rightTables.index'))->with('success', Lang::get('message.success.delete'));

       }

}
