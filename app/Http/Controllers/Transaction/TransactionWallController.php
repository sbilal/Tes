<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Transaction\CreateTransactionRequest;
use App\Http\Requests\Transaction\UpdateTransactionRequest;
use App\Repositories\Transaction\TransactionRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\File;
use App\Models\Transaction;
use App\Models\TransactionComments;
use App\Models\TransactionDocuments;
use App\Models\Property;
use App\Models\Checklist;
use App\Models\PropertyOwnershipApplication;
use App\Models\service;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Sentinel;
use Validator;
use Illuminate\Support\Facades\Log;
use App\Helpers\ErpHelper;
use Yajra\DataTables\Facades\DataTables;


class TransactionWallController extends InfyOmBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;
    protected $erpHelper;

    protected $wizardValidators = [
        "basic" => [ 'next_view' => 'required', 'service_id' => 'required', 'property_id' => 'required' ],
        "admin.transactions.summary-partial" => [ 'primary_owner' => 'required', "selected_customers" => 'required' ] ,
        "admin.invoice.invoice-items" => [  ] 
    ];

    public function __construct(TransactionRepository $transactionRepo) {
        $this->transactionRepository = $transactionRepo;
        $this->erpHelper = new ErpHelper();
    }
    
    public function getSummaryView(Request $request) {
        $transaction = $this->createOrUpdateTransaction($request);        
        
        $property = Property::with(['currentowners', 'currentowners.customer'])->where("id", $request->input('property_id'))->get()->first();
        $buyersInput = explode(",", $request->input('selected_customers'));
        $primaryBuyer = $request->input('primary_customer');
        $buyers = Customer::whereIn('id', $buyersInput)->get();
        $transaction = new Transaction(); 
        $transaction->settlement_date = $request->input('settlement_date');
        $transaction->settlement_amount = $request->input('settlement_amount');
        $transaction->notary_id = $request->input('notary_id');
        $transaction->buyer_id = $primaryBuyer;                
        return view('admin.transaction.summary', compact('property', "owners", "buyers", "primaryBuyer", "transaction"));
    }     
    
    public function getInvoice($transactionId) {
        $transaction = Transaction::with("service")->where("id", "=", $transactionId)->first();
        $serviceFees = $this->erpHelper->getServiceFees($transaction->service->code);
        return view('admin.invoice.invoice', compact("transaction", "serviceFees"));
    }
    
    public function makePayment(Request $request) {
        $transactionId = $request->input("transaction_id");        

        $transaction = Transaction::find($transactionId);
        $transaction->payment_amount = $request->input("payment_amount"); 
        $transaction->payment_date = $request->input("payment_date"); 
        $transaction->payment_method = $request->input("payment_method"); 
        $transaction->payment_customer = $request->input("payment_customer"); 
        $transaction->payment_customer_mobile = $request->input("payment_customer_mobile"); 
        $transaction->payment_reference_number = $request->input("payment_reference"); 
        $transaction->update();
        
        $transaction = $this->erpHelper->createErpInvoiceAndPayment($transaction);            
        $serviceFees = $this->erpHelper->getServiceFees($transaction->service->code);
        return view('admin.invoice.invoice-items', compact("transaction", "serviceFees"));            
    }
    
    public function finishApplicationWizard(Request $request) {        
        $transaction = $request->session()->get('transaction');
        if (!isset($transaction)) return "Something unexpected happened. Please contact administrator";                  
        
        $request->session()->forget("transaction");        
        return $transaction->id;
    }
    
    
    public function processApplicationWizard(Request $request) {        
        $validator = Validator::make($request->all(), $this->wizardValidators["basic"]);        
        
        if ($validator->fails()) {
            echo "ViewName (or) Service Name (or) Service Id (or) Property Id is missing";
            return;
        }        
        
        $nextView = $request->input("next_view");
        $service = $service = service::find($request->input("service_id"));                           ;
        $serviceFees = $this->erpHelper->getServiceFees($service->code);
        $property = Property::with(["currentowners"])->where("id", $request->input("property_id"))->get()->first();
        
        $validator = Validator::make($request->all(), $this->wizardValidators[$nextView]);        
        if ($validator->fails()) {
            echo "required fields missing to process view " . $nextView;
            return;
        }               
        
        $transaction = $this->createOrUpdateTransaction($request,$property); 
        $propertyOwnershipApplication = $this->createOrUpdatePropertyOwnershipApplications($request, $transaction);

        return view($nextView, ["transaction" => $transaction, "propertyOwnershipApplication" => $propertyOwnershipApplication, "property" => $property, "serviceFees" => $serviceFees]);
    }
    
    protected function createTransaction(Request $request) {                
        // create transaction 
        $transaction = $this->createOrUpdateTransaction($request);        

        // store property ownership applications
        $this->createOrUpdatePropertyOwnershipApplications($buyers, $request, $primaryBuyer, $transaction);
        
        $service = service::find($request->input("service_id"));                           
        $serviceFees = $this->erpHelper->getServiceFees($service->code);
        return view('admin.invoice.invoice-items', compact("transaction", "serviceFees"));
    }
    
    protected function createOrUpdateTransaction($request, $property) {
        $transaction = $request->session()->get('transaction');
        $isNewtransaction = isset($transaction)?false:true;
        $primaryBuyer = $request->input('primary_owner');      

        $settlementDate = ($request->has("settlement_date"))?date('Y-m-d H:i:s', strtotime($request->input("settlement_date"))):"";

        
        if ($isNewtransaction) {
            $transaction = new Transaction();                          
            $transaction = $transaction->fill($request->only($transaction->getFillable()));             
        } else {
            Log::info("Transaction exists, updating transaction "  . $transaction->id);
            $transaction = Transaction::find($transaction->id);           
            $transaction = $transaction->fill($request->only($transaction->getFillable()));                         
        }
  
        $transaction->buyer_id = $primaryBuyer;
        $transaction->seller_id = $property->primaryowner->first()->owner_id;
        $transaction->application_date = date("Y-m-d H:i:s");
        $transaction->settlement_date = $settlementDate;
        $transaction->status = "DRAFT";
        $transaction->assigned_to = Sentinel::getUser()->id;  
              
        try{
            if ($isNewtransaction) {
                Log::info("Creating new transaction");
                $transaction->save();
                $transaction->transaction_reference_number = "TR-" . $transaction->id;
                $request->session()->put('transaction', $transaction);
            } 
            $transaction->update();
        } catch(\Exception $e){
            Flash::error($e->getMessage());
            Log::error($e->getMessage());
        }      
        
        return $transaction;
    }
    
    protected function createOrUpdatePropertyOwnershipApplications($request, $transaction) {
        $buyers = explode(",", $request->input('selected_customers'));             
        PropertyOwnershipApplication::where("transaction_id", "=", $transaction->id)->delete();
        $primaryBuyer = $request->input('primary_owner');      
        
        foreach($buyers as $buyer) {
            $propertyOwnershipApplication = $this->makePropertyOwnershipApplication($buyer, $request, $primaryBuyer, $transaction);
            $propertyOwnershipApplication->save();            
        }
        
        $result = PropertyOwnershipApplication::where("transaction_id", "=", $transaction->id)->get();
        return $result;
    }
    
    protected function makePropertyOwnershipApplication($buyer, $request, $primaryBuyer, $transaction) {
        $propertyOwnershipApplication = new PropertyOwnershipApplication();
        $propertyOwnershipApplication = $propertyOwnershipApplication->fill($request->only($propertyOwnershipApplication->getFillable()));
        $propertyOwnershipApplication->settlement_date = date('Y-m-d H:i:s', strtotime($request->input("settlement_date")));
        $propertyOwnershipApplication->transaction_id = $transaction->id;
        $propertyOwnershipApplication->owner_id = $buyer;
        if ($primaryBuyer == $buyer) {
            $propertyOwnershipApplication->primary_owner = "Yes";
        } else {
            $propertyOwnershipApplication->primary_owner = "No";
        }      
        return $propertyOwnershipApplication;
    }
       
    
    public function addTransactionComments($transactionId, Request $request) {
        $transactionComments = new TransactionComments();
        if (!$request->input('comments')) {
            return ["Comments missing"];
        }
        
        $user = Sentinel::getUser();              

        $transactionComments->comments = $request->input('comments');
        $transactionComments->comment_date = date("Y-m-d H:i:s");
        $transactionComments->transaction_id = $transactionId;
        $transactionComments->updated_by = $user->id;
        $transactionComments->save();
        Flash::success('Comments added successfully.');
        return redirect("workflow/application/". $transactionId);
    }
    
    public function addTransactionDocument($transactionId, Request $request) {
        
        // Add check - allow add only if the workflow is assigned to the current user 
        // refer Workflow Controller -> getApplicaiton $assignedToCurrentUser
        
        $transactionDocument = new TransactionDocuments();
        $transactionDocument->document_type_id = $request->input("document_type_id");
        
        if (empty($request->input("document_type_id"))) {
            return ["Document Type is missing"];
        }
        $transactionDocument->comments = $request->input('comments');
        $transactionDocument->updated_by = Sentinel::getUser()->id;
        $transactionDocument->transaction_id = $transactionId;
        $transactionDocument->save();

        $document = $request->file('document');
        if (empty($document)) {
            return ["Document is missing"];
        }
        
        $filename = $document->getClientOriginalName();
        $extension = $document->getClientOriginalExtension();
        $transactionDocument =  $transactionDocument->id . "." . $extension;
        $destinationPath = base_path() . '/public/uploads/documents/';
        $document->move($destinationPath, $transactionDocument);
        Flash::success('Document Uploaded successfully.');
        return redirect("workflow/application/". $transactionId);
    }
    
    public function deleteTransactionComments($id) {
        $user = Sentinel::getUser();
        $record = TransactionComments::where("id", "=", $id)->where("updated_by", "=", $user->id)->get()->first();
        // get workflow record and check if the workflow is assigned to the user then allow delete 
        // checkCanDelete()
        if ($record) {
            $record->delete();
            Flash::success('Comments Deleted successfully.');
        } else {
            Flash::error('Cannot perform this operation. Either you dont have permissions to delete or the applicaiton state does not allow to perform this operation');
            return back();
        }
        return redirect("workflow/application/". $record->transaction_id);        
    }

    public function deleteTransactionDocument($id) {
        $user = Sentinel::getUser();
        $record = TransactionDocuments::where("id", "=", $id)->where("updated_by", "=", $user->id)->get()->first();
        // get workflow record and check if the workflow is assigned to the user then allow delete 
        // checkCanDelete()
        if ($record) {
            $record->delete();
            $filename = base_path() . '/public/uploads/documents/' . $record->id . ".pdf";
            if (File::exists($filename)) {
                File::delete($filename);
            }
            Flash::success('Document Deleted successfully.');
        } else {
            Flash::error('Cannot perform this operation. Either you dont have permissions to delete or the applicaiton state does not allow to perform this operation');
            return back();
        }
        return redirect("workflow/application/". $record->transaction_id);                
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->transactionRepository->pushCriteria(new RequestCriteria($request));
//        $transactions = $this->transactionRepository->all();

        // Get logged in Users transactions
        $transactions = DB::table('Transactions')
            ->join('users', function ($join) {
                $join->on('users.id', '=', 'Transactions.assigned_to')
                    ->where('Transactions.assigned_to', '=', Sentinel::getUser()->id);
            })
            ->join('customers', function ($join){
                $join->on('customers.id','=','Transactions.buyer_id');
            })
            ->join('customers as c', function ($join){
                $join->on('c.id','=','Transactions.seller_id');
            })
            ->select('Transactions.*','customers.first_name as buyerName','c.first_name as sellerName')
            ->get();


        return view('admin.transactions.wall_transaction')
            ->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new Transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.transactions.create');
    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param CreateTransactionRequest $request
     *
     * @return Response
     */
    public function store(CreateTransactionRequest $request)
    {
        $input = $request->all();

        $transaction = $this->transactionRepository->create($input);

        Flash::success('Transaction saved successfully.');

        return redirect(route('admin.transactions.index'));
    }

    /**
     * Display the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaction = Transaction::with("property", "buyer", "seller")->where("id", "=", $id)->get()->first();
        $propertyOwnershipApplication = PropertyOwnershipApplication::with("customer")->where("transaction_id", "=", $transaction->id)->get();
        $service  = service::find($transaction->service_id);                           ;
        $serviceFees = $this->erpHelper->getServiceFees($service->code);


        if (empty($transaction) || ($transaction->assigned_to != Sentinel::getuser()->id)) {
            Flash::error('Transaction not found');

            return redirect(route('admin.transactions.index'));
        }

        return view('admin.transactions.show', ['transaction' => $transaction, "property" => $transaction->property, "propertyOwnershipApplication" => $propertyOwnershipApplication, "serviceFees" => $serviceFees]);
    }

    /**
     * Show the form for editing the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transactions.index'));
        }

        return view('admin.transactions.edit')->with('transaction', $transaction);
    }

    /**
     * Update the specified Transaction in storage.
     *
     * @param  int              $id
     * @param UpdateTransactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransactionRequest $request)
    {
        $transaction = $this->transactionRepository->findWithoutFail($id);

        

        if (empty($transaction)) {
            Flash::error('Transaction not found');

            return redirect(route('transactions.index'));
        }

        $transaction = $this->transactionRepository->update($request->all(), $id);

        Flash::success('Transaction updated successfully.');

        return redirect(route('admin.transactions.index'));
    }

    /**
     * Remove the specified Transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null) {
           $sample = Transaction::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.transactions.index'))->with('success', Lang::get('message.success.delete'));
       }
       
       public function getRecentTransactionsApi() {
           $modal = Transaction::with(["buyer", "seller", "property", "service"]);
           return Datatables::eloquent($modal)->toJson();
       }      
       
       
}
