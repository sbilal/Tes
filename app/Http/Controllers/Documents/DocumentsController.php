<?php

namespace App\Http\Controllers\Documents;

use App\Http\Requests;
use App\Http\Requests\Documents\CreateDocumentsRequest;
use App\Http\Requests\Documents\UpdateDocumentsRequest;
use App\Repositories\Documents\DocumentsRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Documents\Documents;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DocumentsController extends InfyOmBaseController
{
    /** @var  DocumentsRepository */
    private $documentsRepository;

    public function __construct(DocumentsRepository $documentsRepo)
    {
        $this->documentsRepository = $documentsRepo;
    }

    /**
     * Display a listing of the Documents.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->documentsRepository->pushCriteria(new RequestCriteria($request));
        $documents = $this->documentsRepository->all();
        return view('admin.documents.documents.index')
            ->with('documents', $documents);
    }

    /**
     * Show the form for creating a new Documents.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.documents.documents.create');
    }

    /**
     * Store a newly created Documents in storage.
     *
     * @param CreateDocumentsRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentsRequest $request)
    {
        $input = $request->all();

        $documents = $this->documentsRepository->create($input);

        Flash::success('Documents saved successfully.');

        return redirect(route('admin.documents.documents.index'));
    }

    /**
     * Display the specified Documents.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $documents = $this->documentsRepository->findWithoutFail($id);

        if (empty($documents)) {
            Flash::error('Documents not found');

            return redirect(route('documents.index'));
        }

        return view('admin.documents.documents.show')->with('documents', $documents);
    }

    /**
     * Show the form for editing the specified Documents.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $documents = $this->documentsRepository->findWithoutFail($id);

        if (empty($documents)) {
            Flash::error('Documents not found');

            return redirect(route('documents.index'));
        }

        return view('admin.documents.documents.edit')->with('documents', $documents);
    }

    /**
     * Update the specified Documents in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentsRequest $request)
    {
        $documents = $this->documentsRepository->findWithoutFail($id);

        

        if (empty($documents)) {
            Flash::error('Documents not found');

            return redirect(route('documents.index'));
        }

        $documents = $this->documentsRepository->update($request->all(), $id);

        Flash::success('Documents updated successfully.');

        return redirect(route('admin.documents.documents.index'));
    }

    /**
     * Remove the specified Documents from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.documents.documents.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Documents::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.documents.documents.index'))->with('success', Lang::get('message.success.delete'));

       }

}
