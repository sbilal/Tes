<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDocumentTypeRequest;
use App\Http\Requests\UpdateDocumentTypeRequest;
use App\Repositories\DocumentTypeRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\DocumentType;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class DocumentTypeController extends InfyOmBaseController
{
    /** @var  DocumentTypeRepository */
    private $documentTypeRepository;

    public function __construct(DocumentTypeRepository $documentTypeRepo)
    {
        $this->documentTypeRepository = $documentTypeRepo;
    }

    /**
     * Display a listing of the DocumentType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->documentTypeRepository->pushCriteria(new RequestCriteria($request));
        $documentTypes = $this->documentTypeRepository->all();
        return view('admin.documentTypes.index')
            ->with('documentTypes', $documentTypes);
    }

    /**
     * Show the form for creating a new DocumentType.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.documentTypes.create');
    }

    /**
     * Store a newly created DocumentType in storage.
     *
     * @param CreateDocumentTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateDocumentTypeRequest $request)
    {
        $input = $request->all();

        $documentType = $this->documentTypeRepository->create($input);

        Flash::success('DocumentType saved successfully.');

        return redirect(route('admin.documentTypes.index'));
    }

    /**
     * Display the specified DocumentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $documentType = $this->documentTypeRepository->findWithoutFail($id);

        if (empty($documentType)) {
            Flash::error('DocumentType not found');

            return redirect(route('documentTypes.index'));
        }

        return view('admin.documentTypes.show')->with('documentType', $documentType);
    }

    /**
     * Show the form for editing the specified DocumentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $documentType = $this->documentTypeRepository->findWithoutFail($id);

        if (empty($documentType)) {
            Flash::error('DocumentType not found');

            return redirect(route('documentTypes.index'));
        }

        return view('admin.documentTypes.edit')->with('documentType', $documentType);
    }

    /**
     * Update the specified DocumentType in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentTypeRequest $request)
    {
        $documentType = $this->documentTypeRepository->findWithoutFail($id);

        

        if (empty($documentType)) {
            Flash::error('DocumentType not found');

            return redirect(route('documentTypes.index'));
        }

        $documentType = $this->documentTypeRepository->update($request->all(), $id);

        Flash::success('DocumentType updated successfully.');

        return redirect(route('admin.documentTypes.index'));
    }

    /**
     * Remove the specified DocumentType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.documentTypes.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = DocumentType::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.documentTypes.index'))->with('success', Lang::get('message.success.delete'));

       }

}
