<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateZoningRequest;
use App\Http\Requests\UpdateZoningRequest;
use App\Repositories\ZoningRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Zoning;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ZoningController extends InfyOmBaseController
{
    /** @var  ZoningRepository */
    private $zoningRepository;

    public function __construct(ZoningRepository $zoningRepo)
    {
        $this->zoningRepository = $zoningRepo;
    }

    /**
     * Display a listing of the Zoning.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->zoningRepository->pushCriteria(new RequestCriteria($request));
        $zonings = $this->zoningRepository->all();
        return view('admin.zonings.index')
            ->with('zonings', $zonings);
    }

    /**
     * Show the form for creating a new Zoning.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.zonings.create');
    }

    /**
     * Store a newly created Zoning in storage.
     *
     * @param CreateZoningRequest $request
     *
     * @return Response
     */
    public function store(CreateZoningRequest $request)
    {
        $input = $request->all();

        $zoning = $this->zoningRepository->create($input);

        Flash::success('Zoning saved successfully.');

        return redirect(route('admin.zonings.index'));
    }

    /**
     * Display the specified Zoning.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $zoning = $this->zoningRepository->findWithoutFail($id);

        if (empty($zoning)) {
            Flash::error('Zoning not found');

            return redirect(route('zonings.index'));
        }

        return view('admin.zonings.show')->with('zoning', $zoning);
    }

    /**
     * Show the form for editing the specified Zoning.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $zoning = $this->zoningRepository->findWithoutFail($id);

        if (empty($zoning)) {
            Flash::error('Zoning not found');

            return redirect(route('zonings.index'));
        }

        return view('admin.zonings.edit')->with('zoning', $zoning);
    }

    /**
     * Update the specified Zoning in storage.
     *
     * @param  int              $id
     * @param UpdateZoningRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateZoningRequest $request)
    {
        $zoning = $this->zoningRepository->findWithoutFail($id);

        

        if (empty($zoning)) {
            Flash::error('Zoning not found');

            return redirect(route('zonings.index'));
        }

        $zoning = $this->zoningRepository->update($request->all(), $id);

        Flash::success('Zoning updated successfully.');

        return redirect(route('admin.zonings.index'));
    }

    /**
     * Remove the specified Zoning from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.zonings.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Zoning::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.zonings.index'))->with('success', Lang::get('message.success.delete'));

       }

}
