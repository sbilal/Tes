<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePropertyClaimantDocumentsRequest;
use App\Http\Requests\UpdatePropertyClaimantDocumentsRequest;
use App\Repositories\PropertyClaimantDocumentsRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\PropertyClaimantDocuments;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PropertyClaimantDocumentsController extends InfyOmBaseController
{
    /** @var  PropertyClaimantDocumentsRepository */
    private $propertyClaimantDocumentsRepository;

    public function __construct(PropertyClaimantDocumentsRepository $propertyClaimantDocumentsRepo)
    {
        $this->propertyClaimantDocumentsRepository = $propertyClaimantDocumentsRepo;
    }

    /**
     * Display a listing of the PropertyClaimantDocuments.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->propertyClaimantDocumentsRepository->pushCriteria(new RequestCriteria($request));
        $propertyClaimantDocuments = $this->propertyClaimantDocumentsRepository->all();
        return view('admin.propertyClaimantDocuments.index')
            ->with('propertyClaimantDocuments', $propertyClaimantDocuments);
    }

    /**
     * Show the form for creating a new PropertyClaimantDocuments.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.propertyClaimantDocuments.create');
    }

    /**
     * Store a newly created PropertyClaimantDocuments in storage.
     *
     * @param CreatePropertyClaimantDocumentsRequest $request
     *
     * @return Response
     */
    public function store(CreatePropertyClaimantDocumentsRequest $request)
    {
        $input = $request->all();

        $propertyClaimantDocuments = $this->propertyClaimantDocumentsRepository->create($input);

        Flash::success('PropertyClaimantDocuments saved successfully.');

        return redirect(route('admin.propertyClaimantDocuments.index'));
    }

    /**
     * Display the specified PropertyClaimantDocuments.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propertyClaimantDocuments = $this->propertyClaimantDocumentsRepository->findWithoutFail($id);

        if (empty($propertyClaimantDocuments)) {
            Flash::error('PropertyClaimantDocuments not found');

            return redirect(route('propertyClaimantDocuments.index'));
        }

        return view('admin.propertyClaimantDocuments.show')->with('propertyClaimantDocuments', $propertyClaimantDocuments);
    }

    /**
     * Show the form for editing the specified PropertyClaimantDocuments.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propertyClaimantDocuments = $this->propertyClaimantDocumentsRepository->findWithoutFail($id);

        if (empty($propertyClaimantDocuments)) {
            Flash::error('PropertyClaimantDocuments not found');

            return redirect(route('propertyClaimantDocuments.index'));
        }

        return view('admin.propertyClaimantDocuments.edit')->with('propertyClaimantDocuments', $propertyClaimantDocuments);
    }

    /**
     * Update the specified PropertyClaimantDocuments in storage.
     *
     * @param  int              $id
     * @param UpdatePropertyClaimantDocumentsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropertyClaimantDocumentsRequest $request)
    {
        $propertyClaimantDocuments = $this->propertyClaimantDocumentsRepository->findWithoutFail($id);

        

        if (empty($propertyClaimantDocuments)) {
            Flash::error('PropertyClaimantDocuments not found');

            return redirect(route('propertyClaimantDocuments.index'));
        }

        $propertyClaimantDocuments = $this->propertyClaimantDocumentsRepository->update($request->all(), $id);

        Flash::success('PropertyClaimantDocuments updated successfully.');

        return redirect(route('admin.propertyClaimantDocuments.index'));
    }

    /**
     * Remove the specified PropertyClaimantDocuments from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.propertyClaimantDocuments.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = PropertyClaimantDocuments::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.propertyClaimantDocuments.index'))->with('success', Lang::get('message.success.delete'));

       }

}
