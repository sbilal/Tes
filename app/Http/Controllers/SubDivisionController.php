<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSubDivisionRequest;
use App\Http\Requests\UpdateSubDivisionRequest;
use App\Repositories\SubDivisionRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\SubDivision;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\District;

class SubDivisionController extends InfyOmBaseController
{
    /** @var  SubDivisionRepository */
    private $subDivisionRepository;

    public function __construct(SubDivisionRepository $subDivisionRepo)
    {
        $this->subDivisionRepository = $subDivisionRepo;
    }

    /**
     * Display a listing of the SubDivision.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

       // $this->subDivisionRepository->pushCriteria(new RequestCriteria($request));
        //$subDivisions = $this->subDivisionRepository->all();

        $subDivisions = SubDivision::join('District', 'District.id', '=', 'sub_division.DistrictId')
                        ->select('sub_division.*','District.name')
                        ->get();

        return view('admin.subDivisions.index')
            ->with('subDivisions', $subDivisions);
    }

    /**
     * Show the form for creating a new SubDivision.
     *
     * @return Response
     */
    public function create()
    {

        $districts = District::pluck('name', 'id');
        return view('admin.subDivisions.create', compact('districts'));
    }

    /**
     * Store a newly created SubDivision in storage.
     *
     * @param CreateSubDivisionRequest $request
     *
     * @return Response
     */
    public function store(CreateSubDivisionRequest $request)
    {
        $input = $request->all();

        $subDivision = $this->subDivisionRepository->create($input);

        Flash::success('SubDivision saved successfully.');

        return redirect(route('admin.subDivisions.index'));
    }

    /**
     * Display the specified SubDivision.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //$subDivision = $this->subDivisionRepository->findWithoutFail($id);

          $subDivision = SubDivision::join('District', 'District.id', '=', 'sub_division.DistrictId')
                            ->where('sub_division.id', '=' , $id)  
                            ->first();

        if (empty($subDivision)) {
            Flash::error('SubDivision not found');

            return redirect(route('subDivisions.index'));
        }

        return view('admin.subDivisions.show')->with('subDivision', $subDivision);
    }

    /**
     * Show the form for editing the specified SubDivision.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        //$subDivision = $this->subDivisionRepository->findWithoutFail($id);

        $districts = District::pluck('name', 'id');

        $subDivision = SubDivision::join('District', 'District.id', '=', 'sub_division.DistrictId')
                            ->where('sub_division.id', '=' , $id)  
                            ->first();


        if (empty($subDivision)) {
            Flash::error('SubDivision not found');

            return redirect(route('subDivisions.index'));
        }

        return view('admin.subDivisions.edit', compact('subDivision', 'districts' ));
    }

    /**
     * Update the specified SubDivision in storage.
     *
     * @param  int              $id
     * @param UpdateSubDivisionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubDivisionRequest $request)
    {
        $subDivision = $this->subDivisionRepository->findWithoutFail($id);

        

        if (empty($subDivision)) {
            Flash::error('SubDivision not found');

            return redirect(route('subDivisions.index'));
        }

        $subDivision = $this->subDivisionRepository->update($request->all(), $id);

        Flash::success('SubDivision updated successfully.');

        return redirect(route('admin.subDivisions.index'));
    }

    /**
     * Remove the specified SubDivision from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.subDivisions.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = SubDivision::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.subDivisions.index'))->with('success', Lang::get('message.success.delete'));

       }

}
