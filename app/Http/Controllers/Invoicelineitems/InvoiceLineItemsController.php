<?php

namespace App\Http\Controllers\Invoicelineitems;

use App\Http\Requests;
use App\Http\Requests\Invoicelineitems\CreateInvoiceLineItemsRequest;
use App\Http\Requests\Invoicelineitems\UpdateInvoiceLineItemsRequest;
use App\Repositories\Invoicelineitems\InvoiceLineItemsRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Invoicelineitems\InvoiceLineItems;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class InvoiceLineItemsController extends InfyOmBaseController
{
    /** @var  InvoiceLineItemsRepository */
    private $invoiceLineItemsRepository;

    public function __construct(InvoiceLineItemsRepository $invoiceLineItemsRepo)
    {
        $this->invoiceLineItemsRepository = $invoiceLineItemsRepo;
    }

    /**
     * Display a listing of the InvoiceLineItems.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->invoiceLineItemsRepository->pushCriteria(new RequestCriteria($request));
        $invoiceLineItems = $this->invoiceLineItemsRepository->all();
        return view('admin.invoiceLineItems.invoiceLineItems.index')
            ->with('invoiceLineItems', $invoiceLineItems);
    }

    /**
     * Show the form for creating a new InvoiceLineItems.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.invoiceLineItems.invoiceLineItems.create');
    }

    /**
     * Store a newly created InvoiceLineItems in storage.
     *
     * @param CreateInvoiceLineItemsRequest $request
     *
     * @return Response
     */
    public function store(CreateInvoiceLineItemsRequest $request)
    {
        $input = $request->all();

        $invoiceLineItems = $this->invoiceLineItemsRepository->create($input);

        Flash::success('InvoiceLineItems saved successfully.');

        return redirect(route('admin.invoiceLineItems.invoiceLineItems.index'));
    }

    /**
     * Display the specified InvoiceLineItems.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $invoiceLineItems = $this->invoiceLineItemsRepository->findWithoutFail($id);

        if (empty($invoiceLineItems)) {
            Flash::error('InvoiceLineItems not found');

            return redirect(route('invoiceLineItems.index'));
        }

        return view('admin.invoiceLineItems.invoiceLineItems.show')->with('invoiceLineItems', $invoiceLineItems);
    }

    /**
     * Show the form for editing the specified InvoiceLineItems.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $invoiceLineItems = $this->invoiceLineItemsRepository->findWithoutFail($id);

        if (empty($invoiceLineItems)) {
            Flash::error('InvoiceLineItems not found');

            return redirect(route('invoiceLineItems.index'));
        }

        return view('admin.invoiceLineItems.invoiceLineItems.edit')->with('invoiceLineItems', $invoiceLineItems);
    }

    /**
     * Update the specified InvoiceLineItems in storage.
     *
     * @param  int              $id
     * @param UpdateInvoiceLineItemsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInvoiceLineItemsRequest $request)
    {
        $invoiceLineItems = $this->invoiceLineItemsRepository->findWithoutFail($id);

        

        if (empty($invoiceLineItems)) {
            Flash::error('InvoiceLineItems not found');

            return redirect(route('invoiceLineItems.index'));
        }

        $invoiceLineItems = $this->invoiceLineItemsRepository->update($request->all(), $id);

        Flash::success('InvoiceLineItems updated successfully.');

        return redirect(route('admin.invoiceLineItems.invoiceLineItems.index'));
    }

    /**
     * Remove the specified InvoiceLineItems from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.invoiceLineItems.invoiceLineItems.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = InvoiceLineItems::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.invoiceLineItems.invoiceLineItems.index'))->with('success', Lang::get('message.success.delete'));

       }

}
