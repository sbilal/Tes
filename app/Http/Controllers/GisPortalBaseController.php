<?php

namespace App\Http\Controllers;

use Sentinel;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class GisPortalBaseController extends JoshController
{
    public function getModalDelete($id = null, Request $request) {
        $error = '';
        $model = '';
        $confirm_route =  $request->input("confirm_route"); 
        $id = $request->input("id");
        //$confirm_route = route($dest_route, ['id' => $request->input("id")] ); 
        return View('admin.layouts/modal_delete_confirmation', compact('error','model', 'confirm_route', "id"));

    }    
}