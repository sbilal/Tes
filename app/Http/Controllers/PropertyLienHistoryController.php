<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatepropertylienhistoryRequest;
use App\Http\Requests\UpdatepropertylienhistoryRequest;
use App\Repositories\propertylienhistoryRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\propertylienhistory;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class propertylienhistoryController extends InfyOmBaseController
{
    /** @var  propertylienhistoryRepository */
    private $propertylienhistoryRepository;

    public function __construct(propertylienhistoryRepository $propertylienhistoryRepo)
    {
        $this->propertylienhistoryRepository = $propertylienhistoryRepo;
    }

    /**
     * Display a listing of the propertylienhistory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $this->propertylienhistoryRepository->pushCriteria(new RequestCriteria($request));
        $propertylienhistories = $this->propertylienhistoryRepository->all();
        return view('admin.propertylienhistories.index')
            ->with('propertylienhistories', $propertylienhistories);
    }

    /**
     * Show the form for creating a new propertylienhistory.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.propertylienhistories.create');
    }

    /**
     * Store a newly created propertylienhistory in storage.
     *
     * @param CreatepropertylienhistoryRequest $request
     *
     * @return Response
     */
    public function store(CreatepropertylienhistoryRequest $request)
    {
        $input = $request->all();

        $propertylienhistory = $this->propertylienhistoryRepository->create($input);

        Flash::success('Propertylienhistory saved successfully.');

        return redirect(route('admin.propertylienhistories.index'));
    }

    /**
     * Display the specified propertylienhistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $propertylienhistory = $this->propertylienhistoryRepository->findWithoutFail($id);

        if (empty($propertylienhistory)) {
            Flash::error('Propertylienhistory not found');

            return redirect(route('propertylienhistories.index'));
        }

        return view('admin.propertylienhistories.show')->with('propertylienhistory', $propertylienhistory);
    }

    /**
     * Show the form for editing the specified propertylienhistory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $propertylienhistory = $this->propertylienhistoryRepository->findWithoutFail($id);

        if (empty($propertylienhistory)) {
            Flash::error('Propertylienhistory not found');

            return redirect(route('propertylienhistories.index'));
        }

        return view('admin.propertylienhistories.edit')->with('propertylienhistory', $propertylienhistory);
    }

    /**
     * Update the specified propertylienhistory in storage.
     *
     * @param  int              $id
     * @param UpdatepropertylienhistoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepropertylienhistoryRequest $request)
    {
        $propertylienhistory = $this->propertylienhistoryRepository->findWithoutFail($id);

        

        if (empty($propertylienhistory)) {
            Flash::error('Propertylienhistory not found');

            return redirect(route('propertylienhistories.index'));
        }

        $propertylienhistory = $this->propertylienhistoryRepository->update($request->all(), $id);

        Flash::success('propertylienhistory updated successfully.');

        return redirect(route('admin.propertylienhistories.index'));
    }

    /**
     * Remove the specified propertylienhistory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.propertylienhistories.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = propertylienhistory::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.propertylienhistories.index'))->with('success', Lang::get('message.success.delete'));

       }

}
