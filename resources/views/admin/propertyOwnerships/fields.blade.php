<!-- Property Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_id', 'Property Id:') !!}
    {!! Form::number('property_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Owner' => 'Owner', ' Sold' => ' Sold', '  Application' => '  Application'], null, ['class' => 'form-control']) !!}
</div>

<!-- Purchase Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('purchase_date', 'Purchase Date:') !!}
    {!! Form::date('purchase_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchase Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('purchase_price', 'Purchase Price:') !!}
    {!! Form::number('purchase_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sale_date', 'Sale Date:') !!}
    {!! Form::date('sale_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    {!! Form::number('sale_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Service Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('service_id', 'Service Id:') !!}
    {!! Form::number('service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    {!! Form::number('owner_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Primary Owner Field -->
<div class="form-group col-sm-12">
    {!! Form::label('primary_owner', 'Primary Owner:') !!}
    <label class="radio-inline">
        {!! Form::radio('primary_owner', "Yes", null) !!} Yes
    </label>
    <label class="radio-inline">
        {!! Form::radio('primary_owner', " No", null) !!}  No
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.propertyOwnerships.index') !!}" class="btn btn-default">Cancel</a>
</div>
