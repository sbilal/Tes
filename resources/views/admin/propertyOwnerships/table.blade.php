<table class="table table-responsive table-striped table-bordered" id="propertyOwnerships-table" width="100%">
    <thead>
     <tr>
        <th>Property Id</th>
        <th>Status</th>
        <th>Purchase Date</th>
        <th>Purchase Price</th>
        <th>Sale Date</th>
        <th>Sale Price</th>
        <th>Service Id</th>
        <th>Owner Id</th>
        <th>Primary Owner</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($propertyOwnerships as $propertyOwnership)
        <tr>
            <td>  

              <a  data-toggle="modal" class="property-details"  data-target="#property-details-id" propertyId="{{$propertyOwnership->property_id}}" >{!! $propertyOwnership->property_id !!}
                    </a>
                     </td>
            <td>{!! $propertyOwnership->status !!}</td>
            <td>{!! $propertyOwnership->purchase_date !!}</td>
            <td>{!! $propertyOwnership->purchase_price !!}</td>
            <td>{!! $propertyOwnership->sale_date !!}</td>
            <td>{!! $propertyOwnership->sale_price !!}</td>
            <td>{!! $propertyOwnership->service_id !!}</td>
            <td>
                
                 <a  data-toggle="modal" data-target="#customer-details-id" class="customer-details" customerId="{{$propertyOwnership->owner_id}}"  >{!! $propertyOwnership->owner_id !!}
                </a>
            </td>
            <td>{!! $propertyOwnership->primary_owner !!}</td>
            <td>
                 <a href="{{ route('admin.propertyOwnerships.show', collect($propertyOwnership)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view propertyOwnership"></i>
                 </a>
                 <a href="{{ route('admin.propertyOwnerships.edit', collect($propertyOwnership)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit propertyOwnership"></i>
                 </a>
                 <a href="{{ route('admin.propertyOwnerships.confirm-delete', collect($propertyOwnership)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete propertyOwnership"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('admin.properties.property-popup')
@include('admin.customers.customer-popup')
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#propertyOwnerships-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#propertyOwnerships-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );



       </script>

@stop