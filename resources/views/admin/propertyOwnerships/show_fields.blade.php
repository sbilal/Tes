<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $propertyOwnership->id !!}</p>
    <hr>
</div>

<!-- Property Id Field -->
<div class="form-group">
    {!! Form::label('property_id', 'Property Id:') !!}
    <p>{!! $propertyOwnership->property_id !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $propertyOwnership->status !!}</p>
    <hr>
</div>

<!-- Purchase Date Field -->
<div class="form-group">
    {!! Form::label('purchase_date', 'Purchase Date:') !!}
    <p>{!! $propertyOwnership->purchase_date !!}</p>
    <hr>
</div>

<!-- Purchase Price Field -->
<div class="form-group">
    {!! Form::label('purchase_price', 'Purchase Price:') !!}
    <p>{!! $propertyOwnership->purchase_price !!}</p>
    <hr>
</div>

<!-- Sale Date Field -->
<div class="form-group">
    {!! Form::label('sale_date', 'Sale Date:') !!}
    <p>{!! $propertyOwnership->sale_date !!}</p>
    <hr>
</div>

<!-- Sale Price Field -->
<div class="form-group">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    <p>{!! $propertyOwnership->sale_price !!}</p>
    <hr>
</div>

<!-- Service Id Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service Id:') !!}
    <p>{!! $propertyOwnership->service_id !!}</p>
    <hr>
</div>

<!-- Owner Id Field -->
<div class="form-group">
    {!! Form::label('owner_id', 'Owner Id:') !!}
    <p>{!! $propertyOwnership->owner_id !!}</p>
    <hr>
</div>

<!-- Primary Owner Field -->
<div class="form-group">
    {!! Form::label('primary_owner', 'Primary Owner:') !!}
    <p>{!! $propertyOwnership->primary_owner !!}</p>
    <hr>
</div>

