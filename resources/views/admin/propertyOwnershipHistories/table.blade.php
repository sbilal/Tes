<table class="table table-responsive table-striped table-bordered" id="propertyOwnershipHistories-table" width="100%">
    <thead>
     <tr>
        <th>Property Id</th>
        <th>Property Ownership Id</th>
        <th>Status</th>
        <th>Purchase Date</th>
        <th>Purchase Price</th>
        <th>Sale Date</th>
        <th>Sale Price</th>
        <th>Service Id</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($propertyOwnershipHistories as $propertyOwnershipHistory)
        <tr>
            <td>{!! $propertyOwnershipHistory->property_id !!}</td>
            <td>{!! $propertyOwnershipHistory->property_ownership_id !!}</td>
            <td>{!! $propertyOwnershipHistory->status !!}</td>
            <td>{!! $propertyOwnershipHistory->purchase_date !!}</td>
            <td>{!! $propertyOwnershipHistory->purchase_price !!}</td>
            <td>{!! $propertyOwnershipHistory->sale_date !!}</td>
            <td>{!! $propertyOwnershipHistory->sale_price !!}</td>
            <td>{!! $propertyOwnershipHistory->service_id !!}</td>
            <td>
                 <a href="{{ route('admin.propertyOwnershipHistories.show', collect($propertyOwnershipHistory)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view propertyOwnershipHistory"></i>
                 </a>
                 <a href="{{ route('admin.propertyOwnershipHistories.edit', collect($propertyOwnershipHistory)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit propertyOwnershipHistory"></i>
                 </a>
                 <a href="{{ route('admin.propertyOwnershipHistories.confirm-delete', collect($propertyOwnershipHistory)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete propertyOwnershipHistory"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#propertyOwnershipHistories-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#propertyOwnershipHistories-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop