<!-- Property Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_id', 'Property Id:') !!}
    {!! Form::text('property_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Ownership Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_ownership_id', 'Property Ownership Id:') !!}
    {!! Form::text('property_ownership_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchase Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('purchase_date', 'Purchase Date:') !!}
    {!! Form::date('purchase_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Purchase Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('purchase_price', 'Purchase Price:') !!}
    {!! Form::text('purchase_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sale_date', 'Sale Date:') !!}
    {!! Form::date('sale_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    {!! Form::text('sale_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Service Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('service_id', 'Service Id:') !!}
    {!! Form::text('service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.propertyOwnershipHistories.index') !!}" class="btn btn-default">Cancel</a>
</div>
