<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $propertyOwnershipHistory->id !!}</p>
    <hr>
</div>

<!-- Property Id Field -->
<div class="form-group">
    {!! Form::label('property_id', 'Property Id:') !!}
    <p>{!! $propertyOwnershipHistory->property_id !!}</p>
    <hr>
</div>

<!-- Property Ownership Id Field -->
<div class="form-group">
    {!! Form::label('property_ownership_id', 'Property Ownership Id:') !!}
    <p>{!! $propertyOwnershipHistory->property_ownership_id !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $propertyOwnershipHistory->status !!}</p>
    <hr>
</div>

<!-- Purchase Date Field -->
<div class="form-group">
    {!! Form::label('purchase_date', 'Purchase Date:') !!}
    <p>{!! $propertyOwnershipHistory->purchase_date !!}</p>
    <hr>
</div>

<!-- Purchase Price Field -->
<div class="form-group">
    {!! Form::label('purchase_price', 'Purchase Price:') !!}
    <p>{!! $propertyOwnershipHistory->purchase_price !!}</p>
    <hr>
</div>

<!-- Sale Date Field -->
<div class="form-group">
    {!! Form::label('sale_date', 'Sale Date:') !!}
    <p>{!! $propertyOwnershipHistory->sale_date !!}</p>
    <hr>
</div>

<!-- Sale Price Field -->
<div class="form-group">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    <p>{!! $propertyOwnershipHistory->sale_price !!}</p>
    <hr>
</div>

<!-- Service Id Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service Id:') !!}
    <p>{!! $propertyOwnershipHistory->service_id !!}</p>
    <hr>
</div>

