<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $propertylienhistory->id !!}</p>
    <hr>
</div>

<!-- Property Ownership Id Field -->
<div class="form-group">
    {!! Form::label('property_ownership_id', 'Property Ownership Id:') !!}
    <p>{!! $propertylienhistory->property_ownership_id !!}</p>
    <hr>
</div>

<!-- Bank Id Field -->
<div class="form-group">
    {!! Form::label('bank_id', 'Bank Id:') !!}
    <p>{!! $propertylienhistory->bank_id !!}</p>
    <hr>
</div>

<!-- Loan Amount Field -->
<div class="form-group">
    {!! Form::label('loan_amount', 'Loan Amount:') !!}
    <p>{!! $propertylienhistory->loan_amount !!}</p>
    <hr>
</div>

<!-- Loan Date Field -->
<div class="form-group">
    {!! Form::label('loan_date', 'Loan Date:') !!}
    <p>{!! $propertylienhistory->loan_date !!}</p>
    <hr>
</div>

<!-- Release Date Field -->
<div class="form-group">
    {!! Form::label('release_date', 'Release Date:') !!}
    <p>{!! $propertylienhistory->release_date !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $propertylienhistory->status !!}</p>
    <hr>
</div>

