<!-- Property Ownership Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_ownership_id', 'Property Ownership Id:') !!}
    {!! Form::text('property_ownership_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('bank_id', 'Bank Id:') !!}
    {!! Form::text('bank_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Loan Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('loan_amount', 'Loan Amount:') !!}
    {!! Form::text('loan_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Loan Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('loan_date', 'Loan Date:') !!}
    {!! Form::date('loan_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Release Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('release_date', 'Release Date:') !!}
    {!! Form::date('release_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.propertylienhistories.index') !!}" class="btn btn-default">Cancel</a>
</div>
