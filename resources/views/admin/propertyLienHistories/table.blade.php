<table class="table table-responsive table-striped table-bordered" id="propertylienhistories-table" width="100%">
    <thead>
     <tr>
        <th>Property Ownership Id</th>
        <th>Bank Id</th>
        <th>Loan Amount</th>
        <th>Loan Date</th>
        <th>Release Date</th>
        <th>Status</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($propertylienhistories as $propertylienhistory)
        <tr>
            <td>
                <a  data-toggle="modal" data-target="#customer-details-id"  class="customer-details" customerId="{{$propertylienhistory->property_ownership_id}}">{!! $propertylienhistory->property_ownership_id !!}
                </a>
            </td>
            <td>{!! $propertylienhistory->bank_id !!}</td>
            <td>{!! $propertylienhistory->loan_amount !!}</td>
            <td>{!! $propertylienhistory->loan_date !!}</td>
            <td>{!! $propertylienhistory->release_date !!}</td>
            <td>{!! $propertylienhistory->status !!}</td>
            <td>
                 <a href="{{ route('admin.propertylienhistories.show', collect($propertylienhistory)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view propertylienhistory"></i>
                 </a>
                 <a href="{{ route('admin.propertylienhistories.edit', collect($propertylienhistory)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit propertylienhistory"></i>
                 </a>
                 <a href="{{ route('admin.propertylienhistories.confirm-delete', collect($propertylienhistory)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete propertylienhistory"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('admin.customers.customer-popup')
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#propertylienhistories-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#propertylienhistories-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop