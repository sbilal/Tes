<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ledgerScanning->id !!}</p>
    <hr>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $ledgerScanning->date !!}</p>
    <hr>
</div>

<!-- Registration No Field -->
<div class="form-group">
    {!! Form::label('registration_no', 'Registration No:') !!}
    <p>{!! $ledgerScanning->registration_no !!}</p>
    <hr>
</div>

<!-- Magaca Field -->
<div class="form-group">
    {!! Form::label('magaca', 'Magaca:') !!}
    <p>{!! $ledgerScanning->magaca !!}</p>
    <hr>
</div>

<!-- Degree Field -->
<div class="form-group">
    {!! Form::label('degree', 'Degree:') !!}
    <p>{!! $ledgerScanning->degree !!}</p>
    <hr>
</div>

<!-- Waxx Field -->
<div class="form-group">
    {!! Form::label('waxx', 'Waxx:') !!}
    <p>{!! $ledgerScanning->waxx !!}</p>
    <hr>
</div>

<!-- Xaag Field -->
<div class="form-group">
    {!! Form::label('xaag', 'Xaag:') !!}
    <p>{!! $ledgerScanning->xaag !!}</p>
    <hr>
</div>

<!-- Quab Field -->
<div class="form-group">
    {!! Form::label('quab', 'Quab:') !!}
    <p>{!! $ledgerScanning->quab !!}</p>
    <hr>
</div>

<!-- Xof Field -->
<div class="form-group">
    {!! Form::label('xof', 'Xof:') !!}
    <p>{!! $ledgerScanning->xof !!}</p>
    <hr>
</div>

<!-- Cabbie Field -->
<div class="form-group">
    {!! Form::label('cabbie', 'Cabbie:') !!}
    <p>{!! $ledgerScanning->cabbie !!}</p>
    <hr>
</div>

<!-- Gr Date Field -->
<div class="form-group">
    {!! Form::label('gr_date', 'Gr Date:') !!}
    <p>{!! $ledgerScanning->gr_date !!}</p>
    <hr>
</div>

<!-- Gr Number Field -->
<div class="form-group">
    {!! Form::label('gr_number', 'Gr Number:') !!}
    <p>{!! $ledgerScanning->gr_number !!}</p>
    <hr>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $ledgerScanning->amount !!}</p>
    <hr>
</div>

<!-- Orign No Field -->
<div class="form-group">
    {!! Form::label('orign_no', 'Orign No:') !!}
    <p>{!! $ledgerScanning->orign_no !!}</p>
    <hr>
</div>

<!-- File No Field -->
<div class="form-group">
    {!! Form::label('file_no', 'File No:') !!}
    <p>{!! $ledgerScanning->file_no !!}</p>
    <hr>
</div>

<!-- Teysaro Field -->
<div class="form-group">
    {!! Form::label('teysaro', 'Teysaro:') !!}
    <p>{!! $ledgerScanning->teysaro !!}</p>
    <hr>
</div>

