<table class="table table-responsive table-striped table-bordered" id="ledgerScannings-table" width="100%">
    <thead>
     <tr>
        <th>Date</th>
        <th>Registration No</th>
        <th>Magaca</th>
        <th>Degree</th>
        <th>Waxx</th>
        <th>Xaag</th>
        <th>Quab</th>
        <th>Xof</th>
        <th>Cabbie</th>
        <th>Gr Date</th>
        <th>Gr Number</th>
        <th>Amount</th>
        <th>Orign No</th>
        <th>File No</th>
        <th>Teysaro</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($ledgerScannings as $ledgerScanning)
        <tr>
            <td>{!! $ledgerScanning->date !!}</td>
            <td>{!! $ledgerScanning->registration_no !!}</td>
            <td>{!! $ledgerScanning->magaca !!}</td>
            <td>{!! $ledgerScanning->degree !!}</td>
            <td>{!! $ledgerScanning->waxx !!}</td>
            <td>{!! $ledgerScanning->xaag !!}</td>
            <td>{!! $ledgerScanning->quab !!}</td>
            <td>{!! $ledgerScanning->xof !!}</td>
            <td>{!! $ledgerScanning->cabbie !!}</td>
            <td>{!! $ledgerScanning->gr_date !!}</td>
            <td>{!! $ledgerScanning->gr_number !!}</td>
            <td>{!! $ledgerScanning->amount !!}</td>
            <td>{!! $ledgerScanning->orign_no !!}</td>
            <td>{!! $ledgerScanning->file_no !!}</td>
            <td>{!! $ledgerScanning->teysaro !!}</td>
            <td>
                 <a href="{{ route('admin.ledgerScannings.show', collect($ledgerScanning)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view ledgerScanning"></i>
                 </a>
                 <a href="{{ route('admin.ledgerScannings.edit', collect($ledgerScanning)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit ledgerScanning"></i>
                 </a>
                 <a href="{{ route('admin.ledgerScannings.confirm-delete', collect($ledgerScanning)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete ledgerScanning"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#ledgerScannings-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#ledgerScannings-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop