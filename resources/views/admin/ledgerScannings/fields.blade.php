<!-- Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control']) !!}
</div>

<!-- Registration No Field -->
<div class="form-group col-sm-12">
    {!! Form::label('registration_no', 'Registration No:') !!}
    {!! Form::text('registration_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Magaca Field -->
<div class="form-group col-sm-12">
    {!! Form::label('magaca', 'Magaca:') !!}
    {!! Form::text('magaca', null, ['class' => 'form-control']) !!}
</div>

<!-- Degree Field -->
<div class="form-group col-sm-12">
    {!! Form::label('degree', 'Degree:') !!}
    {!! Form::text('degree', null, ['class' => 'form-control']) !!}
</div>

<!-- Waxx Field -->
<div class="form-group col-sm-12">
    {!! Form::label('waxx', 'Waxx:') !!}
    {!! Form::text('waxx', null, ['class' => 'form-control']) !!}
</div>

<!-- Xaag Field -->
<div class="form-group col-sm-12">
    {!! Form::label('xaag', 'Xaag:') !!}
    {!! Form::text('xaag', null, ['class' => 'form-control']) !!}
</div>

<!-- Quab Field -->
<div class="form-group col-sm-12">
    {!! Form::label('quab', 'Quab:') !!}
    {!! Form::text('quab', null, ['class' => 'form-control']) !!}
</div>

<!-- Xof Field -->
<div class="form-group col-sm-12">
    {!! Form::label('xof', 'Xof:') !!}
    {!! Form::text('xof', null, ['class' => 'form-control']) !!}
</div>

<!-- Cabbie Field -->
<div class="form-group col-sm-12">
    {!! Form::label('cabbie', 'Cabbie:') !!}
    {!! Form::text('cabbie', null, ['class' => 'form-control']) !!}
</div>

<!-- Gr Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('gr_date', 'Gr Date:') !!}
    {!! Form::date('gr_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Gr Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('gr_number', 'Gr Number:') !!}
    {!! Form::text('gr_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Orign No Field -->
<div class="form-group col-sm-12">
    {!! Form::label('orign_no', 'Orign No:') !!}
    {!! Form::text('orign_no', null, ['class' => 'form-control']) !!}
</div>

<!-- File No Field -->
<div class="form-group col-sm-12">
    {!! Form::label('file_no', 'File No:') !!}
    {!! Form::text('file_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Teysaro Field -->
<div class="form-group col-sm-12">
    {!! Form::label('teysaro', 'Teysaro:') !!}
    {!! Form::text('teysaro', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.ledgerScannings.index') !!}" class="btn btn-default">Cancel</a>
</div>
