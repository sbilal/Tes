@extends('admin/layouts/default')

@section('title')
services
@parent
@stop

{{-- Page content --}}
@section('content')

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-danger ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Unauthorized
                </h4>
            </div>
            <br />
            <div class="panel-body table-responsive">
                 You are not authorized to view this page                
            </div>
        </div>
 </div>
</section>
@stop
