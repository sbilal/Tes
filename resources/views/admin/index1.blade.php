@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    GIS 
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

    <link href="{{ asset('assets/vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" media="all" href="{{ asset('assets/vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pages/only_dashboard.css') }}"/>
    <meta name="_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/smalotDatepicker/css/bootstrap-datetimepicker.min.css') }}">

@stop

{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1>Welcome to Dashboard</h1>
        <ol class="breadcrumb">
            <li class="active">
                <a href="#">
                    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
                    Dashboard
                </a>
            </li>
        </ol>
    </section>
    <!--table-->
    
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box primary">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="responsive" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            My Transactions List

                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        <table class="table table-bordered table-striped table-condensed flip-content" >
                            <thead>
                                <tr>
                                    <th>Reference No</th>
                                    <th class="numeric">Service</th>
                                    <th>Application Date</th>
                                    <th class="numeric">Property</th>
                                    <th class="numeric">Buyer</th>
                                    <th class="numeric">Seller</th>                                            
                                    <th class="">Buy Date</th>
                                    <th class="numeric">Amount</th>
                                    <th class="">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{$transaction->payment_reference_number}}</td>
                                    <td class="numeric">{{$transaction->service->name}}</td>
                                    <td class="numeric">{{date('d-m-Y', strtotime($transaction->application_date))}}</td>                                            
                                    <td class="numeric">{{$transaction->property->parcel_id}}</td>
                                    <td class="numeric">{{$transaction->buyer->full_name}}</td>
                                    <td class="numeric">{{$transaction->seller->full_name}}</td>
                                    <td class="">{{date('d-m-Y', strtotime($transaction->buy_date))}}</td>                                            
                                    <td class="numeric">${{$transaction->buy_amount}}</td>
                                    <td class="">{{$transaction->status}}</td>
                                </tr>
                                @endforeach                                        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end table -->

    <!--my role transction table-->

    <section class="content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN SAMPLE TABLE PORTLET-->
                        <div class="portlet box primary">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="livicon" data-name="responsive" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                    My Role Queue
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll">
                                <table class="table table-bordered table-striped table-condensed flip-content" >
                                    <thead class="flip-content">
                                        <tr>
                                            <th class="numeric">Service Id</th>
                                            <th>Application Date</th>
                                            <th class="numeric">Buyer Id</th>
                                            <th class="numeric">Seller Id</th>
                                            <th class="numeric">Property Id</th>
                                            <th class="">Buy Date</th>
                                            <th class="">Sale Date</th>
                                            <th class="numeric">Buy Amount</th>
                                            <th class="numeric">Sale Amount</th>
                                            <th class="">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($my_role_transactions as $my_role_transaction)
                                        <tr>
                                            <td class="numeric">{{$my_role_transaction->service_id}}</td>
                                            <td>{{$my_role_transaction->application_date}}</td>
                                            <td class="numeric">{{$my_role_transaction->buyer_id}}</td>
                                            <td class="numeric">{{$my_role_transaction->seller_id}}</td>
                                            <td class="numeric">{{$my_role_transaction->property_id}}</td>
                                            <td class="">{{$my_role_transaction->buy_date}}</td>
                                            <td class="">{{$my_role_transaction->settlement_date}}</td>
                                            <td class="numeric">${{$my_role_transaction->buy_amount}}</td>
                                            <td class="numeric">${{$my_role_transaction->sale_amount}}</td>
                                            <td class="">{{$my_role_transaction->status}}</td>
                                        </tr>

                                        @endforeach
                                       
                                     
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- END SAMPLE TABLE PORTLET-->
                        
                        
                        
                      
                        
                    </div>
                </div>
            </section>
    <!--end table -->

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/moment/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/smalotDatepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- EASY PIE CHART JS -->
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jquery-easyPieChart/js/jquery.easingpie.js') }}"></script>
    <!--for calendar-->
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/fullcalendar/js/fullcalendar.min.js') }}" type="text/javascript"></script>
    <!--   Realtime Server Load  -->
    <script src="{{ asset('assets/vendors/flotchart/js/jquery.flot.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/flotchart/js/jquery.flot.resize.js') }}" type="text/javascript"></script>
    <!--Sparkline Chart-->
    <script src="{{ asset('assets/vendors/sparklinecharts/jquery.sparkline.js') }}"></script>
    <!-- Back to Top-->
    <script type="text/javascript" src="{{ asset('assets/vendors/countUp_js/js/countUp.js') }}"></script>
    <!--   maps -->
    <script src="{{ asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/js/pages/dashboard.js') }}" type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script src="{{ asset('assets/js/pages/table-responsive.js') }}"></script>


@stop