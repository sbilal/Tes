<!-- Customerid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('CustomerId', 'Customerid:') !!}
    {!! Form::text('CustomerId', null, ['class' => 'form-control']) !!}
</div>

<!-- Propertyid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('PropertyId', 'Propertyid:') !!}
    {!! Form::text('PropertyId', null, ['class' => 'form-control']) !!}
</div>

<!-- Documentname Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DocumentName', 'Documentname:') !!}
    {!! Form::text('DocumentName', null, ['class' => 'form-control']) !!}
</div>

<!-- Documenttypeid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DocumentTypeId', 'Documenttypeid:') !!}
    {!! Form::text('DocumentTypeId', null, ['class' => 'form-control']) !!}
</div>

<!-- Documentlink Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DocumentLink', 'Documentlink:') !!}
    {!! Form::text('DocumentLink', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.documents.documents.index') !!}" class="btn btn-default">Cancel</a>
</div>
