<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $documents->id !!}</p>
    <hr>
</div>

<!-- Customerid Field -->
<div class="form-group">
    {!! Form::label('CustomerId', 'Customerid:') !!}
    <p>{!! $documents->CustomerId !!}</p>
    <hr>
</div>

<!-- Propertyid Field -->
<div class="form-group">
    {!! Form::label('PropertyId', 'Propertyid:') !!}
    <p>{!! $documents->PropertyId !!}</p>
    <hr>
</div>

<!-- Documentname Field -->
<div class="form-group">
    {!! Form::label('DocumentName', 'Documentname:') !!}
    <p>{!! $documents->DocumentName !!}</p>
    <hr>
</div>

<!-- Documenttypeid Field -->
<div class="form-group">
    {!! Form::label('DocumentTypeId', 'Documenttypeid:') !!}
    <p>{!! $documents->DocumentTypeId !!}</p>
    <hr>
</div>

<!-- Documentlink Field -->
<div class="form-group">
    {!! Form::label('DocumentLink', 'Documentlink:') !!}
    <p>{!! $documents->DocumentLink !!}</p>
    <hr>
</div>

