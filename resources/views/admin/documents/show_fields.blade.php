<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $documents->id !!}</p>
    <hr>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $documents->customer_id !!}</p>
    <hr>
</div>

<!-- Property Id Field -->
<div class="form-group">
    {!! Form::label('property_id', 'Property Id:') !!}
    <p>{!! $documents->property_id !!}</p>
    <hr>
</div>

<!-- Document Name Field -->
<div class="form-group">
    {!! Form::label('document_name', 'Document Name:') !!}
    <p>{!! $documents->document_name !!}</p>
    <hr>
</div>

<!-- Document Type Id Field -->
<div class="form-group">
    {!! Form::label('document_type_id', 'Document Type Id:') !!}
    <p>{!! $documents->document_type_id !!}</p>
    <hr>
</div>

<!-- Document Link Field -->
<div class="form-group">
    {!! Form::label('document_link', 'Document Link:') !!}
    <p>{!! $documents->document_link !!}</p>
    <hr>
</div>

