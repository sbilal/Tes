<!-- Customer Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_id', 'Property Id:') !!}
    {!! Form::text('property_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Document Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document_name', 'Document Name:') !!}
    {!! Form::text('document_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Document Type Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document_type_id', 'Document Type Id:') !!}
    {!! Form::text('document_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Document Link Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document_link', 'Document Link:') !!}
    {!! Form::text('document_link', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.documents.index') !!}" class="btn btn-default">Cancel</a>
</div>
