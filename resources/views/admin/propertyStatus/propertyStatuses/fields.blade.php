<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Status', 'Status:') !!}
    {!! Form::select('Status', ['active' => 'active', 'inactive' => 'inactive'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.propertyStatus.propertyStatuses.index') !!}" class="btn btn-default">Cancel</a>
</div>
