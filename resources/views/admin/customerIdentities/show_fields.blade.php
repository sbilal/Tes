<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $customerIdentity->id !!}</p>
    <hr>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $customerIdentity->customer_id !!}</p>
    <hr>
</div>

<!-- Id Type Field -->
<div class="form-group">
    {!! Form::label('id_type', 'Id Type:') !!}
    <p>{!! $customerIdentity->id_type !!}</p>
    <hr>
</div>

<!-- Id Number Field -->
<div class="form-group">
    {!! Form::label('id_number', 'Id Number:') !!}
    <p>{!! $customerIdentity->id_number !!}</p>
    <hr>
</div>

<!-- Issuing Authority Field -->
<div class="form-group">
    {!! Form::label('issuing_authority', 'Issuing Authority:') !!}
    <p>{!! $customerIdentity->issuing_authority !!}</p>
    <hr>
</div>

<!-- Place Of Issue Field -->
<div class="form-group">
    {!! Form::label('place_of_issue', 'Place Of Issue:') !!}
    <p>{!! $customerIdentity->place_of_issue !!}</p>
    <hr>
</div>

<!-- Date Of Issue Field -->
<div class="form-group">
    {!! Form::label('date_of_issue', 'Date Of Issue:') !!}
    <p>{!! $customerIdentity->date_of_issue !!}</p>
    <hr>
</div>

<!-- Expiry Date Field -->
<div class="form-group">
    {!! Form::label('expiry_date', 'Expiry Date:') !!}
    <p>{!! $customerIdentity->expiry_date !!}</p>
    <hr>
</div>

<!-- Issuing Country Field -->
<div class="form-group">
    {!! Form::label('issuing_country', 'Issuing Country:') !!}
    <p>{!! $customerIdentity->issuing_country !!}</p>
    <hr>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('points', 'Points:') !!}
    <p>{!! $customerIdentity->points !!}</p>
    <hr>
</div>

