<!-- Customer Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Type Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_type', 'Id Type:') !!}
    {!! Form::select('id_type', ['DriverLicense' => 'DriverLicense', 'Passport' => 'Passport', 'Others' => 'Others'], null, ['class' => 'form-control']) !!}
</div>

<!-- Id Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_number', 'Id Number:') !!}
    {!! Form::text('id_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Issuing Authority Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issuing_authority', 'Issuing Authority:') !!}
    {!! Form::text('issuing_authority', null, ['class' => 'form-control']) !!}
</div>

<!-- Place Of Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('place_of_issue', 'Place Of Issue:') !!}
    {!! Form::text('place_of_issue', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Of Issue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('date_of_issue', 'Date Of Issue:') !!}
    {!! Form::date('date_of_issue', null, ['class' => 'form-control']) !!}
</div>

<!-- Expiry Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('expiry_date', 'Expiry Date:') !!}
    {!! Form::date('expiry_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Issuing Country Field -->
<div class="form-group col-sm-12">
    {!! Form::label('issuing_country', 'Issuing Country:') !!}
    {!! Form::text('issuing_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Points Field -->
<div class="form-group col-sm-12">
    {!! Form::label('points', 'Points:') !!}
    {!! Form::text('points', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.customerIdentities.index') !!}" class="btn btn-default">Cancel</a>
</div>
