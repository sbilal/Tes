@extends('admin/layouts/default')

@section('title')
Transaction
@parent
@stop

@section('content')
<section class="content-header">
    <h1>Workflow</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Workflow
            </a>
        </li>
        <li>List</li>
        <li class="active">Workflow View</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
       <div class="panel panel-primary">
        <div class="panel-heading clearfix">
            <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Workflow details
            </h4>
        </div>
            <div class="panel-body">
                Start Workflow <button class="btn-small btn-success" id="start_workflow">start</button>
                <div class="row">
                    Tasks for Teller
                    <table class="table table-striped table-bordered" id="teller_tasks" width="100%">
                        <thead>
                            <td>Task Id</td>
                            <td>Instance Id</td>
                            <td>Task Name</td>
                            <td>Task Status</td>
                            <td>Assigned To</td>
                        </thead>
                        <tbody>
                        @if (isset($tellerTasksToClaim)) 
                        @foreach ($tellerTasksToClaim->{'task-summary'} as $taskToClaim)
                        <tr>
                            <td>
                                {{ $taskToClaim->{'task-id'} }}
                            </td>
                            <td>
                                {{ $taskToClaim->{'task-proc-inst-id'} }}
                            </td>
                            <td>
                                {{ $taskToClaim->{'task-name'} }}
                            </td>
                            <td>
                                {{ $taskToClaim->{'task-status'} }}
                            </td>
                        </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>                    
            </div>
        </div>
  </div>
</section>
@stop
@section('footer_scripts')
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>

    <script>
        $(document).ready(function() { 
            
            var tellerDataTable = $('#teller_tasks').DataTable({
                 "ajax": 'workflow/unclaimedTask/Teller', 
                "columns": [
                    { data: "task-id" },
                    { data: "task-proc-inst-id" },
                    { data: "task-name" },
                    { data: "task-status" },
                    { data: "task-actual-owner" }
                ],
                "order": [[ 2, "desc" ]]                
            });
            
            $('#start_workflow').click(function () {
                $.ajax({
                    type: 'GET', 
                    url : "workflow/start/1", 
                    success : function (data) {
                        tellerDataTable.ajax.reload();
                    }
                });            
             });                
        });        
    </script>

@stop