@extends('admin/layouts/default')

@section('title')
Transaction
@parent
@stop


{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/invoice.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
@stop

@section('content')
<section class="content-header">
    <h1>Application Approval</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route(Sentinel::getUser()->getRoles()->pluck('slug')->first() . '.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        @include('flash::message')
        <div class="panel">
            <div class="panel-body table-responsive">
                @include('admin.transactions.transaction-history')
                @include('admin.transactions.summary-partial')                  
                <div class="row">
                    <div id="transaction-summary-partial-div" id="printSection">    
                        <section class="content paddingleft_right15">        
                            <div class="row">    
                                <div class="panel panel-info">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                            Comments 
                                        </h4>

                                    </div>
                                    <div class="panel-body">
                                        <section class="content">
                                            <div  class="row ">
                                                <div class="col-md-14">
                                                    @include('admin.transactions.transaction-comments')
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>

                            <div class="row">    
                                <div class="panel panel-info">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                                            Documents
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <section class="content">
                                            <div  class="row ">
                                                <div class="col-md-14">
                                                    @include('admin.transactions.transaction-documents')
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            @if ($assignedToCurrentUser)
                            <div class="row text-center">    
                                <a class="btn btn-section btn-raised btn-warning btn-sm" data-transaction-id="{{ $transaction->id }}" data-toggle="modal" data-href="#transaction-reject-modal-div" href="#transaction-reject-modal-div" id="role-reject-button">Reject</a>
                                <a class="btn btn-section btn-raised btn-success btn-sm" data-transaction-id="{{ $transaction->id }}" data-toggle="modal" data-href="#transaction-checklist-modal-div" href="#transaction-checklist-modal-div" id="role-checklist-button">Approve</a>
                                </form>
                            </div>
                            @endif
                        </section>
                        @include('admin.transactions.transaction-comments-add');
                        @include('admin.transactions.transaction-documents-add');
                        @include('admin.transactions.transaction-reject');
                        @include('admin.checklists.role-checklist');
                        
                    </div>    
                </div>        
            </div>            
        </div>
    </div>
</section>

@stop
@section('footer_scripts')


<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

<script>

$(document).ready(function() {   
    var historyTable = $("#workflow-history-table").DataTable({
                data:[],
                "fnRowCallback" : function(nRow, aData, iDisplayIndex){
                                $("td:first", nRow).html(iDisplayIndex +1);
                               return nRow;
                            },                
                rowCallback: function (row, data) {},
                filter: false,
                info: false,
                ordering: false,
                retrieve: true, 
                "order": [[ 4, "desc" ]],
                "columns": [
                    { data: "", sDefaultContent: "" },
                    { data: "node-name" },
                    { data: "task-event-user" },
                    { data: "added_at", render: function (data, type, full) {
                                                            return data?data.date:"";
                                                         } , sDefaultContent: ""
                    },
                    { data: "started_at", render: function (data, type, full) {
                                                            return data?data.date:"";
                                                         } , sDefaultContent: ""
                    },
                    { data: "completed_at", render: function (data, type, full) {
                                                            return data?data.date:"";
                                                         } , sDefaultContent: "" 
                    },
                ]
    });
    
    historyTable.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();    

    $("#workflow-history").click(function() {
        if ($("#transaction-history-partial-div .panel-body").css('display') !== 'none') {
            $("#transaction-history-partial-div .panel-body").hide();
            $("#workflow-history i").removeClass("fa-chevron-up");   
            $("#workflow-history i").addClass("fa-chevron-down");          
        } else {
            $("#transaction-history-partial-div .panel-body").show();
            $("#workflow-history i").removeClass("fa-chevron-down");   
            $("#workflow-history i").addClass("fa-chevron-up");    
            $.ajax({
                url: "../dt-application-history/" + {{ $transaction->id }},
                type: "get"
                    }).done(function (result) {
                        historyTable = $("#workflow-history-table").DataTable();
                        historyTable.clear().draw();
                        historyTable.rows.add(result.data).draw();
                }).fail(function (jqXHR, textStatus, errorThrown) {
                // needs to implement if it fails
            });             
        }
    });
    
});
</script>
   
@stop
