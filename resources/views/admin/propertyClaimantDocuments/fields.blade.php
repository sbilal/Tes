<!-- Document Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document_id', 'Document Id:') !!}
    {!! Form::text('document_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_id', 'Property Id:') !!}
    {!! Form::text('property_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::text('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Document Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document', 'Document:') !!}
    {!! Form::text('document', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.propertyClaimantDocuments.index') !!}" class="btn btn-default">Cancel</a>
</div>
