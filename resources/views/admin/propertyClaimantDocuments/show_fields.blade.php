<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $propertyClaimantDocuments->id !!}</p>
    <hr>
</div>

<!-- Document Id Field -->
<div class="form-group">
    {!! Form::label('document_id', 'Document Id:') !!}
    <p>{!! $propertyClaimantDocuments->document_id !!}</p>
    <hr>
</div>

<!-- Property Id Field -->
<div class="form-group">
    {!! Form::label('property_id', 'Property Id:') !!}
    <p>{!! $propertyClaimantDocuments->property_id !!}</p>
    <hr>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $propertyClaimantDocuments->customer_id !!}</p>
    <hr>
</div>

<!-- Document Field -->
<div class="form-group">
    {!! Form::label('document', 'Document:') !!}
    <p>{!! $propertyClaimantDocuments->document !!}</p>
    <hr>
</div>

