<!--start table -->
@if($audit_results)
    <section class="content">
        <div class="row">
            <div class="portlet box primary">
                <div class="portlet-body flip-scroll">
                    <table id="audittabledetails" class="table table-bordered table-striped table-condensed flip-content" >
                        <tbody>
                            @foreach($audit_results as $audit_result)

                                @if($audit_result->event == 'created')

                                    <tr>
                                        <td> 
                                            <i class="fa fa-fw fa-edit"></i>
                                            <b>{{$audit_result->first_name}}  {{$audit_result->last_name}}</b> created values

                                            <b>
                                                <?php echo str_replace("App\\Models\\","","$audit_result->auditable_type"); ?> 
                                                    ID:{{$audit_result->auditable_id}}
                                            </b>  ,
                                            {{ \Carbon\Carbon::parse($audit_result->created_at)->diffForHumans() }}
                                        </td>
                                    </tr>
                                @elseif($audit_result->event == 'updated')
                                    <tr>
                                        <td>
                                            <i class="fa fa-fw fa-edit"></i> 
                                            <b> {{$audit_result->first_name}}  {{$audit_result->last_name}}</b> updated value  from 
                                            <b> 
                                                <?php  
                                                   $old_values = str_replace(array('"', '_', '{', '}','\\'), ' ', $audit_result->old_values);

                                                   $new_values = str_replace(array('"', '_', '{', '}','\\'), ' ', $audit_result->new_values); 

                                                    $old_arrays = explode(",",$old_values);
                                                    $new_arrays = explode(",",$new_values);

                                                    foreach ($old_arrays as $key => $old_array) {
                                                        $x= $key;
                                                        echo ucwords($old_array).' to ';
                                                        foreach ($new_arrays as $key => $new_array) {
                                                            if($x == $key){
                                                                $new_array_values =explode(":", $new_array);
                                                                foreach ($new_array_values as $key => $new_array_value) {
                                                                    if($key != 0){
                                                                         echo ucwords($new_array_value).',';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }  
                                                ?>
                                            </b>
                                            {{ \Carbon\Carbon::parse($audit_result->updated_at)->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach                                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endif
<!--end table -->

