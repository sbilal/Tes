<div class="row">
        <div class="panel-body table-responsive">
            <table class="table table-striped table-bordered" id="table3" width="100%">
                <thead>
                <tr>
                    <th>Parcel Id</th>
                    <th>FileNumber</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Zoning</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customer->properties as $ownership)
                <tr>
                    <td> <a class="property-details"  data-toggle="modal" data-target="#property-details-id"  propertyId="{{$ownership->id}}" tab="property">{{$ownership->property->parcel_id}} 
                    </a></td>
                    <td>{{$ownership->property->legacy_file_number}}</td>
                    <td>{{$ownership->property->address}}</td>
                    <td>{{$ownership->property->status}}</td>
                    <td>{{$ownership->property->zoning}}</td>
                </tr>
         
                @endforeach
                </tbody>
            </table>
        </div>
</div>
@include('admin.properties.property-popup')