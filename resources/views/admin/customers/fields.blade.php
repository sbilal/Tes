<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="livicon" data-name="settings" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
            Personal Details
        </h3>
       
    </div>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('first_name', 'First Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('second_name', 'Second Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('second_name', null, ['class' => 'form-control']) !!}
            </div>

        </div>

         <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('third_name', 'Third Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('third_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('fourth_name', 'Fourth Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('fourth_name', null, ['class' => 'form-control']) !!}
            </div>

        </div>

          <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('date_of_birth', 'Date Of Birth:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::date('date_of_birth', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('place_of_birth', 'Place Of Birth:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                 {!! Form::text('place_of_birth', null, ['class' => 'form-control']) !!}
            </div>

        </div>

         <div class="col-md-12">

             <div class="form-group col-sm-2">
                {!! Form::label('gender', 'Gender:') !!}
                
            </div>
           

            <div class="form-group col-sm-4">
                
                <label class="radio-inline">
                    &nbsp;<input type="radio" class="custom-radio" name="gender" id="optionsRadiosInline1" value="Male" @isset($customer) @if($customer->gender === "Male") checked="checked" @endif @endisset >&nbsp; Male</label>
                <label class="radio-inline">
                    <input type="radio" class="custom-radio" name="gender" id="optionsRadiosInline2" value="Female" @isset($customer) @if($customer->gender === "Female") checked="checked" @endif  @endisset>&nbsp;Female</label>
                
            </div>

           

        </div>




        
    </div>

</div>



<!--contact-->

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="livicon" data-name="settings" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
            Contact Details
        </h3>
       
    </div>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('mobile_1', 'Mobile:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::number('mobile_1', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('mobile_2', 'Alt Mobile:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::number('mobile_2', null, ['class' => 'form-control']) !!}
            </div>

        </div>

         <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('email', 'Email:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            </div>

           

        </div>

        <div class="col-md-12">
            <div class="form-group col-sm-2">
               {!! Form::label('address', 'Known Address:') !!}
    
                
            </div>

            <div class="form-group col-sm-9">
               
                {!! Form::text('address', null, ['class' => 'form-control']) !!}
            </div>

           

        </div>

          <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('district', 'District') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                
                {!! Form::select('district', $districts, null,  ['class' => 'form-control'] ) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('sub_division', 'Sub Division:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                 {!! Form::select('sub_division', $sub_divisions, null,  ['class' => 'form-control'] ) !!}
            </div>

        </div>

         <div class="col-md-12">

             <div class="form-group col-sm-2">
                {!! Form::label('zone', 'City:') !!}
                
            </div>

             <div class="form-group col-sm-3">
               
                 {!! Form::select('zone', $cities, null,  ['class' => 'form-control'] ) !!}
            </div>
           

           

           

        </div>




        
    </div>

</div>
<!--contact end -->

<!--family-->

<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="livicon" data-name="settings" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
            Family Details
        </h3>
       
    </div>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('mother_first_name', 'Mother First Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('mother_first_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-2">
                {!! Form::label('mother_second_name', 'Mother Second Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('mother_second_name', null, ['class' => 'form-control']) !!}
            </div>

        </div>

         <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('mother_third_name', 'Mother First Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('mother_third_name', null, ['class' => 'form-control']) !!}
            </div>

           

        
            <div class="form-group col-sm-2">
               {!! Form::label('mother_fourth_name', 'Mother Fourth Name:') !!}
    
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('mother_fourth_name', null, ['class' => 'form-control']) !!}
            </div>

           

        </div>


         <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('father_first_name', 'Father First Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('father_first_name', null, ['class' => 'form-control']) !!}
            </div>

           

        
            <div class="form-group col-sm-2">
               {!! Form::label('father_second_name', 'Father Second Name:') !!}
    
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('father_second_name', null, ['class' => 'form-control']) !!}
            </div>

           

        </div>
         <div class="col-md-12">
            <div class="form-group col-sm-2">
                {!! Form::label('father_third_name', 'Father Third Name:') !!}
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('father_third_name', null, ['class' => 'form-control']) !!}
            </div>

           

        
            <div class="form-group col-sm-2">
               {!! Form::label('father_fourth_name', 'Father Fourth Name:') !!}
    
                
            </div>

            <div class="form-group col-sm-3">
               
                {!! Form::text('father_fourth_name', null, ['class' => 'form-control']) !!}
            </div>

           

        </div>

         
        



        
    </div>

</div>
<!--family end-->
<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.customers.index') !!}" class="btn btn-default">Cancel</a>
</div>