<table class="table table-responsive table-striped table-bordered" id="customers-table" width="100%">
    <thead>
     <tr>
        <th>Customer Name</th>
        <th>Mobile Number</th>
        <th>Gender</th>
        <th>Date Of Birth</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>
             
                <a href="{{ route('admin.customers.show', collect($customer)->first() ) }}" >{!! $customer->full_name !!}
                </a>

            </td>                
            <td>{!! $customer->mobile_1 !!}, {!! $customer->mobile_2 !!}</td>
            <td>{!! $customer->gender !!}</td>
            <td>{{  Carbon\Carbon::parse($customer->date_of_birth)->format('d M Y')  }}</td>
            
           
            <td>
                 <a href="{{ route('admin.customers.show', collect($customer)->first() ) }}" >
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view customer"></i>
                 </a>
                 <a href="{{ route('admin.customers.edit', collect($customer)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit customer"></i>
                 </a>
                 @if(Sentinel::inRole('admin'))
                 <a href="{{ route('admin.customers.confirm-delete', collect($customer)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete customer"></i>
                 </a>
                 @endif
            </td>
        </tr>
    @endforeach
    </tbody>

  
</table>
@include('admin.customers.customer-popup')
@include('admin.properties.property-popup')
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#customers-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#customers-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );


       </script>

@stop