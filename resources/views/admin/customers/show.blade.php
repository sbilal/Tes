@extends('admin/layouts/default')

@section('title')
customer
@parent
@stop

@section('content')
<section class="content-header"  >

    <h1>{{ $customer->full_name }} <a href="{!! route('admin.customers.index') !!}" class="btn btn-xs btn-default"><span class="fa fa-chevron-left"></span> Back </a></h1>

    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li> <a href="{!! route('admin.customers.index') !!}"> Customer </a></li>
        <li class="active">{{ $customer->full_name }}</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
       @if (Session::has('message'))
            <div class="alert <?php echo Session::get('alert-class'); ?>">
                <span class="login_error"><small><?php echo Session::get('message')?></small></span>
            </div>
        @endif
        
        @if(Sentinel::inRole('admin'))
            <a class="btn btn-raised btn-success btn-sm pull-right" id="customer-erp-sync">Sync Customer</a> 
            <a class="btn btn-raised btn-primary btn-sm pull-right" href="{{ route('admin.customers.edit', $customer->id) }}">Edit</a>
        @endif
        
        <div class="panel panel-primary" id="hidepanel1">
            <div class="bs-example">
                <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                    <li class="active">
                        <a href="#customer" class="customer-tab" data-toggle="tab">Customer Details</a>
                    </li>
                    <li class="">
                        <a href="#property" class="property-tab" data-toggle="tab">Property Details</a>
                    </li>
                    <li>
                        <a href="#transactions" class="transaction-tab" data-toggle="tab">Transactions</a>
                    </li>
                    <li>
                        <a href="#family" class="family-tab" data-toggle="tab">Family Details</a>
                    </li>
                    <li class="">
                        <a href="#iddetails" data-toggle="tab"  class="identityIds">Identification Details</a>
                    </li>                            
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="customer">
                        @include('admin.customers.customer-main-partial')
                    </div>
                    <div class="tab-pane fade" id="property" >


                        @include('admin.customers.customer-property')
                    </div>
                    <div class="tab-pane fade" id="transactions">
                        @include('admin.customers.customer_transactions')
                    </div>
                    <div class="tab-pane fade" id="family">
                        @include('admin.customers.customer_family')
                    </div>
                    <div class="tab-pane fade" id="iddetails" >
                        @include('admin.customers.customer-identity')
                    </div>                            
                </div>
            </div>            
        </div>
    </div>

    <!-- <div id="user-audit-details"></div> -->
  

    {{--<div class="form-group">--}}
           {{--<a href="{!! route('admin.customers.index') !!}" class="btn btn-default">Back</a>--}}
    {{--</div>--}}

</section>
@stop
@section('footer_scripts')

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.colReorder.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>




   
<script type="text/javascript">
    $(document).ready(function(){ $('#audittabledetails').DataTable(); });

</script>
    
<script type="text/javascript">
    $(document).ready(function () {

        
        $("#customer-erp-sync").click(function () {
            var url = "customer-erp-sync/" + $("#customer_id").val();
            $.post(url,  null, 
                function (data) {
                    $('#info-message-modal').modal('show');
                    $("#info-message-modal .modalMessage").html("<p>" + data + "</p>");                         
                    return true;
                }).fail(function(){
                    console.log("error");
                });                  
        });

        var audit_id =  $("#customer_id").val();
        var model = "App\\\\Models\\\\Customer";
        auditdetails(audit_id,model);

        $(".customer-tab").click(function(){
       
           var audit_id =  $("#customer_id").val();
           var model = "App\\\\Models\\\\Customer";
           auditdetails(audit_id,model);
          
        });

        $(".property-tab").click(function(){
       
          
          $('#user-audit-details').hide();
          
        });

        $(".family-tab").click(function(){
       
          
          $('#user-audit-details').hide();
          
        });


    });





</script>
    
@stop
