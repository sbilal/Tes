@extends('admin/layouts/default')

@section('title')
Customer Modal Test
@parent
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>properties</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Customers</li>
        <li class="active">properties List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Customers
                </h4>
            </div>
            <br />
            <div class="panel-body table-responsive">
                 <a class="btn btn-raised btn-success btn-sm" data-toggle="modal" data-href="#customer-modal-div" href="#customer-modal-div">Add Customer</a><p></p>               
                 <input hidden="true" id="selected-customers" />
            </div>
            <section id="selected-customers-section">
                <div class="panel panel-primary ">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Selected Customers
                        </h4>
                        <div class="pull-right">
                            <a href="#" data-dismiss="modal" class="glyphicon white-text"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>                 
                    </div>
                    <div class="panel-body table-responsive">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="panel panel-primary">
                                        <table class="table table-striped table-bordered" id="selected-customers-table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>First Name</th>
                                                    <th>Second Name</th>
                                                    <th>Phone</th>
                                                    <th>Gender</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>                
            </section>
            <section id="customer-details">
                
            </section>                      
        </div>
 </div>
</section>

@include('admin.layouts.modal-warning')
@include('admin.customers.modal-search')
@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/customer-modal.js') }}" ></script>
    
    <script type="text/javascript">
        var selectedCustomers = [];
        $(document).ready(function() {
        
        var t = $('#selected-customers-table').DataTable({
            "bFilter" : false,
            "bPaginate" : false,
            "language": {
                 "emptyTable": "No Customers are selected"
               }            
        });
        
        $('#customer-modal-div').on('hidden.bs.modal', function () {
            
            id = $("#customer-id").val();
            inArray = jQuery.inArray(eval(id), selectedCustomers);
            
            if (inArray >= 0) {
                $('#warning-message').modal('show');
                $("#modalMessage").html("<p> Customer already selected </p>");  
            } else {
                $.ajax({
                    type: 'GET', 
                    url : "{{URL('admin/api-get-customer')}}/"  + id, 
                    success : function (data) {
                        t.row.add( [
                            data.id,
                            data.first_name,
                            data.second_name,
                            data.mobile_1,
                            data.gender,
                            "delete"
                        ] ).draw( false );   
                        selectedCustomers.push(data.id);
                        $('#selected-customers').val(JSON.stringify(selectedCustomers)); //store array
                    }
                });  
            }
            
            $.ajax({
                type: 'GET', 
                url : "{{URL('admin/customer-main-partial')}}/"  + id, 
                success : function (data) {
                    $("#customer-details").html(data);
                }
            });            
         }); 
     });
    </script>
@stop

