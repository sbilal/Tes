<div class="row">
    <div class="col-xs-12">
    <a class="btn btn-raised btn-success btn-sm" data-toggle="modal" data-href="#addid" href="#addid">Add ID</a><p></p>

        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
            <table class="table table-bordered table-striped" id="customer_id_table" width="100%">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Number</th>
                    <th>Issue Date</th>
                    <th>Expiry Date</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                @foreach($customer->identities as $identity)

                    <tr>
                        <td>{{$identity->id_type}} <input type="hidden" class="identityId" name="identity_id_{{$identity->id}}" id="identity_id_{{$identity->id}}" value="{{$identity->id}}" /></td>
                        <td>{{$identity->id_number}}</td>
                        <td>{{$identity->date_of_issue}}</td>
                        <td>{{$identity->expiry_date}}</td>
                        <td>{{$identity->issuing_country}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        </div>
    </div>

            <div class="modal fade in" id="addid" tabindex="-1" role="dialog" aria-hidden="false">
                <div class="modal-dialog modal-lg Identification-popup">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title mr-auto">Identification Details</h4>

                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form method="POST" enctype='multipart/form-data' action="{{url('admin/insertIdentificationDetail')}}" accept-charset="UTF-8">
                            <div class="modal-body ">
                                <div class="row">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">                   
                                            <input name="_token" type="hidden" value="">
                                            <!-- Customer Id Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="customer_id">Customer Id:</label>
                                                <input class="form-control" required name="customer_id" type="text" value="{{$customer->id}}" readonly="" id="customer_id">
                                                 {{ csrf_field() }}
                                            </div>
                                            <!-- file field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="id_type">File:</label>
                                                <input type="file" class="form-control" required id="file" name="file"> 
                                            </div>
                                            <!-- file field -->
                                            <!-- Id Type Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="id_type">Id Type:</label>
                                                <select class="form-control" required id="id_type" name="id_type">
                                                    <option value="DriverLicense">DriverLicense</option>
                                                    <option value="Passport">Passport</option>
                                                    <option value="Others">Others</option>
                                                </select>
                                            </div>
                                            <!-- Id Number Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="id_number">Id Number:</label>
                                                <input required class="form-control" name="id_number" type="text" id="id_number">
                                            </div>
                                            <!-- Issuing Authority Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="issuing_authority">Issuing Authority:</label>
                                                <input required class="form-control" name="issuing_authority" type="text" id="issuing_authority">
                                            </div>
                                            <!-- Place Of Issue Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="place_of_issue">Place Of Issue:</label>
                                                <input required class="form-control" name="place_of_issue" type="text" id="place_of_issue">
                                            </div>
                                            <!-- Issuing Country Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="issuing_country">Issuing Country:</label>
                                                <input required class="form-control" name="issuing_country" type="text" id="issuing_country">
                                            </div>
                                            <!-- Date Of Issue Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="date_of_issue">Date Of Issue:</label>
                                                <input required class="form-control" name="date_of_issue" type="date" id="date_of_issue">
                                            </div>
                                            <!-- Expiry Date Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="expiry_date">Expiry Date:</label>
                                                <input required class="form-control" name="expiry_date" type="date" id="expiry_date">
                                            </div>
                                            <!-- Points Field -->
                                            <div class="form-group col-sm-12 col-md-6">
                                                <label for="points">Points:</label>
                                                <input  required class="form-control" name="points" type="text" id="points">
                                            </div>                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                <input type="submit" class="btn btn-primary" value="Save changes" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>


<script type="text/javascript">
$(document).ready(function(){
    $(".identityIds").click(function(){
        var identityId = [];
        var model = "App\\Models\\CustomerIdentity";
        $( ".identityId" ).each(function() {
           
            identityId.push($(this).val());

        });

        var url = "../../customer-audit-details"
        $.ajax({
            type: 'post', 
            url : url, 
            data : {"audit_ids":identityId, "model":model},
            success : function (data) {
              
               $("#user-audit-details").html(data);
               $("#user-audit-details").show();

            }
        }); 

    });
});    
</script>