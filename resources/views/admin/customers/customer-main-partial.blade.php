<div class="panel-heading">
    <h3 class="panel-title">
        <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
        Customer Details
    </h3>
    <span class="pull-right">
        <i class="glyphicon glyphicon-chevron-up clickable"></i>
    </span>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-md-8">
            <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                <table class="table table-bordered table-striped" id="customer">
                    <tbody>
                        <tr>
                            <td>First Name</td>
                            <td>
                                {{ Form::hidden('customer_id', $customer->id, array('id' => 'customer_id')) }}
                                {{ Form::hidden('erp_customer_id', $customer->erp_customer_id, array('id' => 'backend_customer_id')) }}
                                <p class="user_name_max">{{$customer->first_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>Second Name</td>
                            <td>
                                <p class="user_name_max">{{$customer->second_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>Third Name</td>
                            <td>
                                <p class="user_name_max">{{$customer->third_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>Fourth Name</td>
                            <td>
                                <p class="user_name_max">{{$customer->fourth_name}}</p>
                            </td>
                        </tr>  
                        <tr>
                            <td>Phone</td>
                            <td>
                                {{$customer->mobile_1}} / {{$customer->mobile_2}}
                            </td>
                        </tr>                        
                        <tr>
                            <td>E-mail</td>
                            <td>
                                {{$customer->email}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Gender / Sex                                                            
                            </td>
                            <td>
                                {{$customer->gender}}
                            </td>
                        </tr>
                        <tr>
                            <td>Birth Date</td>
                            <td>
                                {{  Carbon\Carbon::parse($customer->date_of_birth)->format('d M Y')  }}
                            </td>
                        </tr>
                        <tr>
                            <td>Place of Birth</td>
                            <td>
                                {{$customer->place_of_birth}}  
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                {{$customer->address}} 
                            </td>
                        </tr>
                        <tr>
                            <td>District /SubDivision</td>
                            <td>
                                {{$customer->district}} , {{$customer->sub_division}} 
                            </td>
                        </tr>
                        <tr>
                            <td>Noor Account Number</td>
                            <td>
                                {{$customer->noor_account_number}}
                            </td>
                        </tr>
                        <tr>
                            <td>Last Updated</td>
                            <td>
                                {{ $customer->created_at->diffForHumans() }}
                            </td>
                        </tr>
                    </tbody></table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="img-file">
                <img src="{{ asset('assets/images/authors/no_avatar.jpg') }}" alt="..." class="img-fluid" style="width: 183px;">
            </div>
        </div>    
    </div>
</div>    