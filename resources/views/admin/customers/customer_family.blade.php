<div class="row">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                        <table class="table table-bordered table-striped" id="customer_family">
                            <tbody>
                                <tr>
                                    <td>Mother First Name</td>
                                    <td class="col-md-8">
                                        <p class="user_name_max">{{$customer->mother_first_name}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mother Second Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->mother_first_name}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mother Third Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->mother_third_name}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mother Fourth Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->mother_fourth_name}}</p>
                                    </td>
                                </tr>  
                                <tr>
                                    <td>Father First Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->father_first_name}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Father Second Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->father_first_name}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Father Third Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->father_third_name}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mother Fourth Name</td>
                                    <td>
                                        <p class="user_name_max">{{$customer->father_fourth_name}}</p>
                                    </td>
                                </tr>                        
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>
        </div>    
    </div>
</div>