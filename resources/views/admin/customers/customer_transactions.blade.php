<div class="row">

    <div class="panel-body table-responsive">
        <table class="table table-striped table-bordered" id="table2" width="100%">
            <thead>
            <tr>

                <th>Transaction Reference Number</th>
                <th>Parcel Id</th>
                <th>Service</th>
                <th>Amount Due</th>
                <th>Amount Paid</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($customer->buyerTransactions as $transaction)
                <tr>
                    <td><a href="{{ route('admin.transactions.show', $transaction->id ) }}">
                        {{$transaction->transaction_reference_number}}
                        </a>
                        <input type="hidden" class="transactionId " name="transaction_id_{{$transaction->id}}" id="transaction_id_{{$transaction->id}}" value="{{$transaction->id}}" /></td>

                    <td > <a data-toggle="modal" class="property-details" data-target="#property-details-ids" propertyId="{{$transaction->property_id}}" tab="transaction" >
                        
                        {{$transaction->property->parcel_id}}
                    </a>
                    </td>
                    <td>{{$transaction->service->name}}</td>
                    <td>{{$transaction->buy_amount}}</td>
                    <td>{{$transaction->sale_amount}}</td>
                    <td>{{$transaction->status}}</td>
                    <td>--</td>



                </tr>
            @endforeach


            </tbody>
        </table>
    </div>

</div>
@include('admin.properties.property-model')
<script type="text/javascript">
    $(document).ready(function(){

        $(".transaction-tab").click(function(){

            var transactionId = [];
            var model = "App\\Models\\Transaction";
            $( ".transactionId" ).each(function() {
               
                transactionId.push($(this).val());
                

            });

            var url = "../../customer-audit-details"
            $.ajax({
                type: 'post', 
                url : url, 
                data : {"audit_ids":transactionId, "model":model},
                success : function (data) {

                   $("#user-audit-details").html(data);
                   $('#user-audit-details').show();
                }
            }); 

        });

    });    
    
</script>



