<div  class="row ">
    <div class="col-md-6">
        <h4>Invoice Details</h4>
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">                                
        <table class="table table-bordered table-striped" >
            <tr>
                <td>Invoice No</td>
                <td>{{  $transaction->erp_invoice_id }}</td>
            </tr>                                   
        </table>
            @include("admin.invoice.invoice-items-table")
        </div>
    </div>
    <div class="col-md-6">
        <h4>Payment Details</h4>
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive"> 
        <table class="table table-bordered table-striped" >
        @if (isset($transaction->erp_payment_id))
            <tr>
                <td>Payment Receipt No </td>
                <td><b>{{ $transaction->erp_payment_id }}</b></td>
            </tr>
            <tr>
                <td>Payment Date </td>
                <td><b>{{ $transaction->payment_date }}</b></td>
            </tr>   
            <tr>
                <td>Payment Method </td>
                <td><b>{{ $transaction->payment_method }}</b></td>
            </tr>   
            <tr>
                <td>Payment Reference No </td>
                <td><b>{{ $transaction->payment_reference_number }}</b></td>
            </tr>                                    
        @endif
        </table>
        </div>
    </div>                            
</div>