<div class="modal fade in" id="transaction-document-div" tabindex="-1" role="dialog" aria-hidden="false" style="width:55%; margin:auto; top:5%; overflow-y: auto">    
        <section class="content" >
        <form method="POST" enctype='multipart/form-data' action="{{url('admin/transaction/' . $transaction->id  . '/document')}}" accept-charset="UTF-8">            
            {{ csrf_field() }}
            <div class="row">
                <div class="">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="tablet" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> 
                                Add Document
                            </h3>
                        </div>
                        <div id="" class="panel-body">
                            <div class="row">
                                <div class="form-body">
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Document Type</label>
                                            <div class="col-sm-8">
                                                <select required name="document_type_id" id="document_type_id" class="form-control">
                                                    <option value="">--Please Select--</option>
                                                    @foreach ($documentTypes as $documentType) 
                                                    <option value="{{ $documentType->id }}">{{ $documentType->Name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>     
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Upload Document</label>
                                            <div class="col-sm-8">
                                                <input type="file" class="form-control" required id="document" name="document" accept="application/pdf"> 
                                            </div>
                                        </div>
                                    </div>                                     
                                </div>
                            </div>
                            <div class="row">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="submit" class="btn btn-primary" value="Save changes" />
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </section>
</div>   


