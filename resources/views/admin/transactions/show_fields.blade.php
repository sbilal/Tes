<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $transaction->id !!}</p>
    <hr>
</div>

<!-- Service Id Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service Id:') !!}
    <p>{!! $transaction->service_id !!}</p>
    <hr>
</div>

<!-- Application Date Field -->
<div class="form-group">
    {!! Form::label('application_date', 'Application Date:') !!}
    <p>{!! $transaction->application_date !!}</p>
    <hr>
</div>

<!-- Buyer Id Field -->
<div class="form-group">
    {!! Form::label('buyer_id', 'Buyer Id:') !!}
    <p>{!! $transaction->buyer_id !!}</p>
    <hr>
</div>

<!-- Seller Id Field -->
<div class="form-group">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    <p>{!! $transaction->seller_id !!}</p>
    <hr>
</div>

<!-- Property Id Field -->
<div class="form-group">
    {!! Form::label('property_id', 'Property Id:') !!}
    <p>{!! $transaction->property_id !!}</p>
    <hr>
</div>

<!-- Sale Date Field -->
<div class="form-group">
    {!! Form::label('settlement_date', 'Settlement Date:') !!}
    <p>{!! $transaction->settlement_date !!}</p>
    <hr>
</div>

<!-- Sale Amount Field -->
<div class="form-group">
    {!! Form::label('settlement_date', 'Settlement Amount:') !!}
    <p>{!! $transaction->settlement_date !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $transaction->status !!}</p>
    <hr>
</div>

