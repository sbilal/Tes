<div class="row">
<div id="transaction-summary-partial-div" id="printSection">    
    <section class="content paddingleft_right15">        
        <div class="row">    
            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Application Details 
                    </h4>
                  
                </div>
                <div class="panel-body">
                    <section class="content">
                        <div  class="row ">
                            <div class="col-md-12">
                                @include('admin.transactions.transaction-partial')
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        
        <div class="row">    
            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Property Details
                    </h4>
                </div>
                <div class="panel-body">
                    <section class="content">
                        <div  class="row ">
                            <div class="col-md-12">
                                @include('admin.properties.main-partial')
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="row">    
            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Customer Details
                    </h4>
                </div>
                <div class="panel-body">
                    <section class="content">
                        @include('admin.transactions.transaction-customer-details');
                    </section>
                </div>
            </div>
        </div>        
        <div class="row">    
            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Sale / Notary Details
                    </h4>
                </div>
                <div class="panel-body">
                    <section class="content">
                        @include('admin.transactions.transaction-sale-details');
                    </section>
                </div>
            </div>
        </div>   
        
        @if (isset($transaction->erp_invoice_id) || isset($transaction->erp_payment_id))
        <div class="row">    
            <div class="panel panel-info">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Payment Details 
                    </h4>
                </div>
                <div class="panel-body">
                    <section class="content">
                        @include('admin.transactions.invoice-payment-partial');
                    </section>
                </div>
            </div>
        </div>  
        @endif
    </section>
</div>    
</div>