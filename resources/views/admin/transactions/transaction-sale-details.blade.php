<div  class="row ">
    <div class="col-md-6">
        <h4>Sale Details</h4>
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">                                
        <table class="table table-bordered table-striped" >
            <tr>
                <td>Settlement Date</td>
                <td>
                    {{date('d F Y', strtotime($transaction->settlement_date))}}
                </td>
            </tr>
            <tr>
                <td>Settlement Amount</td>
                <td>$ {{  $transaction->settlement_amount }}</td>
            </tr>                                    
        </table>
        </div>
    </div>
    <div class="col-md-6">
        <h4>Notary Details</h4>
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive"> 
        <table class="table table-bordered table-striped" >
        @if (isset($transaction->notary))
            <tr>
                <td>Name</td>
                <td>{{ $transaction->notary->full_name }}</td>
            </tr>
            <tr>
                <td>Registration Details</td>
                <td>
                    Regstration No: {{ $transaction->notary->notary_number }} <br/>
                    Expiry Date:     {{date('d F Y', strtotime($transaction->notary->notary_expiry_date))}}
                </td>
            </tr>                                     <tr>
                <td>Contact Details</td>
                <td>
                    {{ $transaction->notary->contact_number }} <br/>
                    {{ $transaction->notary->email }} <br/>
                </td>
            </tr>                                    
            <tr>
                <td>Address</td>
                <td>
                    {{ $transaction->notary->address }}
                    {{ $transaction->notary->district }}
                </td>
            </tr>                                    
        @endif
        </table>
        </div>
    </div>                            
</div>