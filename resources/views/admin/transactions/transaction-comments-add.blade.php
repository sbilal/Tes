<div class="modal fade in" id="transaction-comment-div" tabindex="-1" role="dialog" aria-hidden="false" style="width:55%; margin:auto; top:5%; overflow-y: auto">    
        <section class="content" >
        <form method="POST" action="{{url('admin/transaction/' . $transaction->id  . '/comments')}}" accept-charset="UTF-8">            
            {{ csrf_field() }}
            <div class="row">
                <div class="">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="tablet" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> 
                                Add Comment
                            </h3>
                        </div>
                        <div id="" class="panel-body">
                            <div class="row">
                                <div class="form-body">
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Comment</label>
                                            <div class="col-sm-8">
                                                <input type="hidden" id="transaction_id" value="{{ $transaction->id }}"/>
                                                <textarea name="comments" id="comments" class="form-control" rows="5" cols="400" required></textarea>
                                            </div>
                                        </div>
                                    </div>                                          
                                </div>
                            </div>
                            <div class="row">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="submit" class="btn btn-primary" value="Save changes" />
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </section>
</div>   


