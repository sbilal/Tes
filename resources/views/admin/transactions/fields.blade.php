<!-- Service Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('service_id', 'Service Id:') !!}
    {!! Form::text('service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Application Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('application_date', 'Application Date:') !!}
    {!! Form::date('application_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Buyer Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('buyer_id', 'Buyer Id:') !!}
    {!! Form::text('buyer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Seller Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    {!! Form::text('seller_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Property Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('property_id', 'Property Id:') !!}
    {!! Form::text('property_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('settlement_date', 'Settlement Date:') !!}
    {!! Form::date('settlement_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Sale Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('settlement_amount', 'Settlement Amount:') !!}
    {!! Form::text('settlement_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.transactions.index') !!}" class="btn btn-default">Cancel</a>
</div>
