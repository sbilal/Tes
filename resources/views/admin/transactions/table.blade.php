<table class="table table-responsive table-striped table-bordered" id="transactions-table" width="100%">
    <thead>
     <tr>
        <th>Service Id</th>
        <th>Application Date</th>
        <th>Buyer Id</th>
        <th>Seller Id</th>
        <th>Property Id</th>
        <th>Settlement Date</th>
        <th>Settlement Amount</th>
        <th>Status</th>
        <th>Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($transactions as $transaction)
        {{ Form::hidden('transaction_id', $transaction->id, array('id' => 'transaction_id')) }}
        <tr>
            <td>{!! $transaction->service_id !!}</td>
            <td>{!! $transaction->application_date !!}</td>
            <td>
                
                 <a  data-toggle="modal" data-target="#customer-details-id"  customerId="{{$transaction->buyer_id}}"  class="customer-details"  >{!! $transaction->buyer_id !!}
                </a>
            </td>
            <td>
                
                <a  data-toggle="modal" data-target="#customer-details-id"  customerid="{{$transaction->seller_id}}"  class="customer-details"  >{!! $transaction->seller_id !!}
                </a>
            </td>
            <td>
                
                 <a class="property-details" data-toggle="modal" data-target="#property-details-id"  propertyId="{{$transaction->property_id}}">{!! $transaction->property_id !!}
                    </a>

            </td>
            <td>{!! $transaction->settlement_date !!}</td>
            <td>{!! $transaction->settlement_amount !!}</td>
            <td>{!! $transaction->status !!}</td>
            <td>
                <a class="btn btn-raised btn-success btn-sm payment-modal-button" data-toggle="modal" data-transaction-id="{{ $transaction->id }}" data-href="#payment-modal-div" href="#payment-modal-div" id="payment-modal-button">Pay</a><p></p>
                 <a href="{{ route('admin.transactions.show', collect($transaction)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view transaction"></i>
                 </a>
                 <a href="{{ route('admin.transactions.edit', collect($transaction)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit transaction"></i>
                 </a>
                 <a href="{{ route('admin.transactions.confirm-delete', collect($transaction)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete transaction"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('admin.properties.property-popup')
@include('admin.customers.customer-popup')
@include('admin.invoice.payment-modal')

@section('footer_scripts')

<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {                   
            $("#payment_date").datetimepicker().parent().css("position :relative");            
            $('#payment-modal-div').on('show.bs.modal', function(e) {
                var transactionId = $(e.relatedTarget).data('transaction-id');
                $("#transaction_id").val(transactionId);
                $("#invoice_amount").val("22.00");
                $("#payment_amount").val("22.00");
            });         
            

            $('#payment-modal-div').on('hidden.bs.modal', function() {
                //$(':input', this).val('');
                //$(':select', this).val('');
            });
            
            $("#payment-form").submit(function(e) {
                var url = "transaction/submit-payment"; // the script where you handle the form input.
                var formData = $("#payment-form").serialize();
                $("#payment-modal-div").modal("hide");                
                $.post(url,  formData, 
                    function (data) {
                        $("#info-message-modal").modal("show");
                        $("#info-message-modal .modalMessage").html("Payment Successful");
                        $("#wizard-error").html("").hide();                
                        return false;
                    }).fail(function(data){
                        $("#warning-message-modal").html("Failed to make payment");
                        $("#warning-message-modal").show();                
                    });                     
                //e.preventDefault(); // avoid to execute the actual submit of the form.
                return false;
            });            
        });        
    </script>
    
    
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>    
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    
 

    <script>
        $('#transactions-table').DataTable({
            responsive: true,
            pageLength: 10
        });
        $('#transactions-table').on( 'page.dt', function () {
           setTimeout(function(){
                 $('.livicon').updateLivicon();
           },500);
        } );

       </script>

@stop