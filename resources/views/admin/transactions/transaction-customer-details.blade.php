<div  class="row ">
    <div class="col-md-6">
        <h4>Seller Details</h4>
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">                                
        <table class="table table-bordered table-striped" >
            <tr>
                <td>Sellers</td>
                <td>
                    @if($property->currentowners)
                        @foreach($property->currentowners as $owner)
                            {{ $owner->customer->full_name }} 
                            @if($owner->primary_owner == 'Yes') [P] @endif <br/>
                        @endforeach
                    @endif                                            
                </td>
            </tr>
            <tr>
                <td>Purchase Date</td>
                <td>
                    @if($property->currentowners) 
                        {{date('d F Y', strtotime($property->currentowners[0]->purchase_date))}}
                    @endif
                </td>
            </tr>     
            <tr>
                <td>Purchase Date</td>
                <td>
                    @if($property->currentowners) 
                        $ {{$property->currentowners[0]->purchase_price }}
                    @endif
                </td>
            </tr>                                     
        </table>
        </div>
    </div>
    <div class="col-md-6">
        <h4>Buyer Details</h4>
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">                                
        <table class="table table-bordered table-striped" >
            <tr>
                <td>Buyers</td>
                <td>
                    @if($propertyOwnershipApplication)
                        @foreach($propertyOwnershipApplication as $buyer)
                            {{ $buyer->customer->full_name }} 
                            @if ($buyer->customer->id == $transaction->buyer_id) [P] @endif <br/>
                        @endforeach
                    @endif                                            
                </td>
            </tr>   
        </table>
        </div>
    </div>                            
</div>