<link href="{{ asset('assets/css/pages/wall_transaction.css') }}" rel="stylesheet" type="text/css"/>

      <div class="container-fluid dashbord1_container">      
        <div class="row-fluid">
             <div class="metro-nav">
                @php($index=0)
                
            @foreach($transactions as $transaction)
                @php($index++)
                
                @if($index%3+1==1)
                    @php($icon="fa-money")
                        <div class="metro-nav-block nav-block-orange double">
                @elseif($index%4==1)
                    @php($icon="fa-cloud-upload")
                        <div class="metro-nav-block nav-block-green">
               
                @elseif($index%4+1==1)
                    @php($icon="fa-th-large")
                        <div class="metro-nav-block nav-block-grey">
                @elseif($index%8+1==1)
                    @php($icon="fa-globe")                    
                        <div class="metro-nav-block nav-block-red double">
                @elseif($index%6==1)
                    @php($icon="fa-list")
                        <div class="metro-nav-block nav-block-yellow">
                @else
                    @php($icon="fa-home")
                        <div class="metro-nav-block nav-block-purple">
                @endif
                      <a data-original-title="" href="{{ route('admin.transactions.show', collect($transaction)->first() ) }}">
                        <i class="fa {{$icon}}"></i>
                        <div class="info"></div>
                        <div class="status">{!! $transaction->settlement_date !!}</div>
                      </a>
              
                    </div> <!-- ends here -->
            @endforeach
        </div><!-- metro-nav -->
        </div><!-- row-fluid -->
      </div><!-- container-fluid --> 
      


@section('footer_scripts')


   
    
    
    
    
   

    
 


@stop