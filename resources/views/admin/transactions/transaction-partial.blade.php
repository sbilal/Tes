<div  class="row ">
    <div class="col-md-6">
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">                                
        <table class="table table-bordered table-striped" >
            <tr>
                <td>Application Date</td>
                <td>
                    {{date('d F Y', strtotime($transaction->application_date))}}
                </td>
            </tr>
            <tr>
                <td>Application Reference Number</td>
                <td><input type="hidden" id="transaction_id" value="{{ $transaction->id }}"/> {{  $transaction->transaction_reference_number }}</td>
            </tr>                                    
        </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">                                
            <table class="table table-bordered table-striped" >
                <tr>
                    <td>Application Status</td>
                    <td>{{  $transaction->status }}</td>
                </tr>
                <tr>
                    <td>Asssigned To</td>
                    <td>@if (isset($currentAssignedUser)) {{  $currentAssignedUser }} @endif</td>
                </tr>                                 
            </table>
        </div>                            
</div>