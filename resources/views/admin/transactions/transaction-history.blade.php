<div class="row">
    <div id="transaction-history-partial-div" id="printSection">    
        <section class="content paddingleft_right15">        
            <div class="row">    
                <div class="panel panel-info">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Workflow History 
                        </h4>
                        <span class="pull-right ">
                            <a href='#' id="workflow-history" style='color:#fff'><i class="fa fa-chevron-down"></i></a>
                        </span>
                    </div>
                    <div class="panel-body" style="display: none">
                        <section class="content">
                            <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive"> 
                                <table class="table table-bordered table-striped" width="100%" id="workflow-history-table" >
                                    <thead><tr>                                                
                                            <th width="5%"></th>
                                            <th>Workflow Step</th>
                                            <th>User</th>
                                            <th width="15%">Assigned Date</th>
                                            <th width="15%">Start Date</th>
                                            <th width="15%">Completed Date</th>
                                        </tr>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>                            
                </div>
        </section>
    </div>
</div>
