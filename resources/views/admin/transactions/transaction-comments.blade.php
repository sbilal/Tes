<div  class="row ">
    <div class="col-md-12">
        @if ($assignedToCurrentUser)
        <div class="row">            
            <span class="pull-right">
                <a class="btn btn-section btn-raised btn-success btn-sm payment-modal-button" data-transaction-id="{{ $transaction->id }}" data-toggle="modal" data-href="#transaction-comment-div" href="#transaction-comment-div" id="transaction-coomment-add-button">add</a><p></p>
            </span>
        </div>  
        @endif
        <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive"> 
        <table class="table table-bordered table-striped" width="100%" id="transaction-comments-table" >
            <thead><tr>
                    <th width="15%">Date</th>
                    <th>Comments</th>
                    <th width="15%">Updated By</th>
                    <th width="2%">Action</th>
                </tr>
            @if (isset($transactionComments))
            <tbody>
                @foreach($transactionComments as $comments)
                <tr>
                    <td>{{ Carbon\Carbon::parse($comments->created_at)->format('d M Y  h:i a') }}</td>
                    <td>{{ $comments->comments }} </td>
                    <td>{{ $comments->updatedBy->full_name }} </td>
                    <td>
                        <a href="#" data-action="{{ route("admin.transaction.comments.delete", $comments->id) }}" data-toggle="modal" data-target="#delete-confirmation-modal">
                            <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete"></i>
                        </a>
                    </td>                    
                </tr>
                @endforeach
            </tbody>
            @endif
        </table>
        </div>
    </div>                            
</div>