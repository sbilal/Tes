@extends('admin/layouts/default')

@section('title')
Transaction
@parent
@stop


{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/css/pages/invoice.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/pages/wall_transaction.css') }}" rel="stylesheet" type="text/css"/>
@stop

@section('content')
<section class="content-header">
    <h1>Transaction View</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>Transactions</li>
        <li class="active">View Transaction</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="panel panel-primary" id="hidepanel1">
    <div class="row">
            <div class="bs-example">
                <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                    <li class="active">
                        <a href="#transaction" data-toggle="tab">Transaction Details</a>
                    </li>
                    <li class="">
                        <a href="#comments" data-toggle="tab">Comments</a>
                    </li>
                    <li>
                        <a href="#documents" data-toggle="tab">Documents</a>
                    </li>
                    <li>
                        <a href="#workflow" data-toggle="tab">Workflow</a>
                    </li>                         
                </ul>
                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="transaction">
                        <div class="row">
                            <div class="pull-right" style="padding-right: 15px">
                                    {!! Form::model($transaction, ['route' => ['workflow.initiate', $transaction->id ], 'method' => 'post']) !!}
                                            @if (!isset($transaction->workflow_instance_id))
                                                {!! Form::submit('Submit', ['class' => 'btn btn-sm btn-responsive button-alignment btn-success']) !!}
                                            @endif                
                                        <a onclick="javascript:window.print();" class="btn btn-sm btn-responsive button-alignment btn-success">Print</a>         
                                    {!! Form::close() !!}
                            </div>
                        </div>                        
                        @include('admin.transactions.summary-partial') 
                    </div>
                    <div class="tab-pane fade" id="comments">
                    </div>
                    <div class="tab-pane fade" id="documents">
                    </div>
                    <div class="tab-pane fade" id="workflow">
                    </div>

                </div>
            </div>   
        </div>
    </div>             
    <div class="form-group">
           <a href="{!! route('admin.transactions.index') !!}" class="btn btn-default">Back</a>
    </div>
  </div>
</section>
@stop
