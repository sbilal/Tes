<!-- Propertyid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('PropertyId', 'Propertyid:') !!}
    {!! Form::text('PropertyId', null, ['class' => 'form-control']) !!}
</div>

<!-- Ownerid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('OwnerId', 'Ownerid:') !!}
    {!! Form::text('OwnerId', null, ['class' => 'form-control']) !!}
</div>

<!-- Sharepercentage Field -->
<div class="form-group col-sm-12">
    {!! Form::label('SharePercentage', 'Sharepercentage:') !!}
    {!! Form::text('SharePercentage', null, ['class' => 'form-control']) !!}
</div>

<!-- Ownershipdate Field -->
<div class="form-group col-sm-12">
    {!! Form::label('OwnershipDate', 'Ownershipdate:') !!}
    {!! Form::date('OwnershipDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Saledate Field -->
<div class="form-group col-sm-12">
    {!! Form::label('SaleDate', 'Saledate:') !!}
    {!! Form::date('SaleDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Status', 'Status:') !!}
    {!! Form::select('Status', ['active' => 'active', 'inactive' => 'inactive'], null, ['class' => 'form-control']) !!}
</div>

<!-- Saleprice Field -->
<div class="form-group col-sm-12">
    {!! Form::label('SalePrice', 'Saleprice:') !!}
    {!! Form::number('SalePrice', null, ['class' => 'form-control']) !!}
</div>

<!-- Acquiredby Field -->
<div class="form-group col-sm-12">
    {!! Form::label('AcquiredBy', 'Acquiredby:') !!}
    {!! Form::text('AcquiredBy', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.propertyOwnership.propertyOwnerships.index') !!}" class="btn btn-default">Cancel</a>
</div>
