<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $propertyOwnership->id !!}</p>
    <hr>
</div>

<!-- Propertyid Field -->
<div class="form-group">
    {!! Form::label('PropertyId', 'Propertyid:') !!}
    <p>{!! $propertyOwnership->PropertyId !!}</p>
    <hr>
</div>

<!-- Ownerid Field -->
<div class="form-group">
    {!! Form::label('OwnerId', 'Ownerid:') !!}
    <p>{!! $propertyOwnership->OwnerId !!}</p>
    <hr>
</div>

<!-- Sharepercentage Field -->
<div class="form-group">
    {!! Form::label('SharePercentage', 'Sharepercentage:') !!}
    <p>{!! $propertyOwnership->SharePercentage !!}</p>
    <hr>
</div>

<!-- Ownershipdate Field -->
<div class="form-group">
    {!! Form::label('OwnershipDate', 'Ownershipdate:') !!}
    <p>{!! $propertyOwnership->OwnershipDate !!}</p>
    <hr>
</div>

<!-- Saledate Field -->
<div class="form-group">
    {!! Form::label('SaleDate', 'Saledate:') !!}
    <p>{!! $propertyOwnership->SaleDate !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('Status', 'Status:') !!}
    <p>{!! $propertyOwnership->Status !!}</p>
    <hr>
</div>

<!-- Saleprice Field -->
<div class="form-group">
    {!! Form::label('SalePrice', 'Saleprice:') !!}
    <p>{!! $propertyOwnership->SalePrice !!}</p>
    <hr>
</div>

<!-- Acquiredby Field -->
<div class="form-group">
    {!! Form::label('AcquiredBy', 'Acquiredby:') !!}
    <p>{!! $propertyOwnership->AcquiredBy !!}</p>
    <hr>
</div>

