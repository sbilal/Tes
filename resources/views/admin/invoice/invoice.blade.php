<div class="modal fade in" id="full-invoice-modal-div" tabindex="-1" role="dialog" aria-hidden="false" style="width:95%; margin:auto; top:5%; overflow-y: auto">    
            <section class="content" >
                <div class="row">
                    <div class="">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="livicon" data-name="tablet" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> 
                                    Invoice from <bold>GIS</bold>                                
                                </h3>
                            </div>
                            <div id="printSection" class="panel-body" style="border:1px solid #ccc;padding:0;margin:0;">
                                <div class="row" style="padding: 15px;margin-top:5px;">
                                    <div class="col-md-6">
                                        <!--<img src="{{ asset('assets/img/logo2.png') }}" alt="logo" class="img-responsive">-->
                                        Logo
                                    </div>
                                    <div class="col-md-6">
                                        <div class="pull-right">
                                            <strong>Pay To:</strong><br>
                                            <strong>GIS</strong><br>
                                            1234 Market Street<br>
                                            SuperCity, AL 35209<br>
                                            someone@gisportal.com<br>
                                            REGISTRATION NO:254876
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding: 15px;">
                                    <div class="col-md-9 col-xs-6" style="margin-top:5px;">
                                        <strong>Invoice To:</strong><br>
                                        {{ $transaction->buyer->full_name }}<br>
                                        {{ $transaction->buyer->address }}<br>
                                        {{ $transaction->buyer->district }}<br>
                                        {{ $transaction->buyer->mobile_1 }}<br>
                                        {{ $transaction->buyer->email }}<br>
                                    </div>
                                    <div class="col-md-3 col-xs-6" style="padding-right:0">
                                        <div id="invoice-notice" class="bg-primary" style="background-color: #eee;text-align:right;padding: 15px;padding-bottom:23px;border-bottom-left-radius: 6px;border-top-left-radius: 6px;">
                                            <h4><strong>Invoice INV001</strong></h4>
                                            <h4><strong>20 DEC 2015</strong></h4>
                                            Payment Terms: 15days<br>
                                            Payment Due by 01 Jan 2016<br>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding:15px;">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="table-responsive">
                                            @include('admin.invoice.invoice-items')
                                        </div>
                                    </div>
                                </div>
                                <div style="background-color: #eee;padding:15px;" id="footer-bg">
                                    <!--<div class="row">
                                        <div class="col-md-6">
                                            <strong>Payment Details</strong><br>
                                            <strong>Kayin Bank</strong><br>
                                            <strong>Bank/Sort code</strong>: 32-25-85<br>
                                            <strong>Account Number</strong>: 54257963<br>
                                            <strong>Payment Reference</strong>: INV001<br>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pull-right">
                                                <strong>Other Information</strong><br>
                                                <strong>Company Registration Number</strong>:254798621<br>
                                                <strong>Contact/PO</strong>:PO5876452
                                            </div>
                                        </div>
                                    </div>-->
                                    <hr>
                                    <p class="text-center"><strong>Payment should be made by bank transfer or cheque made by payable to GIS</strong></p>
                                    <div style="margin:10px 20px;text-align:center;" class="btn-section">
                                        <button type="button" class="btn btn-responsive btn_marTop button-alignment btn-info"
                                                data-toggle="button">
                                            <a style="color:#fff;" onclick="javascript:window.print();">
                                                <i class="livicon" data-name="printer" data-size="16" data-loop="true"
                                                   data-c="#fff" data-hc="white" style="position:relative;top:3px;"></i>
                                                Print
                                            </a>
                                        </button>
                                        <button type="button" class="btn btn-responsive btn_marTop button-alignment btn-info"
                                                data-toggle="button">
                                            <a style="color:#fff;" onclick='$("#full-invoice-modal-div").hide();'>Close</a>                                                                            
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                <!-- content --> 
</div>   