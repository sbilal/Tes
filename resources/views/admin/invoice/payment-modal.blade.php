<div class="modal fade in" id="payment-modal-div" tabindex="-1" role="dialog" aria-hidden="false" style="width:55%; margin:auto; top:5%; overflow-y: auto">    
    <form action="{{ route('admin.transaction.submit-payment') }}" class="form-horizontal"  autocomplete="off" method="POST" id="payment-form">                                    
        <section class="content" >
            <div class="row">
                <div class="">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="livicon" data-name="tablet" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> Payment</h3>
                        </div>
                        <div id="" class="panel-body">
                            <div class="row">
                                <div class="form-body">
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Invoice Amount</label>
                                            <div class="col-sm-5">
                                                <input type="hidden" id="transaction_id"  name="transaction_id" required/>                            
                                                <input type="text" class="form-control" id="invoice_amount"  name="invoice_amount" readonly="true" required/>                            
                                            </div>
                                        </div>
                                    </div>                                            
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Payment Amount</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" id="payment_amount"  name="payment_amount" readonly="true" required/>                            
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Payment Date</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" id="payment_date"  name="payment_date" required/> 
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Payment Method</label>
                                            <div class="col-sm-5">
                                                <select id="payment_method" name="payment_method" class="form-control select2" required>
                                                    <option value="">Select value...</option>
                                                    <option value="1210 Noor - Geo">Noor Account</option>
                                                    <option value="1110 - Cash - Geo">Cash</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Payment Ref No</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" id="payment_reference" name="payment_reference" required>
                                            </div>
                                        </div>
                                    </div>                                            
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Payee Customer</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="payment_customer" id="payment_customer" required>
                                            </div>
                                        </div>
                                    </div>                                            
                                    <div  class="row">
                                        <div class="form-group">
                                            <label  class="col-sm-3 control-label">Customer Mobile</label>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control" name="payment_customer_mobile" id="payment_customer_mobile" required>
                                            </div>
                                        </div>
                                    </div>                                             
                                </div>
                            </div>
                            <div class="row">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="submit" type="button" name="submit" class="btn btn-success confirm" />                                                                                                
                                </div>                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </form>    
</div>   


