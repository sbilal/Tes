<section id="invoice-details-section">
    <div class="row" >
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Invoice Details 
                </h4>
            </div>
            <div class="panel-body">
                <section class="content">
                    <div  class="row">
                        <div class="form-group">
                            <a class="btn btn-section btn-raised btn-primary btn-sm invoice-print" id="invoice-print">
                                @if (isset($transaction->erp_payment_id))
                                    Print Receipt
                                @else 
                                    Print Invoice
                                @endif 
                            </a>
                            @if (isset($transaction->erp_payment_id))
                                <div class="row pull-right">
                                    <table class="table table-striped table-bordered" >
                                        <tr>
                                            <td>Payment Receipt No </td>
                                            <td><b>{{ $transaction->erp_payment_id }}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Payment Date </td>
                                            <td><b>{{ $transaction->payment_date }}</b></td>
                                        </tr>   
                                        <tr>
                                            <td>Payment Method </td>
                                            <td><b>{{ $transaction->payment_method }}</b></td>
                                        </tr>    
                                        <tr>
                                            <td>Payment Reference No </td>
                                            <td><b>{{ $transaction->payment_reference_number }}</b></td>
                                        </tr>                                        
                                    </table>
                                </div>
                            @endif                            
                            @if (!isset($transaction->erp_payment_id))
                                <a class="btn btn-section btn-raised btn-success btn-sm payment-modal-button" data-transaction-id="{{ $transaction->id }}" data-toggle="modal" data-href="#payment-modal-div" href="#payment-modal-div" id="payment-modal-button">Pay</a><p></p>
                            @endif
                                 <div class="row" style="padding:15px;">
                                    <div class="col-md-12 col-xs-12">

                                        <div class="table-responsive">
                                            {{ Form::hidden('d_transaction_id', $transaction->id, array('id' => 'd_transaction_id')) }}
                                            {{ Form::hidden('payment_status', $transaction->payment_status, array('id' => 'payment_status')) }}
                                            {{ Form::hidden('payment_reference_number', $transaction->payment_reference_number, array('id' => 'payment_reference_number')) }}
                                            @include('admin.invoice.invoice-items-table')
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>                
                </section>                            
            </div>
        </div>
    </div>
</section>
