<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $invocie->id !!}</p>
    <hr>
</div>

<!-- Transactionid Field -->
<div class="form-group">
    {!! Form::label('TransactionId', 'Transactionid:') !!}
    <p>{!! $invocie->TransactionId !!}</p>
    <hr>
</div>

<!-- Customerid Field -->
<div class="form-group">
    {!! Form::label('CustomerId', 'Customerid:') !!}
    <p>{!! $invocie->CustomerId !!}</p>
    <hr>
</div>

<!-- Invoicedate Field -->
<div class="form-group">
    {!! Form::label('InvoiceDate', 'Invoicedate:') !!}
    <p>{!! $invocie->InvoiceDate !!}</p>
    <hr>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('Status', 'Status:') !!}
    <p>{!! $invocie->Status !!}</p>
    <hr>
</div>

