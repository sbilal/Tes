<!-- Transactionid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('TransactionId', 'Transactionid:') !!}
    {!! Form::text('TransactionId', null, ['class' => 'form-control']) !!}
</div>

<!-- Customerid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('CustomerId', 'Customerid:') !!}
    {!! Form::text('CustomerId', null, ['class' => 'form-control']) !!}
</div>

<!-- Invoicedate Field -->
<div class="form-group col-sm-12">
    {!! Form::label('InvoiceDate', 'Invoicedate:') !!}
    {!! Form::date('InvoiceDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Status', 'Status:') !!}
    {!! Form::select('Status', ['active' => 'active', 'inactive' => 'inactive'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.invocie.invocies.index') !!}" class="btn btn-default">Cancel</a>
</div>
