<table class="table table-striped table-bordered" >
    <thead>
    @php ($taxrate = 0)
    <tr>
        <th>No.</th>
        <th>Fee Item</th>
        <th align="right">Fee($)</th>
        <th align="right">Tax ({{$taxrate}}%)</th>
        <th align="right">NetSubtotal($)</th>
    </tr>
    </thead>
    <tbody>
    @php ($index = 1)
    @php ($nettotal = 0)
    @php ($taxtotal = 0)
    @php ($grandtotal = 0)
    @foreach($serviceFees as $serviceFee) 
    <tr>
        @php ($itemrate = $serviceFee->standard_rate)
        @php ($displayrate = number_format($serviceFee->standard_rate, 2))
        <?php if (strpos($serviceFee->item_code, 'LEVY') !== false) {
           $itemrate  = $transaction->settlement_amount * 0.025;
           $displayrate = number_format($itemrate, 2)  . " [2.5% of $" . $transaction->settlement_amount . "] ";
        } ?>
        @php ($tax = $taxrate * $serviceFee->standard_rate)                                                    
        @php ($subtotal = $tax + $itemrate)                                                   
        @php ($taxtotal += $tax)                                                  
        @php ($nettotal += $subtotal)                                                    
        <td>{{ $index++ }}    </td>
        <td>{{ $serviceFee->description }}</td>
        <td align="right">${{ $displayrate }} </td>
        <td align="right">${{ number_format($tax, 2) }} ({{$taxrate}}%) </td>
        <td align="right">${{ number_format($subtotal,2) }} </td>                                                    
    </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td align="right">Net Total</td>
        <td align="right">${{ number_format($nettotal,2) }}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td align="right">Tax</td>
        <td align="right">${{ number_format($taxtotal,2) }}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td align="right"><strong>TOTAL</strong></td>
        <td align="right">${{ number_format($nettotal + $taxtotal, 2) }} <input type="hidden" name="d_invoice_amount" id="d_invoice_amount" value="{{ number_format($nettotal + $taxtotal, 2) }} "></td>
    </tr>
    </tbody>
</table>