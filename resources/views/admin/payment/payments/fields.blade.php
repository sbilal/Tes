<!-- Invoiceid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('InvoiceId', 'Invoiceid:') !!}
    {!! Form::text('InvoiceId', null, ['class' => 'form-control']) !!}
</div>

<!-- Paidamount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('PaidAmount', 'Paidamount:') !!}
    {!! Form::number('PaidAmount', null, ['class' => 'form-control']) !!}
</div>

<!-- Paymentdate Field -->
<div class="form-group col-sm-12">
    {!! Form::label('PaymentDate', 'Paymentdate:') !!}
    {!! Form::date('PaymentDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Paymentmethod Field -->
<div class="form-group col-sm-12">
    {!! Form::label('PaymentMethod', 'Paymentmethod:') !!}
    {!! Form::text('PaymentMethod', null, ['class' => 'form-control']) !!}
</div>

<!-- Referencenumber Field -->
<div class="form-group col-sm-12">
    {!! Form::label('ReferenceNumber', 'Referencenumber:') !!}
    {!! Form::text('ReferenceNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.payment.payments.index') !!}" class="btn btn-default">Cancel</a>
</div>
