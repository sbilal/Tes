<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $payment->id !!}</p>
    <hr>
</div>

<!-- Invoiceid Field -->
<div class="form-group">
    {!! Form::label('InvoiceId', 'Invoiceid:') !!}
    <p>{!! $payment->InvoiceId !!}</p>
    <hr>
</div>

<!-- Paidamount Field -->
<div class="form-group">
    {!! Form::label('PaidAmount', 'Paidamount:') !!}
    <p>{!! $payment->PaidAmount !!}</p>
    <hr>
</div>

<!-- Paymentdate Field -->
<div class="form-group">
    {!! Form::label('PaymentDate', 'Paymentdate:') !!}
    <p>{!! $payment->PaymentDate !!}</p>
    <hr>
</div>

<!-- Paymentmethod Field -->
<div class="form-group">
    {!! Form::label('PaymentMethod', 'Paymentmethod:') !!}
    <p>{!! $payment->PaymentMethod !!}</p>
    <hr>
</div>

<!-- Referencenumber Field -->
<div class="form-group">
    {!! Form::label('ReferenceNumber', 'Referencenumber:') !!}
    <p>{!! $payment->ReferenceNumber !!}</p>
    <hr>
</div>

