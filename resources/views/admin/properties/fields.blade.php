<!-- Parcelid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('ParcelId', 'Parcelid:') !!}
    {!! Form::text('parcel_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Migrationstatus Field -->
<div class="form-group col-sm-12">
    {!! Form::label('MigrationStatus', 'Migrationstatus:') !!}
    {!! Form::text('migration_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Area Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Area', 'Area:') !!}
    {!! Form::text('area', null, ['class' => 'form-control']) !!}
</div>

<!-- North Field -->
<div class="form-group col-sm-12">
    {!! Form::label('North', 'North:') !!}
    {!! Form::text('north', null, ['class' => 'form-control']) !!}
</div>

<!-- South Field -->
<div class="form-group col-sm-12">
    {!! Form::label('South', 'South:') !!}
    {!! Form::text('south', null, ['class' => 'form-control']) !!}
</div>

<!-- East Field -->
<div class="form-group col-sm-12">
    {!! Form::label('East', 'East:') !!}
    {!! Form::text('east', null, ['class' => 'form-control']) !!}
</div>

<!-- West Field -->
<div class="form-group col-sm-12">
    {!! Form::label('West', 'West:') !!}
    {!! Form::text('west', null, ['class' => 'form-control']) !!}
</div>

<!-- Barcode Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Barcode', 'Barcode:') !!}
    {!! Form::text('barcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Deednumber Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DeedNumber', 'Deednumber:') !!}
    {!! Form::text('deed_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Oldfilenumber Field -->
<div class="form-group col-sm-12">
    {!! Form::label('legacy_file_number', 'Legacy File Number:') !!}
    {!! Form::text('legacy_file_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Streetnumber Field -->
<div class="form-group col-sm-12">
    {!! Form::label('street_number', 'Streetnumber:') !!}
    {!! Form::text('street_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Streetname Field -->
<div class="form-group col-sm-12">
    {!! Form::label('street_name', 'Streetname:') !!}
    {!! Form::text('street_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- District Field -->
<div class="form-group col-sm-12">
    {!! Form::label('district', 'District:') !!}
    {!! Form::text('district', null, ['class' => 'form-control']) !!}
</div>

<!-- Subdivision Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sub_division', 'Sub Division:') !!}
    {!! Form::text('sub_division', null, ['class' => 'form-control']) !!}
</div>
<!-- Zoning Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Zone', 'Zone:') !!}
    {!! Form::text('zone', null, ['class' => 'form-control']) !!}
</div>

<!-- Purposeofuse Field -->
<div class="form-group col-sm-12">
    {!! Form::label('purpose_of_use', 'Purpose of Use:') !!}
    {!! Form::text('purpose_of_use', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.properties.index') !!}" class="btn btn-default">Cancel</a>
</div>
