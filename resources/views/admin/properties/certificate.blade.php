<!DOCTYPE html>
<html>
<head>
<style>
   /* body {
        width:21cm; height:29.7cm;
          
        margin-left: auto;
        margin-right: auto;
    }*/

/*page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  height: 21cm;  
}
@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}*/

</style>
</head>
    <body>
       
        <table style="height:21cm ; width: 29.7cm">
            <tr >
                <td style= " padding-top: 4.9cm; padding-left: 9.7cm; ">
                    {{$property->parcel_id }}
                </td>
            </tr> 

            <tr>
                <td  style=" padding-top: 0.6cm; padding-left: 9.4cm;">
                    12-08-2018
                </td>
            </tr> 

            <tr>
                <td  style=" padding-top: 0.6cm; padding-left: 8.9cm;">
                    {{$property->street_number . ',' . $property->address . ',' . $property->street_name}}
                </td>
            </tr> 

            <tr>
                <td  style=" padding-top: 0.6cm; padding-left: 8.2cm;">
                    Identification
                </td>
            </tr>

            <tr>
                <td  style=" padding-top: 0.6cm; padding-left: 4.8cm;">
                    {{$property->district}}
                </td>
                <td  style=" padding-top: 0.6cm; ">
                    {{$property->sub_division}}
                </td>
            </tr> 

            <tr>
                <td  style=" padding-top: 0.6cm; padding-left: 4.5cm;">
                    {{$property->area}}
                </td>
            </tr>
            <tr>
                <td  style=" padding-top: 0.6cm; padding-left: 4.5cm;">
                    {{$property->purpose_of_use}}
                </td>
            </tr>
            <tr>
                <td  style=" padding-top: 2.5cm; padding-left: 3.7cm; font-weight: bold; font-size: 21px;">
                    @if($property->currentowners)

                        @foreach($property->currentowners as  $key => $owner)
                            
                                {{ $owner->customer->full_name }} 
                            
                        @endforeach
                    @endif
                </td>
            </tr>

            <tr>
                <td  style=" padding-top: 2.6cm; padding-left: 11.6cm;">
                    21*6 M
                </td>
            </tr>

            <tr>
                <td style="padding-top: 12px;" >
                    <span style="padding-left: 133px;">{{$property->north}}</span>
                    <span style="padding-left: 70px;">{{$property->south}}</span>
                    <span style="padding-left: 70px;">{{$property->east}}</span>
                    <span style="padding-left: 44px;">{{$property->west}}</span>
                </td>
            </tr>
        </table>
    </body>
</html>