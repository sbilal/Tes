<table class="table table-responsive table-striped table-bordered" id="properties-table" width="100%">
    <thead>
     <tr>
        <th>Parcel Id</th>
        <th>Legacy File Number</th>
        <th>District</th>
        <th>SubDivision</th>        
        <th>Address</th>
        <th>Status</th>
        <th>Migration Status</th>
        <th>Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($properties as $property)
        <tr>
              <td> <a href="{{ route('admin.properties.show', collect($property)->first() ) }}">

                {!! $property->parcel_id !!}
            </a>
                    
            </td>
            <td>{!! $property->legacy_file_number !!}</td>
            <td>{!! $property->district !!}</td>
            <td>{!! $property->sub_division !!}</td>
            <td>{!! $property->address !!}</td>
            <td>{!! $property->status !!}</td>
            <td>{!! $property->migration_status !!}</td>
            <td>
                 <a href="{{ route('admin.properties.show', collect($property)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view property"></i>
                 </a>
                 <a href="{{ route('admin.properties.edit', collect($property)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit property"></i>
                 </a>
                 <a href="{{ route('admin.properties.confirm-delete', collect($property)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete property"></i>
                 </a>
            </td>
        </tr>

     
       
    @endforeach
    </tbody>
</table>
@include('admin.properties.property-popup')

@section('footer_scripts')



    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#properties-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#properties-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );




       </script>

@stop