@extends('admin/layouts/default')

@section('title')
property
@parent
@stop

@section('content')
<section class="content-header">
    <h1>Property View   <a href="{!! route('admin.properties.index') !!}" class="btn btn-default">Back</a></h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>properties</li>
        <li class="active">property View</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Property Details
                </h4>
            </div>
            <div class="panel-body">
                <section class="content">
                    <div  class="row ">
                        <div class="col-md-12">
                           @include('admin.properties.main-partial')    
                           <div class="form-group col-sm-12 ">
                                <label>Certificate</label>
                            
                                <a href="{!! url('admin/property-cetificate/'.$property->id.'') !!}" class="btn btn-primary">Print</a>
                               
                            </div>
                       </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Ownership History
                </h4>
            </div>
            <div class="panel-body">
                <section class="content">
                    <div  class="row ">
                        <div class="col-md-12">
                            <div class="row ">
                                <div class="panel panel-primary filterable table-tools">
                                    <table class="table table-striped table-bordered" id="inline_edit" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Owners</th>
                                            <th>Status</th>
                                            <th>Purchase Date</th>
                                            <th>Sale Date</th>
                                            <th>Sale Price</th>
                                            <th>Acquired By</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($property->owners))
                                        @foreach($property->owners as $item)
                                        <tr>
                                            <td>{{$item->owner->full_name }}</td>
                                            <td>{{$item->status }}</td>
                                            <td>{{date('d F Y', strtotime($item->purchase_date))}}</td>
                                            <td>{{date('d F Y', strtotime($item->sale_date))}}</td>
                                            <td>{{$item->sale_price}}</td>
                                            <td>{{$item->service->name}}</td>
                                        </tr>
                                        @endforeach
                                        @else
                                            <tr>
                                        <span>Ownership detail are not found</span>
                                            </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>


            </div>
        </div>
        <div class="form-group">

        </div>
    </div>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Lien History
                </h4>
            </div>
            <div class="panel-body">
                <section class="content">
                    <div  class="row ">
                        <div class="col-md-12">
                            <div class="row ">
                                <div class="row ">
                                    <div class="panel panel-primary filterable table-tools">
                                        <table class="table table-striped table-bordered" id="inline_edit" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Bank Name</th>
                                                <th>Owners</th>
                                                <th>Loan Amount</th>
                                                <th>Loan Date</th>
                                                <th>Release Date</th>
                                                <th>State</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(sizeof($property_lien_histories)>0)
                                            @foreach($property_lien_histories as $item)
                                            <tr>
                                                <td>{{$item->getBankInfo->Name}}</td>
                                                <td>{{$item->getOwnershipInfo->owners->full_name}}</td>
                                                <td>${{$item->LoanAmount}}</td>
                                                <td>{{date('d F Y', strtotime($item->LoanDate))}}</td>
                                                <td>{{date('d F Y', strtotime($item->ReleaseDate))}}</td>
                                                <td>{{$item->Status}}</td>
                                            </tr>
                                            @endforeach
                                            @else
                                                <tr>
                                            <span>Lien detail are not found</span>
                                                </tr>
                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>


            </div>
        </div>
        <div class="form-group">

        </div>
    </div>
</section>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyADWjiTRjsycXf3Lo0ahdc7dDxcQb475qw&sensor=false"></script>

<script>

    var geocoder = new google.maps.Geocoder();
    var address = '{{$property->District}}';

    geocoder.geocode( { 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 8,
                center: {lat: latitude, lng: longitude}
            });
            var geocoder = new google.maps.Geocoder();


            geocodeAddress(geocoder, map);

        }


    });

    function geocodeAddress(geocoder, resultsMap) {

        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }



    $(document).ready(function () {
       var audit_id =  $("#auditable_id").val();
       var model = "App\\\\Models\\\\Property";
       auditdetails(audit_id,model);
                           
    });


    </script>
@stop
