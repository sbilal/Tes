@extends('admin/layouts/default')

@section('title')
properties
@parent
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>properties</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li>properties</li>
        <li class="active">properties List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    properties List
                </h4>
                <div class="pull-right">
                    <a href="{{ route('admin.properties.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
            </div>
            <br />
            <div class="panel-body table-responsive">
                 <a class="btn btn-raised btn-success btn-sm" data-toggle="modal" data-href="#property-modal-div" href="#property-modal-div">Add ID</a><p></p>               
                 <input hidden="true" id="property-id" />
            </div>
        </div>
 </div>
</section>

@include('admin.properties.modal-search')
@stop

@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/property-modal.js') }}" ></script>
@stop

