<div class="row panel panel-primary">
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-striped" id="users">
                <tr>
                    <input type="hidden" id="auditable_id" value="{{$property->id}}">
                    <td>Address</td>
                    <td>{{$property->street_number . ',' . $property->address . ',' . $property->street_name}}</td>
                </tr>
                <tr>
                    <td>District</td>
                    <td>{{$property->district}}</td>
                </tr>
                <tr>
                    <td>Subdivision</td>
                    <td>{{$property->sub_division}}</td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>{{$property->district}}</td>
                </tr>
                <tr>
                    <td>Ownership</td>
                    <td>
                        @if($property->currentowners)
                        @foreach($property->currentowners as $owner)
                            {{ $owner->customer->full_name }} 
                            @if($owner->primary_owner == 'Yes') [P] @endif <br/>
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>{{$property->status}}</td>
                </tr>

                
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div id="map" style="height: 300px;" class="gmap"></div>
    </div>
</div>
