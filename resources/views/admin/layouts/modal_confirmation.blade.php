<div class="modal-header" id="delete_confirm">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">@lang($model.'/modal.title')</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
        @lang($model.'/modal.body')
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">@lang($model.'/modal.cancel')</button>
  @if(!$error)
    <a href="{{ $confirm_route }}" type="button" class="btn btn-danger">@lang($model.'/modal.confirm')</a>
  @endif
</div>


<div class="modal fade pullDown" id="confirmation-message-modal" role="dialog" aria-labelledby="modalConfirm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title" id="modalConfirm">Confirm</h4>
            </div>
            <div class="modal-body modalMessage">
                  Are you sure to complete this operation?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" type="button" data-dismiss="modal" class="btn btn-success confirm">Confirm</a>
            </div>
        </div>
    </div>
</div> 
