@php ($role = strtolower(Sentinel::getUser()->getRoles()->pluck('name')->first()))

<li class="{{ route('user.dashboard') }} ? 'active' : '' }}">
    <a href="{{ route('user.dashboard') }}">
        <i class="livicon" data-c="#000000" data-hc="#000000" data-name="wrench" data-size="12"
           data-loop="true"></i>
        Dashboard
    </a>
</li>    
    
@if(Sentinel::inRole('admin') || Sentinel::inRole('teller'))
<li class="{{ Request::is('admin/change-ownership*') ? 'active' : '' }}">
    <a href="{!! route('admin.change-ownership.index') !!}">
        <i class="livicon" data-c="#000000" data-hc="#000000" data-name="wrench" data-size="12"
           data-loop="true"></i>
        Change Ownership
    </a>
</li>
@endif

@if((Sentinel::inRole('admin'))||(Sentinel::inRole('teller')))
<li class="{{ Request::is('admin/customers*') ? 'active' : '' }}">
    <a href="{!! route('admin.customers.index') !!}">
        <i class="livicon" data-c="#F89A14" data-hc="#F89A14" data-name="user" data-size="12"
           data-loop="true"></i>
        Customer
    </a>
</li>
@endif
@if((Sentinel::inRole('admin'))||(Sentinel::inRole('teller')))
<li class="{{ Request::is('admin/properties*') ? 'active' : '' }}">
    <a href="{!! route('admin.properties.index') !!}">
        <i class="livicon" data-c="#000000" data-hc="#000000" data-name="home" data-size="12"
           data-loop="true"></i>
        Property

    </a>
</li>
@endif
@if(Sentinel::inRole('admin'))

<li class="{{ Request::is('admin/transactions*') ? 'active' : '' }}">
    <a href="{!! route('admin.transactions.index') !!}">
        <i class="livicon" data-c="#000000" data-hc="#000000" data-name="thumbnails-big" data-size="12"
           data-loop="true"></i>
        Transactions
    </a>
</li>
@endif
@if(Sentinel::inRole('admin'))

<li class="{{ Request::is('admin/transactions_wall*') ? 'active' : '' }}">
    <a href="{!! route('admin.transactions.wall_transaction') !!}">
        <i class="livicon" data-c="#000000" data-hc="#000000" data-name="gift" data-size="12"
           data-loop="true"></i>
        Transactions Wall
    </a>
</li>

@endif

@if((Sentinel::inRole('admin'))||(Sentinel::inRole('teller')))
<li class="">
    <a href="#">
        <i class="livicon" data-c="#000000" data-hc="#000000" data-name="map" data-size="12"
           data-loop="true"></i>
        My Queue
    </a>
</li>
@endif

@if(Sentinel::inRole('admin'))
<li  {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users')  ||
     Request::is('admin/groups') || Request::is('admin/groups/create') || Request::is('admin/groups/*') ? 'class="active"' : '') !!}>
    <a href="#">
        <i class="livicon" data-name="gear" data-c="#418BCA" data-hc="#418BCA" data-size="12" data-loop="true"></i>
        <span class="title">Security</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
    <li {!! (Request::is('admin/users') || Request::is('admin/users/create') || Request::is('admin/user_profile') || Request::is('admin/users/*') || Request::is('admin/deleted_users') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="user" data-size="12" data-c="#6CC66C" data-hc="#6CC66C"
               data-loop="true"></i>
            <span class="title">Users</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Users
                </a>
            </li>
            <li {!! (Request::is('admin/users/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/users/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New User
                </a>
            </li>
            <li {!! ((Request::is('admin/users/*')) && !(Request::is('admin/users/create')) || Request::is('admin/user_profile') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::route('admin.users.show',Sentinel::getUser()->id) }}">
                    <i class="fa fa-angle-double-right"></i>
                    View Profile
                </a>
            </li>
            <li {!! (Request::is('admin/deleted_users') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/deleted_users') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Deleted Users
                </a>
            </li>
        </ul>
    </li>
    <li {!! (Request::is('admin/groups') || Request::is('admin/groups/create') || Request::is('admin/groups/*') ? 'class="active"' : '') !!}>
        <a href="#">
            <i class="livicon" data-name="users" data-size="12" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Groups</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::is('admin/groups') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/groups') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Group List
                </a>
            </li>
            <li {!! (Request::is('admin/groups/create') ? 'class="active" id="active"' : '') !!}>
                <a href="{{ URL::to('admin/groups/create') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Add New Group
                </a>
            </li>
        </ul>
    </li>

    <li class="{{ Request::is('admin/permission*') ? 'active' : '' }}">
    <a href="{!! route('admin.permission.create') !!}">
        <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="edit" data-size="18"
           data-loop="true"></i>
        Permission
    </a>
    </li>

</ul>
</li>
        

<li class="{{ Request::is('admin/documentTypes*') ? 'active' : '' }} {{ Request::is('admin/feeTypes*') ? 'active' : '' }} {{ Request::is('admin/statuses*') ? 'active' : '' }} {{ Request::is('admin/district/districts*') ? 'active' : '' }} {{ Request::is('admin/subDivisions*') ? 'active' : '' }} {{ Request::is('admin/documents*') ? 'active' : '' }} {{ Request::is('admin/propertylienhistories*') ? 'active' : '' }} {{ Request::is('admin/propertyClaimantDocuments*') ? 'active' : '' }} {{ Request::is('admin/serviceFeeItems*') ? 'active' : '' }} {{ Request::is('admin/zonings*') ? 'active' : '' }} {{ Request::is('admin/propertyStatuses*') ? 'active' : '' }} {{ Request::is('admin/banks*') ? 'active' : '' }} {{ Request::is('admin/documentShelves*') ? 'active' : '' }} {{ Request::is('admin/customerIdentities*') ? 'active' : '' }}  {{ Request::is('admin/checklists*') ? 'active' : '' }} {{ Request::is('admin/propertyOwnerships*') ? 'active' : '' }}">
    <a href="#">
        <i class="livicon" data-name="gear" data-c="#418BCA" data-hc="#418BCA" data-size="12" data-loop="true"></i>
        <span class="title">Settings</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="{{ Request::is('admin/services*') ? 'active' : '' }}">
            <a href="{!! route('admin.services.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="wrench" data-size="12"
                   data-loop="true"></i>
                Services
            </a>
        </li>

        <li class="{{ Request::is('admin/notaries*') ? 'active' : '' }}">
            <a href="{!! route('admin.notaries.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="info" data-size="12"
                   data-loop="true"></i>
                Notaries
            </a>
        </li>
        
        <li class="{{ Request::is('admin/documentTypes*') ? 'active' : '' }}">
            <a href="{!! route('admin.documentTypes.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="edit" data-size="12"
                   data-loop="true"></i>
                Document Types
            </a>
        </li>

        <li class="{{ Request::is('admin/statuses*') ? 'active' : '' }} ">
            <a href="{!! route('admin.statuses.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="gear" data-size="12"
                   data-loop="true"></i>
                Status
            </a>
        </li>
        <li class="{{ Request::is('admin/district*') ? 'active' : '' }}">
            <a href="{!! route('admin.district.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#54C672" data-name="globe" data-size="12"
                   data-loop="true"></i>
                Districts
            </a>
        </li>

        <li class="{{ Request::is('admin/subDivisions*') ? 'active' : '' }}">
            <a href="{!! route('admin.subDivisions.index') !!}">
                <i class="livicon" data-c="#FF9533" data-hc="#FF9533" data-name="thumbnails-big" data-size="12"
                   data-loop="true"></i>
                Sub Divisions
            </a>
        </li>

        <li class="{{ Request::is('admin/documents*') ? 'active' : '' }}">
            <a href="{!! route('admin.documents.index') !!}">
                <i class="livicon" data-c="#1DA1F2" data-hc="#1DA1F2" data-name="edit" data-size="12"
                   data-loop="true"></i>
                Documents
            </a>
        </li>

        <li class="{{ Request::is('admin/propertyOwnerships*') ? 'active' : '' }}">
            <a href="{!! route('admin.propertyOwnerships.index') !!}">
                <i class="livicon" data-c="#F89A14" data-hc="#F89A14" data-name="user" data-size="12"
                   data-loop="true"></i>
                Property Ownerships
            </a>
        </li>

        <li class="{{ Request::is('admin/propertylienhistories*') ? 'active' : '' }}">
            <a href="{!! route('admin.propertylienhistories.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="servers" data-size="12"
                   data-loop="true"></i>
                Propertylien Histories
            </a>
        </li>

        <li class="{{ Request::is('admin/propertyClaimantDocuments*') ? 'active' : '' }}">
            <a href="{!! route('admin.propertyClaimantDocuments.index') !!}">
                <i class="livicon" data-c="#1DA1F2" data-hc="#1DA1F2" data-name="list" data-size="12"
                   data-loop="true"></i>
                Property Claimant Documents
            </a>
        </li>

        <li class="{{ Request::is('admin/zonings*') ? 'active' : '' }}">
            <a href="{!! route('admin.zonings.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="globe" data-size="12"
                   data-loop="true"></i>
                Zonings
            </a>
        </li>

        <li class="{{ Request::is('admin/propertyStatuses*') ? 'active' : '' }}">
            <a href="{!! route('admin.propertyStatuses.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="servers" data-size="12"
                   data-loop="true"></i>
                Property Statuses
            </a>
        </li>

        <li class="{{ Request::is('admin/banks*') ? 'active' : '' }}">
            <a href="{!! route('admin.banks.index') !!}">
                <i class="livicon" data-c="#1DA1F2" data-hc="#1DA1F2" data-name="bank" data-size="12"
                   data-loop="true"></i>
                Banks
            </a>
        </li>

        <li class="{{ Request::is('admin/documentShelves*') ? 'active' : '' }}">
            <a href="{!! route('admin.documentShelves.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="edit" data-size="12"
                   data-loop="true"></i>
                Document Shelves
            </a>
        </li>

        <li class="{{ Request::is('admin/customerIdentities*') ? 'active' : '' }}">
            <a href="{!! route('admin.customerIdentities.index') !!}">
                <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="users" data-size="12"
                   data-loop="true"></i>
                Customer Identities
            </a>
        </li>

        <li class="{{ Request::is('admin/propertyOwnerships*') ? 'active' : '' }}">
            <a href="{!! route('admin.propertyOwnerships.index') !!}">
                <i class="livicon" data-c="#1DA1F2" data-hc="#1DA1F2" data-name="list" data-size="12"
                   data-loop="true"></i>
                Property Ownership History
            </a>
        </li>

        <li class="{{ Request::is('admin/checklists*') ? 'active' : '' }}">
            <a href="{!! route('admin.checklists.index') !!}">
                <i class="livicon" data-c="#000000" data-hc="#000000" data-name="list" data-size="12"
                   data-loop="true"></i>
                Checklists
            </a>
        </li>
    </ul>
</li>


<li class="{{ Request::is('admin/ledgerScannings*') ? 'active' : '' }}">
    <a href="{!! route('admin.ledgerScannings.index') !!}">
        <i class="livicon" data-c="#6CC66C" data-hc="#6CC66C" data-name="edit" data-size="12"
           data-loop="true"></i>
        Ledger Scannings
    </a>
</li>



<li {!! (Request::is('admin/activity_log') ? 'class="active"' : '') !!} {!! (Request::is('admin/log_viewers/logs') ? 'class="active"' : '') !!}>
    <a href="#">
        <i class="livicon" data-name="eye-open" data-size="18" data-c="#418BCA" data-hc="#418BCA"
           data-loop="true"></i>
        <span class="title">Logs</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
       <li {!! (Request::is('admin/activity_log') ? 'class="active"' : '') !!}>
            <a href="{{  URL::to('admin/activity_log') }}">
                <i class="livicon" data-name="eye-open" data-size="18" data-c="#F89A14" data-hc="#F89A14"
                   data-loop="true"></i>
                Activity Log
            </a>
        </li>
        <li {!! (Request::is('admin/log_viewers/logs') ? 'class="active"' : '') !!}>

            <a href="{!! route('admin.log_viewers.logs') !!}">
                <i class="livicon" data-name="eye-open" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                   data-loop="true"></i>
                Api Log
            </a>
        </li>
    </ul>
</li>




@endif


