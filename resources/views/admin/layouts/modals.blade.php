<div class="modal fade pullDown" id="warning-message-modal" role="dialog" aria-labelledby="modalLabelnews">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h4 class="modal-title" id="modalLabelnews">Error</h4>
            </div>
            <div class="modal-body modalMessage">

            </div>
            <div class="modal-footer">
                <button class="btn  btn-warning" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade pullDown" id="info-message-modal" role="dialog" aria-labelledby="modalResult">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h4 class="modal-title" id="modalResult">Result</h4>
            </div>
            <div class="modal-body modalMessage">

            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade pullDown" id="confirmation-message-modal" role="dialog" aria-labelledby="modalConfirm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title" id="modalConfirm">Confirm</h4>
            </div>
            <div class="modal-body modalMessage">
                  Are you sure to complete this operation?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a href="#" type="button" data-dismiss="modal" class="btn btn-success confirm">Confirm</a>
            </div>
        </div>
    </div>
</div> 
