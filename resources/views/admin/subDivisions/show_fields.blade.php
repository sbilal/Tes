<div class="col-md-8">
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="users">

                <tr>
                    <td> {!! Form::label('DistrictName', 'District Name:') !!}</td>
                    <td>
                        {!! $subDivision->name !!}
                    </td>

                </tr>
                <tr>
                    <td> {!! Form::label('SubDivision Name', 'Sub Divisions Name:') !!}</td>
                    <td>
                        {!! $subDivision->Name !!}
                    </td>

                </tr>

            </table>
        </div>
    </div>
</div>
