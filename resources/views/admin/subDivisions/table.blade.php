<table class="table table-responsive table-striped table-bordered" id="subDivisions-table" width="100%">
    <thead>
     <tr>
        <th>District Name</th>
        <th>Sub Division Name</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($subDivisions as $subDivision)
        <tr>
            <td>{!! $subDivision->name !!}</td>
            <td>{!! $subDivision->Name !!}</td>
            <td>
                 <a href="{{ route('admin.subDivisions.show', collect($subDivision)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view subDivision"></i>
                 </a>
                 <a href="{{ route('admin.subDivisions.edit', collect($subDivision)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit subDivision"></i>
                 </a>
                 <a href="{{ route('admin.subDivisions.confirm-delete', collect($subDivision)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete subDivision"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#subDivisions-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#subDivisions-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop