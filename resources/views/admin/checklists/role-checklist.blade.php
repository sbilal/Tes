<div class="modal fade in" id="transaction-checklist-modal-div" tabindex="-1" role="dialog" aria-hidden="false" style="width:60%; margin:auto; top:5%">    
        <div class="panel panel-success ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Checklist
                </h4>
                <div class="pull-right">
                    <a href="#" data-dismiss="modal" class="glyphicon white-text"><i class="glyphicon glyphicon-remove"></i></a>
                </div>                 
            </div>
            <div class="panel-body table-responsive">
                    <div class="modal-body">
                        <form method="POST" action="{{ route('workflow.approve-application', [$transaction->id])}}" >                                                          
                            <div class="row">
                                <p class="m-r-6 ">I agree that the following items are verified and completed</p>
                            </div>                       
                        <div class="row">
                            <div class="panel panel-primary">
                                {{ csrf_field() }}
                                <input hidden="true" id="transaction_id" name='transaction_id' value="{{$transaction->id}}"/>
                                <table class="table table-striped table-bordered" id="customer-modal" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%"></th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rolechecklist as $checklist) 
                                        <tr>
                                            <td><input type="checkbox" required name="check"></td>
                                            <td>{{ $checklist->description }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row text-center">    
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
                            <input type="submit" name="submit" value="Submit" class="btn btn-success btn-sm"/>
                        </div>
                        </form>                        
                    </div>
            </div>
        </div>
</div>    
 
