<table class="table table-responsive table-striped table-bordered" id="checklists-table" width="100%">
    <thead>
     <tr>
        <th width="20%">Role</th>
        <th>Name</th>
        <th>Description</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($checklists as $checklist)
        <tr>
            <td width="20%">{!! $checklist->role->name !!}</td>
            <td>{!! $checklist->name !!}</td>
            <td>{!! $checklist->description !!}</td>
            <td>
                 <a href="{{ route('admin.checklists.show', collect($checklist)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view checklist"></i>
                 </a>
                 <a href="{{ route('admin.checklists.edit', collect($checklist)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit checklist"></i>
                 </a>
                 <a href="{{ route('admin.checklists.confirm-delete', collect($checklist)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete checklist"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#checklists-table').DataTable({
            responsive: true,
            pageLength: 10,
            initComplete: function () {
                this.api().columns().every( function (i) {
                    if (i==0) {
                        var column = this;
                        var select = $('<select class="form-control" style="width:100%"><option value=""></option></select>')
                            .appendTo( $(column.header()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    }
                } );
            }                      
          });
    $('#checklists-table').on( 'page.dt', function () {
       setTimeout(function(){
             $('.livicon').updateLivicon();
       },500);
    } );

       </script>

@stop