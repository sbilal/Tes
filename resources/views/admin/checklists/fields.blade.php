<!-- Role Field -->
<div class="form-group col-sm-12">
    {!! Form::label('role', 'Role:') !!}
    <select class="form-control required" title="Select role..." name="role_id" id="role_id">
        <option value="">Select</option>
        @foreach($roles as $role)
            <option value="{{ $role->id }}" @if(isset($checklist) && $role->id == $checklist->role_id) selected="selected" @endif >{{ $role->name}}</option>
        @endforeach    
    </select>
</div>

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.checklists.index') !!}" class="btn btn-default">Cancel</a>
</div>
