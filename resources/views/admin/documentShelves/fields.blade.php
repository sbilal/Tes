<!-- Document Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('document_id', 'Document Id:') !!}
    {!! Form::text('document_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rack Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('rack_number', 'Rack Number:') !!}
    {!! Form::text('rack_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Shelf Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('shelf_number', 'Shelf Number:') !!}
    {!! Form::text('shelf_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.documentShelves.index') !!}" class="btn btn-default">Cancel</a>
</div>
