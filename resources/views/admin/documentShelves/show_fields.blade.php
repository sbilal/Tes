<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $documentShelf->id !!}</p>
    <hr>
</div>

<!-- Document Id Field -->
<div class="form-group">
    {!! Form::label('document_id', 'Document Id:') !!}
    <p>{!! $documentShelf->document_id !!}</p>
    <hr>
</div>

<!-- Rack Number Field -->
<div class="form-group">
    {!! Form::label('rack_number', 'Rack Number:') !!}
    <p>{!! $documentShelf->rack_number !!}</p>
    <hr>
</div>

<!-- Shelf Number Field -->
<div class="form-group">
    {!! Form::label('shelf_number', 'Shelf Number:') !!}
    <p>{!! $documentShelf->shelf_number !!}</p>
    <hr>
</div>

