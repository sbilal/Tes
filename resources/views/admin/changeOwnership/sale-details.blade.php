<section id="transaction-details-section">
    <div class="row" >
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Sale Detail
                </h4>
            </div>

            <div class="panel-body">
                <section class="content">
                    <div  class="row">
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Settlement date</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control required" id="settlement_date"  name="settlement_date" />                            
                            </div>
                        </div>
                    </div>
                    <br>
                    <div  class="row ">
                        <div class="form-group">
                            <label  class="col-sm-3 control-label">Settlement Amount</label>
                            <div class="col-sm-5">
                                {!! Form::number('settlement_amount', null, array('placeholder' => 'Settlment Amount','class' => 'form-control required','id'=>'settlement_amount')) !!}
                            </div>

                        </div>
                    </div>
                </section>                           
            </div>
        </div>
    </div>

    <div class="row" >
        <div class="panel panel-primary">
            <div class="panel-heading clearfix">
                <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Notary
                </h4>
            </div>

            <div class="panel-body">
                <section class="content">
                    <div  class="row">
                        <div class="form-group">
                            <a class="btn btn-raised btn-success btn-sm" data-toggle="modal" data-href="#notary-modal-div" href="#notary-modal-div">Select Notary</a><p></p>                            
                            <input type="hidden" id='notary_id' name='notary_id'/>
                        </div>
                    </div>                
                </section>   
                @include('admin.changeOwnership.selected-notary-table')           
            </div>
        </div>
    </div>
</section>