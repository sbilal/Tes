@extends('admin/layouts/default')

@section('title')
    Change Ownership
    @parent
@stop
@section('header_styles')
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">--}}
    <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/jquery_steps/css/jquery.steps.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />    
    <link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />    
@stop

@section('content')

    <section class="content-header">
        <h1>Change Ownership   <a href="{!! route('admin.change-ownership.index') !!}" class="btn btn-default">Back</a></h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('admin.dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li class="active">change-ownership</li>
        </ol>
    </section>
    <section class="content paddingleft_right15">
        <div class="row" >

           <div class="panel-body">
        <form id="commentForm" method="post" action="#">
            <div id="rootwizard">
                <ul>
                    <li>
                        <a href="#tab1" data-toggle="tab">Property Details</a>
                    </li>
                    <li>
                        <a href="#tab2" data-toggle="tab">Buyer</a>
                    </li>
                    <li>
                        <a href="#tab3" data-toggle="tab">Sale Detail</a>
                    </li>
                    <li>
                        <a href="#tab4" data-toggle="tab">Verification</a>
                    </li>
                    <li>
                        <a href="#tab5" data-toggle="tab">Payment</a>
                    </li>
                    <li>
                        <a href="#tab6" data-toggle="tab">Confirm</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="tab1">

                    </div>
                    <div class="tab-pane" id="tab2">
                    </div>
                    <div class="tab-pane" id="tab3">

                    </div>
                    <div class="tab-pane" id="tab4">

                    </div>
                    <div class="tab-pane" id="tab5">

                    </div>
                    <div class="tab-pane" id="tab6">

                    </div>
                   <ul class="pager wizard">
                        <li class="previous">
                            <a href="#">Previous</a>
                       </li>
                        <li class="next">
                            <a href="#">Next</a>
                        </li>
                       <li class="next finish" style="display:none;">
                            <a href="javascript:;">Finish</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">User Register</h4>
                        </div>
                        <div class="modal-body">
                            <p>You Submitted Successfully.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
        </div>
    </section>

    <section class="content">
        <div  class="row">
            <div class="col-md-8">
            <div class="form-group">
                <label  class="col-sm-3 control-label">Lookup Property</label>
                <div class="col-sm-5">
                    {!! Form::text('search_text', null, array('placeholder' => 'Search Text','class' => 'form-control','id'=>'search_text')) !!}
                    {{--<input class="form-control" placeholder="Enter file number or parcel id" onchange="load_property_detail();" name="search_query" id="search_query" type="text">--}}
                </div>

            </div>
            </div>
            <div class="col-md-4"></div>

        </div>
    </section>
    <br>
    <section class="content paddingleft_right15" id="load_property">

    </section>
    <section class="content paddingleft_right15" id="load_customer">

    </section>
   

    <section class="content paddingleft_right15" id="sales_detail" style="display: none">
         <section class="content paddingleft_right15" id="verification">

    </section>
    <section class="content paddingleft_right15" id="payment_list">

    </section>


        <div class="row" >
            <div class="panel panel-primary">
                <div class="panel-heading clearfix">
                    <h4 class="panel-title"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Sale Detail
                    </h4>
                </div>

                <div class="panel-body">
                    <section class="content">
                        <div  class="row">
                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Date Of Sale</label>
                                <div class="col-sm-5">
                                    {!! Form::text('date_of_sale', null, array('placeholder' => 'Date Of Sale','class' => 'form-control','id'=>'date_of_sale')) !!}
                                    {{--<input class="form-control" placeholder="Enter file number or parcel id" onchange="load_property_detail();" name="search_query" id="search_query" type="text">--}}
                                </div>

                            </div>
                        </div>
                        <br>
                        <div  class="row ">
                            <div class="form-group">
                                <label  class="col-sm-3 control-label">Sale Price</label>
                                <div class="col-sm-5">
                                    {!! Form::number('sale_price', null, array('placeholder' => 'Sale Price','class' => 'form-control','id'=>'sale_price')) !!}
                                    {{--<input class="form-control" placeholder="Enter file number or parcel id" onchange="load_property_detail();" name="search_query" id="search_query" type="text">--}}
                                </div>

                            </div>
                        </div>


                    </section>
                </div>

            </div>
        </div>
    </section>
    <section class="content" id="notary_data" style="display: none">
    <div  class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label  class="col-sm-3 control-label">Select Notary</label>
                <div class="col-sm-5">
                   
                    <input id="property_id_note" name="property_id_note" type="hidden" value="" >

                   <select name="notary" id="notary" onchange="load_notary_details();" class = 'form-control'>
                        <option value="">Please Select</option>
                        @foreach($notary as $note)
                            <option value="{{$note->first_name}}">{{$note->first_name}}</option>
                        @endforeach    

                    </select>
                    
                  <!--   {!! Form::select('notary', $notary, null, ['class' => 'form-control', 'onchange' => 'load_notary_details()', 'id' => 'notary']  ) !!} -->
                    <!-- <input class="form-control" placeholder="Enter file number or parcel id" onchange="load_property_detail(2);" name="search_query" id="search_query" type="text"> -->
                </div>

            </div>
        </div>
        <div class="col-md-4"></div>

    </div>
        <div class="row">
            <div class="col-md-5"> </div>
            <div class="col-md-4">
                <button id="submit_ownership" class="btn btn-responsive btn-primary btn-sm">Submit</button>
            </div>
            <div class="col-md-3"></div>
        </div>

        <br>
        <br>

        <div class="row">
            <div class="col-md-5"> </div>
            <div class="col-md-4">
                <button id="payment_confirm" class="btn btn-responsive btn-primary btn-sm">Payment</button>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>

    <div class="load-buyer-detail"></div>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyADWjiTRjsycXf3Lo0ahdc7dDxcQb475qw&sensor=false"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script>
       $( function() {
           $( "#date_of_sale" ).datepicker();
       } );
        final_customer_ids = new Array();
        temp_customer_ids = new Array();

        $(document).ready(function() {
            var src = "{{ URL::to('admin/auto-complete')}}";
            $("#search_text").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term : request.term
                        },
                        success: function(data) {
                            response(data);

                        },

                    });
                },
                select: function (event, ui) {

                    

                    if(ui.item.id !="") {
                         $('#property_id_note').val(ui.item.id);
                        load_property_detail(ui.item.id)
                    }
                },

            });
        });

    function load_notary_details() {

         //var property_id = $('#property_id_note').val();
         var notary = $('#notary').val();
       


             var property_id = $('#property_id').val();
           var date_of_sale = $('#date_of_sale').val();
           var sale_price = $('#sale_price').val();
           var final_customer_ids = $('#final_selected_customer').val();
           var status = "APPLIED";
           var acquired_by = 1;


             // alert('final_customer_ids '+final_customer_ids);
         // alert('date_of_sale '+date_of_sale);
         // alert('sale_price '+sale_price);
         // alert('notary '+notary);
         // alert('final_customer_ids '+final_customer_ids);

        
        
        var load_url = "{{ URL::to('admin/verification')}}";
        $.post(load_url, { property_id: property_id, notary: notary, date_of_sale:date_of_sale, sale_price:sale_price, final_customer_ids:final_customer_ids, },function(data) {

            //alert('sfds');
            $("#verification").replaceWith(data);
        });

    }

    function load_property_detail(property_id) {
       // alert('property_id '+property_id);
        
        var load_url = "{{ URL::to('admin/load-property')}}";
        $.post(load_url, { property_id: property_id},function(data) {
            $("#load_property").replaceWith(data);
        });

    }

        $(document).on("click","#add_buyer",function() {

            var property_id = $('#property_id').val();
            var buyer_url = "{{ URL::to('admin/load_buyer_model') }}";
            $('.load-buyer-detail').load(buyer_url, {property_id:property_id}, function (result) {
                $('#load_buyer_detail').modal({show: true});
                $('#customer-table').DataTable({
                    responsive: true,
                    pageLength: 10,
                    lengthChange: false
                });
            });
        });
    function select_row(row_id) {
        $("#active_"+row_id).addClass('selected').siblings().removeClass('selected');
        var active_id = 'active_'+row_id;
        document.getElementById('last_selected_customer').value = row_id;
//        $("#customer-table tr").each(function() {
//            alert(this.id);
//            if(this.id == active_id){
//
//                $("#change_button_"+row_id).text("Selected");
//            }else{
//                $("#change_button_"+row_id).text("Select");
//            }
//        });

        var temp_customer_ids = [];
        temp_customer_ids.push(row_id);
        //console.log(temp_customer_ids);
    }
    $(document).on("click","#final_add_customer",function() {
        var last_selected_customer = $('#last_selected_customer').val();
          if (last_selected_customer != 0) {
                var exist_id = final_customer_ids.indexOf(last_selected_customer);
                if (exist_id == '-1') {
                    final_customer_ids.push(last_selected_customer);
                }else {
                    swal("", "You have already added this Buyer", "warning")
                }
                $('#load_buyer_detail').modal('hide');
                console.log(final_customer_ids);
            }
            if (final_customer_ids.length > 0) {
                var load_cust_url = "{{ URL::to('admin/load-customer')}}";
                $.post(load_cust_url, {final_customer_ids: JSON.stringify(final_customer_ids)}, function (data) {
                    $("#load_customer").replaceWith(data);
                    $('#sales_detail').css("display", "block");
                    $('#notary_data').css("display", "block");
                    document.getElementById('final_selected_customer').value = final_customer_ids;
                    $('#load-customer-table').DataTable({
                        responsive: true,
                        pageLength: 10,
                        lengthChange: false,
                        searching: false,
                        paging: false
                    });
                });
            }else{
                swal("", "Please Select at least one buyer", "warning")
            }

        });

       $(document).on("click","#submit_ownership",function() {
           var property_id = $('#property_id').val();
           var date_of_sale = $('#date_of_sale').val();
           var sale_price = $('#sale_price').val();
           var final_customer_ids = $('#final_selected_customer').val();
           var status = "APPLIED";
           var acquired_by = 1;
           console.log(final_customer_ids);
          // var final_customer_ids = JSON.stringify(final_customer_ids);
           // console.log(property_id);
           // console.log(date_of_sale);
           // console.log(sale_price);
           // console.log(final_customer_ids);
           if(date_of_sale =="") {
               hidden_error_message();
               $("#date_of_sale").parent().after("<div id='error-message2' class='validation' style='color:red;padding-bottom: 10px;'>Choose a Date Of Sale</div>");
               return false;
           }
           if(sale_price =="") {
               hidden_error_message();
               $("#sale_price").parent().after("<div id='error-message1' class='validation' style='color:red;padding-bottom: 10px;'>Enter a Sale Price</div>");
               return false;
           }
           if(sale_price <0) {
               hidden_error_message();
               $("#sale_price").parent().after("<div id='error-message1' class='validation' style='color:red;padding-bottom: 10px;'>Enter a  Valid Sale Price</div>");
               return false;
           }


           hidden_error_message();
           var load_url = "{{ URL::to('admin/post-ownership')}}";
           $.post(load_url, { property_id: property_id,date_of_sale:date_of_sale,sale_price:sale_price,status:status,acquired_by:acquired_by,final_customer_ids:final_customer_ids},function(data) {

                //console.log(data+'fdfdf');
               swal("", "Successfully Change ownership", "success")
           });

       });

        $(document).on("click","#payment_confirm",function() {
           var date = $('#date_payment').val();
           var ref_no = $('#ref_no').val();
           
          
           // console.log(sale_price);
           // console.log(final_customer_ids);
           // if(date_of_sale =="") {
           //     hidden_error_message();
           //     $("#date_of_sale").parent().after("<div id='error-message2' class='validation' style='color:red;padding-bottom: 10px;'>Choose a Date Of Sale</div>");
           //     return false;
           // }
           // if(sale_price =="") {
           //     hidden_error_message();
           //     $("#sale_price").parent().after("<div id='error-message1' class='validation' style='color:red;padding-bottom: 10px;'>Enter a Sale Price</div>");
           //     return false;
           // }
           


           //hidden_error_message();
           var load_url = "{{ URL::to('admin/payment')}}";
           $.post(load_url, { date:date,ref_no:ref_no},function(data) {

            $("#payment_list").replaceWith(data);

                //console.log(data+'fdfdf');
               //swal("", "Successfully Change ownership", "success")
           });

       });


       $(document).on("click","#submit_final",function() {
        
           var property_id = $('#property_id').val();
           var date_of_sale = $('#date_of_sale').val();
           var sale_price = $('#sale_price').val();
           var final_customer_ids = $('#final_selected_customer').val();
           var seller = $('#property_owner_id').val();
          
           var load_url = "{{ URL::to('admin/post-payment')}}";
           $.post(load_url, { property_id: property_id,date_of_sale:date_of_sale,sale_price:sale_price,final_customer_ids:final_customer_ids,seller:seller},function(data) {

               
               swal("", "Payment Successfully ", "success")
           });

       });
       function hidden_error_message() {
           $('#error-message1').css("display", "none");
           $('#error-message2').css("display", "none");
           return true;
       }


    </script>
@stop
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/form_wizard.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jquery_steps/js/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/sweetalert/js/sweetalert.min.js') }}" type="text/javascript"></script>
@stop



