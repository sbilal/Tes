@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Change Ownership
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />    
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/sweetalert/css/sweetalert.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('assets/vendors/jquery_steps/css/jquery.steps.css') }}" rel="stylesheet"/>

    <link type="text/css" href="{{ asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
    <link type="text/css" href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/pages/invoice.css') }}" rel="stylesheet" type="text/css"/>
    
    <style>
    .wizard > .content > .body
    {        
        width: 100%;
        padding: 2.0%;
        height: 110%;
        overflow-y: auto;
    }    
    </style>
    
@stop

{{-- Page content --}}
@section('content')   
    <section class="content paddingleft_right15">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="box-title">Change Ownership</h4>
            </div>
            <div class="panel-body">
                <div id='wizard-error' class='validation' style='display:none; color:red;padding-bottom: 10px;'></div>
                <form id="coform" action="#" class="basic_steps">
                    <h6>Property</h6>
                    <div class="">
                        <div class="form-group">
                            <div class="row">                                
                                <a class="btn btn-raised btn-success btn-sm" data-toggle="modal" data-href="#property-modal-div" href="#property-modal-div">Select Property</a><p></p>
                                <input type="hidden" id="service_id" name="service_id" value="1" />                                    
                                <input type="hidden" id="property_id" name="property_id" />                                    
                            </div>
                        </div>
                        <section class="bg-white" id="property-details"></section>                                     
                    </div>
                    
                    <h6>Buyer</h6>
                    <div class="mt-2 mb-3">
                        <div class="row">
                            <a class="btn btn-raised btn-success btn-sm" data-toggle="modal" data-href="#customer-modal-div" href="#customer-modal-div">Add Buyers</a><p></p>                            
                            <input type="hidden" id="selected_customers" name="selected_customers"/>                            
                        </div>
                        @include('admin.changeOwnership.selected-customers-table')  
                    </div>
                    
                    <h6>Sale Detail</h6>
                    <div class="mt-2 mb-3">
                        <div class="row">
                            @include('admin.changeOwnership.sale-details')  
                        </div>
                    </div>
                    <h6>Summary</h6>
                    <div class="mt-2 mb-3">
                        <div class="row">
                            <section id="wizard-summary-view"></section>
                        </div>
                    </div>
                    <h6>Payment</h6>
                    <div class="mt-2 mb-3">
                        <div class="row">
                            <section id="wizard-invoice-view"></section>
                            <section id="show-full-invoice-modal"></section>
                        </div>
                    </div>
                </form>
            </div>
        </div>                
    </section>

@include('admin.notaries.modal-search')      
@include('admin.properties.modal-search')  
@include('admin.customers.modal-search')
@include('admin.invoice.payment-modal')


@stop

@section('footer_scripts')
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyADWjiTRjsycXf3Lo0ahdc7dDxcQb475qw&sensor=false"></script>   
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/jquery_steps/js/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('assets/js/pluginjs/validate.js') }}"></script>
    <script src="{{ asset('assets/js/pages/gis-wizard.js') }}"></script>

    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/sifter/sifter.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/microplugin/microplugin.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/selectize/js/selectize.min.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-switch/js/bootstrap-switch.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/switchery/js/switchery.js') }}" ></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/bootstrap-maxlength/js/bootstrap-maxlength.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/card/lib/js/jquery.card.js') }}"></script>
    <script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/custom_elements.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.responsive.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/property-modal.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/customer-modal.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/notary-modal.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/payment.js') }}" ></script>

<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>    
<script  src="{{ asset('assets/js/pages/invoice.js') }}"  type="text/javascript"></script>
    
    <script>
        $(document).ready(function() {                   
            $("#settlement_date").datetimepicker().parent().css("position :relative");
        });        
    </script>
@stop 