<section id="selected-customers-section">
    <div class="panel panel-primary ">
        <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Selected Customers
            </h4>
            <div class="pull-right">
                <a href="#" data-dismiss="modal" class="glyphicon white-text"><i class="glyphicon glyphicon-remove"></i></a>
            </div>                 
        </div>
        <div class="panel-body table-responsive">
                <div class="modal-body">
                    <div class="row">
                        <div class="panel panel-primary">
                            <table class="table table-striped table-bordered" id="selected-customers-table" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Gender</th>
                                        <th>Primary</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>                
</section>