@extends('admin/layouts/default')
{{-- Web site Title --}}
@section('title')
    Permission Create
    @parent
@stop
{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
       Permission Create
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                 Dashboard
            </a>
        </li>
       
        <li class="active">
            Permission Create
        </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="users-add" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Permission Create
                    </h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="post" >
                        <div class="form-group ">
                            <label for="title" class="col-sm-2" style="text-align: right;">
                                Roles
                            </label>
                            <div class="col-sm-6">
                                
                                <select size="1" id="slug" onchange="roles()">
                                    <option value="">Please Select Role</option>
                                    @foreach($roles as $role)
                                    <option value="{{$role->slug}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="permission-details"></div>
            </div>

            <div class="form-group" id="save-btn" style="display: none">
                <div class="col-sm-offset-2 col-sm-4">
                    <a class="btn btn-danger" href="{{ route('admin.permission.create') }}">
                        @lang('button.cancel')
                    </a>
                    <button type="submit" class="btn btn-success" id="update-permission">
                        @lang('button.save')
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>



@stop
