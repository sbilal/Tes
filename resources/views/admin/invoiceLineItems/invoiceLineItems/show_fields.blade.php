<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $invoiceLineItems->id !!}</p>
    <hr>
</div>

<!-- Invoiceid Field -->
<div class="form-group">
    {!! Form::label('InvoiceId', 'Invoiceid:') !!}
    <p>{!! $invoiceLineItems->InvoiceId !!}</p>
    <hr>
</div>

<!-- Feetype Field -->
<div class="form-group">
    {!! Form::label('FeeType', 'Feetype:') !!}
    <p>{!! $invoiceLineItems->FeeType !!}</p>
    <hr>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('Amount', 'Amount:') !!}
    <p>{!! $invoiceLineItems->Amount !!}</p>
    <hr>
</div>

