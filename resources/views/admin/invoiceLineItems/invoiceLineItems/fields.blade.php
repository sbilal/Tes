<!-- Invoiceid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('InvoiceId', 'Invoiceid:') !!}
    {!! Form::text('InvoiceId', null, ['class' => 'form-control']) !!}
</div>

<!-- Feetype Field -->
<div class="form-group col-sm-12">
    {!! Form::label('FeeType', 'Feetype:') !!}
    {!! Form::text('FeeType', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Amount', 'Amount:') !!}
    {!! Form::number('Amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.invoiceLineItems.invoiceLineItems.index') !!}" class="btn btn-default">Cancel</a>
</div>
