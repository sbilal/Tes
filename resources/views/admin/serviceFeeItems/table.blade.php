<table class="table table-responsive table-striped table-bordered" id="serviceFeeItems-table" width="100%">
    <thead>
     <tr>
        <th>Service Id</th>
        <th>Fee Type Id</th>
        <th>Amount</th>
        <th>Due On Status</th>
        <th>Due On Duration</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($serviceFeeItems as $serviceFeeItems)
        <tr>
            <td>{!! $serviceFeeItems->service_id !!}</td>
            <td>{!! $serviceFeeItems->fee_type_id !!}</td>
            <td>{!! $serviceFeeItems->amount !!}</td>
            <td>{!! $serviceFeeItems->due_on_status !!}</td>
            <td>{!! $serviceFeeItems->due_on_duration !!}</td>
            <td>
                 <a href="{{ route('admin.serviceFeeItems.show', collect($serviceFeeItems)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view serviceFeeItems"></i>
                 </a>
                 <a href="{{ route('admin.serviceFeeItems.edit', collect($serviceFeeItems)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit serviceFeeItems"></i>
                 </a>
                 <a href="{{ route('admin.serviceFeeItems.confirm-delete', collect($serviceFeeItems)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete serviceFeeItems"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#serviceFeeItems-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#serviceFeeItems-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop