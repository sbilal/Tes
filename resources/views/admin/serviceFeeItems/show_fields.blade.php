<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $serviceFeeItems->id !!}</p>
    <hr>
</div>

<!-- Service Id Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service Id:') !!}
    <p>{!! $serviceFeeItems->service_id !!}</p>
    <hr>
</div>

<!-- Fee Type Id Field -->
<div class="form-group">
    {!! Form::label('fee_type_id', 'Fee Type Id:') !!}
    <p>{!! $serviceFeeItems->fee_type_id !!}</p>
    <hr>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $serviceFeeItems->amount !!}</p>
    <hr>
</div>

<!-- Due On Status Field -->
<div class="form-group">
    {!! Form::label('due_on_status', 'Due On Status:') !!}
    <p>{!! $serviceFeeItems->due_on_status !!}</p>
    <hr>
</div>

<!-- Due On Duration Field -->
<div class="form-group">
    {!! Form::label('due_on_duration', 'Due On Duration:') !!}
    <p>{!! $serviceFeeItems->due_on_duration !!}</p>
    <hr>
</div>

