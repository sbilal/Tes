<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $serviceFeeItems->id !!}</p>
    <hr>
</div>

<!-- Serviceid Field -->
<div class="form-group">
    {!! Form::label('ServiceId', 'Serviceid:') !!}
    <p>{!! $serviceFeeItems->ServiceId !!}</p>
    <hr>
</div>

<!-- Feetypeid Field -->
<div class="form-group">
    {!! Form::label('FeeTypeId', 'Feetypeid:') !!}
    <p>{!! $serviceFeeItems->FeeTypeId !!}</p>
    <hr>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('Amount', 'Amount:') !!}
    <p>{!! $serviceFeeItems->Amount !!}</p>
    <hr>
</div>

<!-- Dueonstatus Field -->
<div class="form-group">
    {!! Form::label('DueOnStatus', 'Dueonstatus:') !!}
    <p>{!! $serviceFeeItems->DueOnStatus !!}</p>
    <hr>
</div>

<!-- Dueonduration Field -->
<div class="form-group">
    {!! Form::label('DueOnDuration', 'Dueonduration:') !!}
    <p>{!! $serviceFeeItems->DueOnDuration !!}</p>
    <hr>
</div>

