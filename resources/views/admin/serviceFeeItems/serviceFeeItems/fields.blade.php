<!-- Serviceid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('ServiceId', 'Serviceid:') !!}
    {!! Form::text('ServiceId', null, ['class' => 'form-control']) !!}
</div>

<!-- Feetypeid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('FeeTypeId', 'Feetypeid:') !!}
    {!! Form::text('FeeTypeId', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Amount', 'Amount:') !!}
    {!! Form::number('Amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Dueonstatus Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DueOnStatus', 'Dueonstatus:') !!}
    {!! Form::text('DueOnStatus', null, ['class' => 'form-control']) !!}
</div>

<!-- Dueonduration Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DueOnDuration', 'Dueonduration:') !!}
    {!! Form::text('DueOnDuration', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.serviceFeeItems.serviceFeeItems.index') !!}" class="btn btn-default">Cancel</a>
</div>
