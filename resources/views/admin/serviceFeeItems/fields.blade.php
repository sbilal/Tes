<!-- Service Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('service_id', 'Service Id:') !!}
    {!! Form::text('service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Fee Type Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fee_type_id', 'Fee Type Id:') !!}
    {!! Form::text('fee_type_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-12">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Due On Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('due_on_status', 'Due On Status:') !!}
    {!! Form::text('due_on_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Due On Duration Field -->
<div class="form-group col-sm-12">
    {!! Form::label('due_on_duration', 'Due On Duration:') !!}
    {!! Form::text('due_on_duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.serviceFeeItems.index') !!}" class="btn btn-default">Cancel</a>
</div>
