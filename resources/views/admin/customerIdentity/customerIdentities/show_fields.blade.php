<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $customerIdentity->id !!}</p>
    <hr>
</div>

<!-- Customerid Field -->
<div class="form-group">
    {!! Form::label('CustomerId', 'Customerid:') !!}
    <p>{!! $customerIdentity->CustomerId !!}</p>
    <hr>
</div>

<!-- Idtype Field -->
<div class="form-group">
    {!! Form::label('id_type', 'Idtype:') !!}
    <p>{!! $customerIdentity->id_type !!}</p>
    <hr>
</div>

<!-- Idnumber Field -->
<div class="form-group">
    {!! Form::label('IdNumber', 'Idnumber:') !!}
    <p>{!! $customerIdentity->IdNumber !!}</p>
    <hr>
</div>

<!-- Issuingauthority Field -->
<div class="form-group">
    {!! Form::label('IssuingAuthority', 'Issuingauthority:') !!}
    <p>{!! $customerIdentity->IssuingAuthority !!}</p>
    <hr>
</div>

<!-- Placeofissue Field -->
<div class="form-group">
    {!! Form::label('PlaceOfIssue', 'Placeofissue:') !!}
    <p>{!! $customerIdentity->PlaceOfIssue !!}</p>
    <hr>
</div>

<!-- Dateofissue Field -->
<div class="form-group">
    {!! Form::label('DateOfIssue', 'Dateofissue:') !!}
    <p>{!! $customerIdentity->DateOfIssue !!}</p>
    <hr>
</div>

<!-- Expirydate Field -->
<div class="form-group">
    {!! Form::label('ExpiryDate', 'Expirydate:') !!}
    <p>{!! $customerIdentity->ExpiryDate !!}</p>
    <hr>
</div>

<!-- Issuingcountry Field -->
<div class="form-group">
    {!! Form::label('IssuingCountry', 'Issuingcountry:') !!}
    <p>{!! $customerIdentity->IssuingCountry !!}</p>
    <hr>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('Points', 'Points:') !!}
    <p>{!! $customerIdentity->Points !!}</p>
    <hr>
</div>

