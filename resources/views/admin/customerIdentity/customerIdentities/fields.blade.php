<!-- Customerid Field -->
<div class="form-group col-sm-12">
    {!! Form::label('CustomerId', 'Customerid:') !!}
    {!! Form::text('CustomerId', null, ['class' => 'form-control']) !!}
</div>

<!-- Idtype Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_type', 'Idtype:') !!}
    {!! Form::select('id_type', ['DriverLicense ' => 'DriverLicense ', 'Passport' => 'Passport'], null, ['class' => 'form-control']) !!}
</div>

<!-- Idnumber Field -->
<div class="form-group col-sm-12">
    {!! Form::label('IdNumber', 'Idnumber:') !!}
    {!! Form::text('IdNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Issuingauthority Field -->
<div class="form-group col-sm-12">
    {!! Form::label('IssuingAuthority', 'Issuingauthority:') !!}
    {!! Form::text('IssuingAuthority', null, ['class' => 'form-control']) !!}
</div>

<!-- Placeofissue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('PlaceOfIssue', 'Placeofissue:') !!}
    {!! Form::text('PlaceOfIssue', null, ['class' => 'form-control']) !!}
</div>

<!-- Dateofissue Field -->
<div class="form-group col-sm-12">
    {!! Form::label('DateOfIssue', 'Dateofissue:') !!}
    {!! Form::date('DateOfIssue', null, ['class' => 'form-control']) !!}
</div>

<!-- Expirydate Field -->
<div class="form-group col-sm-12">
    {!! Form::label('ExpiryDate', 'Expirydate:') !!}
    {!! Form::date('ExpiryDate', null, ['class' => 'form-control']) !!}
</div>

<!-- Issuingcountry Field -->
<div class="form-group col-sm-12">
    {!! Form::label('IssuingCountry', 'Issuingcountry:') !!}
    {!! Form::text('IssuingCountry', null, ['class' => 'form-control']) !!}
</div>

<!-- Points Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Points', 'Points:') !!}
    {!! Form::text('Points', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.customerIdentity.customerIdentities.index') !!}" class="btn btn-default">Cancel</a>
</div>
