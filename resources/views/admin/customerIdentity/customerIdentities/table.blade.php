<table class="table table-responsive table-striped table-bordered" id="customerIdentities-table" width="100%">
    <thead>
     <tr>
        <th>Customerid</th>
        <th>Idtype</th>
        <th>Idnumber</th>
        <th>Issuingauthority</th>
        <th>Placeofissue</th>
        <th>Dateofissue</th>
        <th>Expirydate</th>
        <th>Issuingcountry</th>
        <th>Points</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($customerIdentities as $customerIdentity)
        <tr>
            <td>{!! $customerIdentity->CustomerId !!}</td>
            <td>{!! $customerIdentity->id_type !!}</td>
            <td>{!! $customerIdentity->IdNumber !!}</td>
            <td>{!! $customerIdentity->IssuingAuthority !!}</td>
            <td>{!! $customerIdentity->PlaceOfIssue !!}</td>
            <td>{!! $customerIdentity->DateOfIssue !!}</td>
            <td>{!! $customerIdentity->ExpiryDate !!}</td>
            <td>{!! $customerIdentity->IssuingCountry !!}</td>
            <td>{!! $customerIdentity->Points !!}</td>
            <td>
                 <a href="{{ route('admin.customerIdentity.customerIdentities.show', collect($customerIdentity)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view customerIdentity"></i>
                 </a>
                 <a href="{{ route('admin.customerIdentity.customerIdentities.edit', collect($customerIdentity)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit customerIdentity"></i>
                 </a>
                 <a href="{{ route('admin.customerIdentity.customerIdentities.confirm-delete', collect($customerIdentity)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete customerIdentity"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#customerIdentities-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#customerIdentities-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop