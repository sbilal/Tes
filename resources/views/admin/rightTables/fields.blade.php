<!-- Property Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('Property_id', 'Property Id:') !!}
    {!! Form::text('Property_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.rightTables.index') !!}" class="btn btn-default">Cancel</a>
</div>
