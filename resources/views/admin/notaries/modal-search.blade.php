<div class="modal fade in" id="notary-modal-div" tabindex="-1" role="dialog" aria-hidden="false" style="width:80%; margin:auto; top:5%">    
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Search Notary
                </h4>
                <div class="pull-right">
                    <a href="#" data-dismiss="modal" class="glyphicon white-text"><i class="glyphicon glyphicon-remove"></i></a>
                </div>                 
            </div>
            <div class="panel-body table-responsive">
                    <div class="modal-body">
                        <div class="row">
                            <div class="panel panel-primary">
                                <input hidden="true" id="notary_id" name='notary_id'/>
                                <table class="table table-striped table-bordered" id="notary-modal" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>First Name</th>
                                            <th>Second Name</th>
                                            <th>Phone</th>
                                            <th>Notary Number</th>
                                            <th>Expiry Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
</div>    
 