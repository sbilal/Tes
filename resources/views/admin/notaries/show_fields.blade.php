<div class="panel-heading">
    <h3 class="panel-title">
        <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
        Notary Details
    </h3>
    <span class="pull-right">
        <i class="glyphicon glyphicon-chevron-up clickable"></i>
    </span>
</div>
<div class="panel-body">
    <div class="row">
        <div class="col-md-8">
            <div class="table-responsive-lg table-responsive-sm table-responsive-md table-responsive">
                <table class="table table-bordered table-striped" id="customer">
                    <tbody>
                        <tr>
                            <td>First Name</td>
                            <td>
                                <p class="user_name_max">{{$notary->first_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>Second Name</td>
                            <td>
                                <p class="user_name_max">{{$notary->second_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>Third Name</td>
                            <td>
                                <p class="user_name_max">{{$notary->third_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td>Fourth Name</td>
                            <td>
                                <p class="user_name_max">{{$notary->fourth_name}}</p>
                            </td>
                        </tr>  
                        <tr>
                            <td>Phone</td>
                            <td>
                                {{$notary->contact_number}} 
                            </td>
                        </tr>                        
                        <tr>
                            <td>E-mail</td>
                            <td>
                                {{$notary->email}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Notary Registered Number                                                          
                            </td>
                            <td>
                                {{$notary->notary_number}}
                            </td>
                        </tr>
                        <tr>
                            <td>Expiry Date</td>
                            <td>
                                {{  Carbon\Carbon::parse($notary->notary_expiry_date)->format('d M Y')  }}
                            </td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                {{$notary->address}} 
                            </td>
                        </tr>
                        <tr>
                            <td>District /SubDivision</td>
                            <td>
                                {{$notary->district}} , {{$notary->sub_division}} 
                            </td>
                        </tr>                        
                        <tr>
                            <td>Last Updated</td>
                            <td>
                                {{ $notary->created_at->diffForHumans() }}
                            </td>
                        </tr>
                    </tbody></table>
            </div>
        </div>
        <div class="col-md-4">
            <div class="img-file">
                <img src="http://localhost/laravel56/public/assets/images/authors/no_avatar.jpg" alt="..." class="img-fluid">
            </div>
        </div>    
    </div>
</div>    