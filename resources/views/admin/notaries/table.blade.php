<table class="table table-responsive table-striped table-bordered" id="notaries-table" width="100%">
    <thead>
     <tr>
        <th>First Name</th>
        <th>Second Name</th>
        <th>Third Name</th>
        <th>Fourth Name</th>
        <th>Address</th>
        <th>District</th>
        <th>Contact Number</th>
        <th>Notary Number</th>
        <th>Notary Expiry Date</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($notaries as $notary)
        <tr>
            <td>{!! $notary->first_name !!}</td>
            <td>{!! $notary->second_name !!}</td>
            <td>{!! $notary->third_name !!}</td>
            <td>{!! $notary->fourth_name !!}</td>
            <td>{!! $notary->address !!}</td>
            <td>{!! $notary->district !!}</td>
            <td>{!! $notary->contact_number !!}</td>
            <td>{!! $notary->notary_number !!}</td>
            <td>{!! $notary->notary_expiry_date !!}</td>
            <td>
                 <a href="{{ route('admin.notaries.show', collect($notary)->first() ) }}">
                     <i class="livicon" data-name="info" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view notary"></i>
                 </a>
                 <a href="{{ route('admin.notaries.edit', collect($notary)->first() ) }}">
                     <i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="edit notary"></i>
                 </a>
                 <a href="{{ route('admin.notaries.confirm-delete', collect($notary)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete notary"></i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#notaries-table').DataTable({
                      responsive: true,
                      pageLength: 10
                  });
                  $('#notaries-table').on( 'page.dt', function () {
                     setTimeout(function(){
                           $('.livicon').updateLivicon();
                     },500);
                  } );

       </script>

@stop