<!-- First Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Second Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('second_name', 'Second Name:') !!}
    {!! Form::text('second_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Third Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('third_name', 'Third Name:') !!}
    {!! Form::text('third_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Fourth Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('fourth_name', 'Fourth Name:') !!}
    {!! Form::text('fourth_name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('contact_number', 'Contact Number:') !!}
    {!! Form::text('contact_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-12">
    {!! Form::label('district', 'District:') !!}
    {!! Form::text('district', null, ['class' => 'form-control']) !!}
</div>

<!-- State Field -->
<div class="form-group col-sm-12">
    {!! Form::label('sub_division', 'SubDivision:') !!}
    {!! Form::text('sub_division', null, ['class' => 'form-control']) !!}
</div>

<!-- Notary Number Field -->
<div class="form-group col-sm-12">
    {!! Form::label('notary_number', 'Notary Number:') !!}
    {!! Form::number('notary_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Notary Expiry Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('notary_expiry_date', 'Notary Expiry Date:') !!}
    {!! Form::date('notary_expiry_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.notaries.index') !!}" class="btn btn-default">Cancel</a>
</div>
