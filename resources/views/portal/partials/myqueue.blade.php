<div class="panel panel-primary filterable">
    <div class="panel-heading clearfix  ">
        <div class="panel-title pull-left">
               <div class="caption">
            <i class="livicon" data-name="camera" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
            Recent Applications
        </div>
        </div>
    </div>
    <div class="panel-body table-responsive">
        <table class="table table-striped table-bordered" width="100%" id="recent-transactions-table">
            <thead>
                <tr>
                    <th>Reference No</th>
                    <th class="numeric">Service</th>
                    <th>Application Date</th>
                    <th class="numeric">Property</th>
                    <th class="numeric">Buyer</th>
                    <th class="numeric">Seller</th>                                            
                    <th class="">Settlement Date</th>
                    <th class="numeric">Amount</th>
                    <th class="numeric">Payment Ref No</th>
                    <th class="">Application Status</th>
                </tr>
            </thead>
            <tbody>
               
            </tbody>            
        </table>
    </div>
</div>