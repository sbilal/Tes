<div class="panel panel-success filterable">
    <div class="panel-heading clearfix  ">
        <div class="panel-title pull-left">
               <div class="caption">
            <i class="livicon" data-name="camera" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
            My Tasks
        </div>
        </div>
    </div>
    <div class="panel-body table-responsive">
        <table class="table table-striped table-bordered" width="100%" id="my-tasks-table">
            <thead>
                <tr>
                    <th>Task Id</th>
                    <th>Reference No</th>
                    <th class="numeric">Service</th>
                    <th class="numeric">Property</th>
                    <th class="numeric">Buyer</th>
                    <th class="">Settlement Date</th>
                    <th class="numeric">Payment Details</th>
                    <th class="">Application Status</th>
                    <th class="">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                    <td/>
                </tr>
                    
            </tbody>
        </table>
    </div>
</div>